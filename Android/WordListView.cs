﻿using System;
using Android.Widget;
using Android.Views;
using Kasikoi.Data;
using System.Collections.Generic;
using Android.Content;

namespace Kasikoi
{
    public class WordListView
    {
        MainActivity activity;

        Button btnLearningState;
        Button btnFilter;
        LinearLayout boxWordList;
        Button btnPrevious;
        Button btnNext;
        TextView txtPage;

        const int wordsPerPage = 20;
        int totalPages;
        int currentPage;
        int totalWords;

        string search;
        string sort;
        string filter;

        public WordListView(MainActivity activity, string search = null, string sort = "한자 오름차순 순", string filter = "전체 단어")
        {
            this.activity = activity;

            activity.SetContentView(Resource.Layout.WordList);

            btnLearningState = activity.FindViewById<Button>(Resource.Id.btnLearningState);
            btnFilter = activity.FindViewById<Button>(Resource.Id.btnFilter);
            boxWordList = activity.FindViewById<LinearLayout>(Resource.Id.boxWordList);
            btnPrevious = activity.FindViewById<Button>(Resource.Id.btnPrevious);
            btnNext = activity.FindViewById<Button>(Resource.Id.btnNext);
            txtPage = activity.FindViewById<TextView>(Resource.Id.txtPage);
            LearningSystem LS = LearningSystem.Instance;

            this.search = search;
            this.sort = sort;
            this.filter = filter;

            var wordlist = LS.RefreshWordList(search, sort, filter);

            totalWords = wordlist.Count;
            totalPages = wordlist.Count / wordsPerPage + 1;
            currentPage = 1;


            FillPage(wordlist, 1);
            txtPage.Text = currentPage + "/" + totalPages;





            btnLearningState.Click += delegate
                {
                    new MainPageView(activity);
                };

            btnFilter.Click += delegate
                {
                    new WordFilterView(activity, search, sort, filter);
                };

            btnNext.Click += delegate
                {
                    if (totalPages == currentPage)
                    {
                        Toast.MakeText(activity, "마지막 페이지입니다.", ToastLength.Short).Show();
                        return;
                    }
                    currentPage++;
                    FillPage(wordlist, currentPage);
                    txtPage.Text = currentPage + "/" + totalPages;
                };

            btnPrevious.Click += delegate
                {
                    if (currentPage == 1)
                    {
                        Toast.MakeText(activity, "첫번째 페이지입니다.", ToastLength.Short).Show();
                        return;
                    }
                    currentPage--;
                    FillPage(wordlist, currentPage);
                    txtPage.Text = currentPage + "/" + totalPages;
                };
        }



        void FillPage(List<Word> wordlist, int page)
        {
            boxWordList.RemoveAllViews();
            int offsetFirst = (page - 1) * wordsPerPage;
            int offsetLast = Math.Min(page * wordsPerPage - 1, totalWords - 1);  
            for (int  i = offsetFirst; i <= offsetLast; i++)
            {
                var w = wordlist[i];
                Button btnWord = new Button(activity);
                string wordText = StringHelper.ConcatenateWithSemicolon(w.KanjiNotations)
                    + " [" + StringHelper.ConcatenateWithSemicolon(w.KanaNotations) + "]: "
                    + StringHelper.ConcatenateWithSemicolon(w.Meanings);
                btnWord.Text = wordText;
                btnWord.SetBackgroundColor(Android.Graphics.Color.ParseColor("#00000000"));
                btnWord.Gravity = GravityFlags.Left;

                boxWordList.AddView(btnWord);

                btnWord.Click += delegate
                    {
                        int wordOffset = wordlist.FindIndex(x => x.Id == w.Id);
                        var infoIntent = new Intent(activity, typeof(WordInfoActivity));
                        infoIntent.PutExtra("wordOffset", wordOffset);
                        activity.StartActivityForResult(infoIntent, 0);
                    };

            }

        }




    }
}

