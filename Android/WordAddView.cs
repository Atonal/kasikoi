﻿using System;
using System.Collections.Generic;
using Android.Widget;
using Kasikoi.Data;
using Android.Content;
using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace Kasikoi
{
    public class WordAddView
    {
        MainActivity activity;

        EditText etxtAddKanji;
        EditText etxtAddKana;
        EditText etxtAddMeaning;
        EditText etxtAddExample;
        EditText etxtAddExampleMeaning;
        CheckBox cbxNoKanji;
        TextView txtError;
        LinearLayout boxExmapleWrap;
        LinearLayout boxExample1;
        LinearLayout boxExample2;
        LinearLayout boxExample3;
        LinearLayout boxExample4;
        TextView txtExample1;
        TextView txtExample2;
        TextView txtExample3;
        TextView txtExample4;
        Button btnEditExample1;
        Button btnEditExample2;
        Button btnEditExample3;
        Button btnEditExample4;

        List<LinearLayout> boxExamples = new List<LinearLayout>();
        List<TextView> txtExamples = new List<TextView>();
        List<Button> btnExamples = new List<Button>();
        List<Word> similarWords = new List<Word>();
        Color etxtBackground;

        Button btnAddWord;
        Button btnCancel;

        public WordAddView(MainActivity activity)
        {
            this.activity = activity;
            activity.SetContentView(Resource.Layout.WordAdd);

            etxtAddKanji = activity.FindViewById<EditText>(Resource.Id.etxtAddKanji);
            etxtAddKana = activity.FindViewById<EditText>(Resource.Id.etxtAddKana);
            etxtAddMeaning = activity.FindViewById<EditText>(Resource.Id.etxtAddMeaning);
            etxtAddExample = activity.FindViewById<EditText>(Resource.Id.etxtAddExample);
            etxtAddExampleMeaning = activity.FindViewById<EditText>(Resource.Id.etxtAddExampleMeaning);
            cbxNoKanji = activity.FindViewById<CheckBox>(Resource.Id.cbxNoKanji);
            txtError = activity.FindViewById<TextView>(Resource.Id.txtError);
            boxExmapleWrap = activity.FindViewById<LinearLayout>(Resource.Id.boxExampleWrap);
            boxExample1 = activity.FindViewById<LinearLayout>(Resource.Id.boxExample1);
            boxExample2 = activity.FindViewById<LinearLayout>(Resource.Id.boxExample2);
            boxExample3 = activity.FindViewById<LinearLayout>(Resource.Id.boxExample3);
            boxExample4 = activity.FindViewById<LinearLayout>(Resource.Id.boxExample4);
            txtExample1 = activity.FindViewById<TextView>(Resource.Id.txtExample1);
            txtExample2 = activity.FindViewById<TextView>(Resource.Id.txtExample2);
            txtExample3 = activity.FindViewById<TextView>(Resource.Id.txtExample3);
            txtExample4 = activity.FindViewById<TextView>(Resource.Id.txtExample4);
            btnEditExample1 = activity.FindViewById<Button>(Resource.Id.btnEditExample1);
            btnEditExample2 = activity.FindViewById<Button>(Resource.Id.btnEditExample2);
            btnEditExample3 = activity.FindViewById<Button>(Resource.Id.btnEditExample3);
            btnEditExample4 = activity.FindViewById<Button>(Resource.Id.btnEditExample4);
            btnAddWord = activity.FindViewById<Button>(Resource.Id.btnAddWord);
            btnCancel = activity.FindViewById<Button>(Resource.Id.btnCancel);

            etxtBackground = (etxtAddKanji.Background as ColorDrawable).Color;

            boxExamples.Add(boxExample1);
            boxExamples.Add(boxExample2);
            boxExamples.Add(boxExample3);
            boxExamples.Add(boxExample4);
            txtExamples.Add(txtExample1);
            txtExamples.Add(txtExample2);
            txtExamples.Add(txtExample3);
            txtExamples.Add(txtExample4);
            btnExamples.Add(btnEditExample1);
            btnExamples.Add(btnEditExample2);
            btnExamples.Add(btnEditExample3);
            btnExamples.Add(btnEditExample4);


            txtError.Visibility = Android.Views.ViewStates.Gone;
            UpdateSimilarWords(new List<Word>());

            etxtAddKanji.TextChanged += delegate
            {
                UpdateSimilarWords();
            };
            etxtAddKana.TextChanged += delegate
            {
                UpdateSimilarWords();
            };
            etxtAddMeaning.TextChanged += delegate
            {
                UpdateSimilarWords();
            };

            btnAddWord.Click += delegate
            {
                AddWord();
            };
            btnCancel.Click += delegate
            {
                MoveToMainPage();
            };
            
            btnEditExample1.Click+= delegate
                {
                    var editActivity = new Intent(activity, typeof(WordEditActivity));
                    editActivity.PutExtra("wordId", similarWords[0].Id);
                    activity.StartActivity(editActivity);
                };
            btnEditExample2.Click+= delegate
                {
                    var editActivity = new Intent(activity, typeof(WordEditActivity));
                    editActivity.PutExtra("wordId", similarWords[1].Id);
                    activity.StartActivity(editActivity);
                };
            btnEditExample3.Click+= delegate
                {
                    var editActivity = new Intent(activity, typeof(WordEditActivity));
                    editActivity.PutExtra("wordId", similarWords[2].Id);
                    activity.StartActivity(editActivity);
                };
            btnEditExample4.Click+= delegate
                {
                    var editActivity = new Intent(activity, typeof(WordEditActivity));
                    editActivity.PutExtra("wordId", similarWords[3].Id);
                    activity.StartActivity(editActivity);
                };
            /*
            for (int i = 0; i < 4; i++)
            {
                btnExamples[i].Click+= delegate
                    {
                        var editActivity = new Intent(activity, typeof(WordEditActivity));
                        editActivity.PutExtra("wordId", similarWords[i].Id);
                        activity.StartActivity(editActivity);
                    };
            }
            */

            activity.BackButtonPressd += HandleBackButton;
        }

        void AddWord()
        {
            if (string.IsNullOrWhiteSpace(etxtAddKana.Text))
            {
                activity.ShowAlertPopup("가나 표기는 반드시 입력해야 합니다.", delegate {
                    etxtAddKana.SetBackgroundColor(Color.Red);
                });
                return;
            }
            if (string.IsNullOrWhiteSpace(etxtAddKanji.Text) && string.IsNullOrWhiteSpace(etxtAddMeaning.Text))
            {
                activity.ShowAlertPopup("한자 표기나 뜻 중 하나는 반드시 입력해야 합니다.", delegate {
                    etxtAddKanji.SetBackgroundColor(Color.Red);
                    etxtAddMeaning.SetBackgroundColor(Color.Red);
                });
                return;
            }

            var word = new Word(0,
                StringHelper.SplitWithPunctuation(etxtAddKanji.Text),
                StringHelper.SplitWithPunctuation(etxtAddKana.Text),
                StringHelper.SplitWithPunctuation(etxtAddMeaning.Text),
                cbxNoKanji.Checked,
                etxtAddExample.Text,
                etxtAddExampleMeaning.Text);

            var LS = LearningSystem.Instance;
            var addedWord = LS.AddWord(word);
            LoginService.commandService.AddWord(addedWord.Id,
                etxtAddKanji.Text,
                etxtAddKana.Text,
                etxtAddMeaning.Text,
                cbxNoKanji.Checked,
                etxtAddExample.Text,
                etxtAddExampleMeaning.Text);

            etxtAddKanji.Text = "";
            etxtAddKana.Text = "";
            etxtAddMeaning.Text = "";
            etxtAddExample.Text = "";
            etxtAddExampleMeaning.Text = "";
            cbxNoKanji.Checked = false;

            Toast.MakeText(activity.ApplicationContext, "단어가 추가되었습니다.", ToastLength.Short).Show();
        }

        void UpdateSimilarWords()
        {
            var similarList = new List<Word>();
            var system = LearningSystem.Instance;
            etxtAddKanji.SetBackgroundColor(etxtBackground);
            var kanjis = StringHelper.SplitWithPunctuation(etxtAddKanji.Text);
            foreach (var x in kanjis)
            {
                var similarListForX = system.GetWordWithKanjiNotation(x);
                if (similarListForX.Count > 0)
                {
                    similarList.AddRange(similarListForX);
                    etxtAddKanji.SetBackgroundColor(Color.Orange);
                }
            }

            etxtAddKana.SetBackgroundColor(etxtBackground);
            var kanas = StringHelper.SplitWithPunctuation(etxtAddKana.Text);
            foreach (var x in kanas)
            {
                var similarListForX = system.GetWordWithKanaNotation(x);
                if (similarListForX.Count > 0)
                {
                    similarList.AddRange(similarListForX);
                    etxtAddKana.SetBackgroundColor(Color.Orange);
                }
            }

            etxtAddMeaning.SetBackgroundColor(etxtBackground);

            for (int i = 0; i < similarList.Count; i++)
                for (int j = i + 1; j < similarList.Count; j++)
                {
                    if (similarList[i] == similarList[j])
                    {
                        similarList.RemoveAt(j);
                        j--;
                    }
                }

            UpdateSimilarWords(similarList);
        }

        void MoveToMainPage()
        {
            LearningSystem.Instance.Save();
            activity.BackButtonPressd -= HandleBackButton;
            new MainPageView(activity);
        }

        void HandleBackButton()
        {
            activity.MarkBackButtonHandled();
            MoveToMainPage();
        }

        void UpdateSimilarWords(List<Word> words)
        {
            foreach (var x in boxExamples)
            {
                x.Visibility = Android.Views.ViewStates.Gone;
            }
            similarWords.Clear();

            if (words.Count == 0)
                boxExmapleWrap.Visibility = Android.Views.ViewStates.Gone;
            else
                boxExmapleWrap.Visibility = Android.Views.ViewStates.Visible;

            for (int i=0; i < 4 && i < words.Count; i++)
            {
                boxExamples[i].Visibility = Android.Views.ViewStates.Visible;
                string displayTxt = StringHelper.ConcatenateWithSemicolon(words[i].KanjiNotations)
                                    + "[" + StringHelper.ConcatenateWithSemicolon(words[i].KanaNotations) + "]"
                    + StringHelper.ConcatenateWithSemicolon(words[i].Meanings);
                txtExamples[i].Text = displayTxt;
                similarWords.Add(words[i]);
            }

        }
    }
}

