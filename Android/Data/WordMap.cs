﻿using System.Collections.Generic;
using System.Linq;

namespace Kasikoi.Data
{
	public class WordMap
	{
		public void Add(string key, Word word)
		{
			if (!map.ContainsKey(key))
				map.Add(key, new List<Word>());

			map[key].Add(word);
		}

		public void Clear()
		{
			map.Clear();
		}

        public void RemoveById(int wordId)
        {
            foreach (var key in map.Keys.ToList())
            {
                map[key].RemoveAll(w => w.Id == wordId);
                if (map[key].Count == 0)
                {
                    map.Remove(key);
                }
            }
        }


		public IReadOnlyList<Word> this[string key]
		{
			get
			{
				if (map.ContainsKey(key))
					return map[key];
				return emptyList;
			}
		}

		Dictionary<string, List<Word>> map = new Dictionary<string, List<Word>>();
		List<Word> emptyList = new List<Word>();
	}
}