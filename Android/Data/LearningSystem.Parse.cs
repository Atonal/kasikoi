﻿using Kasikoi.Data.Parsers;
using System.Collections.Generic;
using System.Text;
using System.Json;
using System;
using System.IO;

namespace Kasikoi.Data
{
	public partial class LearningSystem
	{
        public static readonly string KanjiFilePath = PathHelper.SaveDirectory + "/kanji.txt";

		class FileSection
		{
            public const string Username = "Username";
            public const string LastCommand = "LastCommand";
			public const string WordBook = "Word Book";
			public const string WordRecord = "Word Record";
			public const string Log = "Log";
			public const string EndSection = "End Section";
		}

		static class ParseHelper
		{
			public static void MoveToSection(string section, string[] parseLines, ref int currentLine)
			{
				var marker = SectionMarker(section);
				while (currentLine < parseLines.Length)
				{
					if (parseLines[currentLine] == marker)
					{
						currentLine++;
						return;
					}
					currentLine++;
				}
			}

			public static int MoveToEndSection(string[] parseLines, ref int currentLine)
			{
				var marker = EndSectionMarker;
				while (currentLine < parseLines.Length)
				{
					if (parseLines[currentLine] == marker)
					{
						currentLine++;
						return currentLine - 1;
					}
					currentLine++;
				}
				return parseLines.Length;
			}

			static string SectionMarker(string section)
			{
				return "# " + section;
			}

			public static void AppendSection(StringBuilder sb, string section)
			{
				sb.AppendLine(SectionMarker(section));
			}

			static string EndSectionMarker = SectionMarker(FileSection.EndSection);
			public static bool IsEndSection(string[] parseLines, int currentLine)
			{
				return parseLines[currentLine] == EndSectionMarker;
			}
		}

		static void Parse(string[] parseLines, LearningSystem parseTarget)
		{
			int currentLine = 0;

            ParseHelper.MoveToSection(FileSection.Username, parseLines, ref currentLine);
            parseTarget.username = parseLines[currentLine];

            ParseHelper.MoveToSection(FileSection.LastCommand, parseLines, ref currentLine);
            parseTarget.lastCommand = DateTime.Parse(parseLines[currentLine]);

			ParseHelper.MoveToSection(FileSection.WordBook, parseLines, ref currentLine);
			var words = new List<Word>();
			while (currentLine < parseLines.Length)
			{
				if (ParseHelper.IsEndSection(parseLines, currentLine))
					break;
				var word = WordParser.Parse(parseLines, ref currentLine);
				words.Add(word);
			}
			parseTarget.wordBook = new WordBook(words);
			parseTarget.kanjiCharacterToWord.Clear();
			foreach (var w in words)
			{
				foreach (var c in w.ComposedKanji)
				{
					parseTarget.kanjiCharacterToWord.Add(c.ToString(), w);
				}
			}

            LoadKanjiBook(parseTarget);

			ParseHelper.MoveToSection(FileSection.WordRecord, parseLines, ref currentLine);
			parseTarget.wordRecords.Clear();
			while (currentLine < parseLines.Length)
			{
				if (ParseHelper.IsEndSection(parseLines, currentLine))
					break;

				var record = WordRecord.Parse(parseLines, ref currentLine);
				parseTarget.wordRecords.Add(record.Id, record);
			}

			ParseHelper.MoveToSection(FileSection.Log, parseLines, ref currentLine);
			var logFrom = currentLine;
			var logTo = ParseHelper.MoveToEndSection(parseLines, ref currentLine);
			parseTarget.logList.Clear();
            for (int line = logFrom; line < logTo; line++)
            {
                parseTarget.logList.Add(new Log(parseLines[line]));
            }

			parseTarget.problemSelector = new ProblemSelector(
				parseTarget.wordBook,
				parseTarget.wordRecords);
		}

        static void LoadKanjiBook(LearningSystem parseTarget)
        {
            if (File.Exists(KanjiFilePath))
            {
                var kanjiLines = File.ReadAllLines(KanjiFilePath);
                var kanjis = new List<Kanji>();
                for (int i = 2; i < kanjiLines.Length; i += 2)
                {
                    var kanji = new Kanji(i / 2, kanjiLines[i - 1], StringHelper.SplitWithPunctuation(kanjiLines[i]));
                    kanjis.Add(kanji);
                }
                parseTarget.kanjiBook = new KanjiBook(kanjis);
            }
            else
            {
                parseTarget.kanjiBook = new KanjiBook();
            }
        }

        public void ParseJson(JsonObject jsonObject, DateTime lastCommand, LearningSystem parseTarget)
        {
            string userName = (string)jsonObject["userName"];
            parseTarget.username = userName;

            parseTarget.lastCommand = lastCommand;

            parseTarget.logList.Clear();
            JsonArray logArray = jsonObject["log"] as JsonArray;
            foreach (JsonValue x in logArray)
            {
                JsonArray logEntry = x as JsonArray;
                parseTarget.logList.Add(new Log(DateTime.Parse((string)logEntry[0]) - TimeSpan.FromHours(9),
                        (int)logEntry[1],
                        (int)logEntry[2],
                        (int)logEntry[3],
                        (int)logEntry[4]));
            }

            JsonObject learningPoolObject = jsonObject["learningPool"] as JsonObject;
            var words = new List<Word>();
            foreach (var pair in learningPoolObject)
            {
                int wordId = Int32.Parse(pair.Key);
                JsonArray wordInfo = pair.Value as JsonArray;

                words.Add(new Word(System.Int32.Parse(pair.Key),
                    StringHelper.SplitWithPunctuation((string)wordInfo[0]),
                    StringHelper.SplitWithPunctuation((string)wordInfo[1]),
                    StringHelper.SplitWithPunctuation((string)wordInfo[2]),
                    (bool)wordInfo[3],
                    (string)wordInfo[4],
                    (string)wordInfo[5]));
            }
            parseTarget.wordBook = new WordBook(words);

            parseTarget.kanjiCharacterToWord.Clear();
            foreach (var w in words)
            {
                foreach (var c in w.ComposedKanji)
                {
                    parseTarget.kanjiCharacterToWord.Add(c.ToString(), w);
                }
            }

            LoadKanjiBook(parseTarget);

            JsonObject learningRecordObject = jsonObject["learningRecord"] as JsonObject;
            parseTarget.wordRecords.Clear();

            foreach (var pair in learningRecordObject)
            {
                int recordId = Int32.Parse(pair.Key);
                JsonArray recordInfo = pair.Value as JsonArray;

                List<bool> markPattern = new List<bool>();

                foreach(char c in (string)recordInfo[9])
                {
                    if (c == 'O')
                        markPattern.Add(true);
                    else
                        markPattern.Add(false);
                }

                parseTarget.wordRecords.Add(recordId, new WordRecord(recordId,
                    DateTime.Parse((string)recordInfo[0]) - TimeSpan.FromHours(9),
                    DateTime.Parse((string)recordInfo[1]) - TimeSpan.FromHours(9),
                    (int)recordInfo[2],
                    (int)recordInfo[3],
                    (int)recordInfo[4],
                    (int)recordInfo[5],
                    (int)recordInfo[6],
                    (int)recordInfo[7],
                    (int)recordInfo[8],
                    markPattern));
            }

			parseTarget.problemSelector = new ProblemSelector(
				parseTarget.wordBook,
				parseTarget.wordRecords);
        }

		public static string Serialize(LearningSystem data)
		{
			StringBuilder sb = new StringBuilder();

            ParseHelper.AppendSection(sb, FileSection.Username);
            var name = data.username;
            sb.AppendLine(name);

            ParseHelper.AppendSection(sb, FileSection.LastCommand);
            sb.AppendLine(data.lastCommand.ToString("yyyy-MM-ddTHH:mm:ss.000Z"));

			ParseHelper.AppendSection(sb, FileSection.WordBook);
			var words = data.GetAllWords();
			foreach (var w in words)
				WordParser.AppendTo(sb, w);
			ParseHelper.AppendSection(sb, FileSection.EndSection);
			sb.AppendLine();

			ParseHelper.AppendSection(sb, FileSection.WordRecord);
			foreach (var r in data.wordRecords.Values)
				r.AppendTo(sb);
			ParseHelper.AppendSection(sb, FileSection.EndSection);

			ParseHelper.AppendSection(sb, FileSection.Log);
			foreach (var log in data.logList)
                sb.AppendLine(log.ToString());
			ParseHelper.AppendSection(sb, FileSection.EndSection);


			return sb.ToString();
		}


	}
}
