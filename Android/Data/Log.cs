﻿using System;

namespace Kasikoi.Data
{
    public class Log
    {
        public Log(DateTime date, int totalWordCount, int EncounteredWordCount, int MemorizedWordCount, int LearnCount)
        {
            this.Date = new DateTime(date.Year, date.Month, date.Day);
            this.TotalWordCount = totalWordCount;
            this.EncounteredWordCount = EncounteredWordCount;
            this.MemorizedWordCount = MemorizedWordCount;
            this.LearnCount = LearnCount;
        }

        public Log(string logString)
        {
            var tokens = logString.Split('\t');
            var date = DateTime.Parse(tokens[0]);
            this.Date = new DateTime(date.Year, date.Month, date.Day);
            this.TotalWordCount = int.Parse(tokens[1]);
            this.EncounteredWordCount = int.Parse(tokens[2]);
            this.MemorizedWordCount = int.Parse(tokens[3]);
            this.LearnCount = int.Parse(tokens[4]);
        }
            
        public override string ToString()
        {
            return string.Format("{0}\t{1}\t{2}\t{3}\t{4}",
                Date.ToString("yyyy-MM-dd"),
                this.TotalWordCount,
                this.EncounteredWordCount,
                this.MemorizedWordCount,
                this.LearnCount);
        }

        public void AddLearnCount()
        {
            LearnCount = LearnCount + 1;
        }

        public readonly DateTime Date;
        public int TotalWordCount { get; private set;}
        public int EncounteredWordCount { get; private set;}
        public int MemorizedWordCount { get; private set;}
        public int LearnCount { get; private set;}
    }
}

