﻿using System.Text;

namespace Kasikoi.Data.Parsers
{
	public static class IdRelationParser
	{
		public static IdRelations Parse(string[] textToBeParsed, int lineFrom, int lineTo)
		{
			IdRelations relation = new IdRelations();
			for (int line = lineFrom; line < lineTo && line < textToBeParsed.Length; line++)
			{
				var ids = StringHelper.SplitIntegersBySpace(textToBeParsed[line]);
				var key = ids[0];
				for (int ir = 1; ir < ids.Length; ir++)
					relation.AddRelation(key, ids[ir]);
			}
			return relation;
		}

		public static void AppendTo(StringBuilder targetBuilder, IdRelations relation)
		{
			var keys = relation.GetAllKeyIds();
			foreach (var key in keys)
			{
				var related = relation.GetAllRelatedIdsWith(key);
				targetBuilder.Append(key);
				targetBuilder.Append(' ');
				StringHelper.AppendWithSpaceTo(targetBuilder, related);
				targetBuilder.AppendLine();
			}
		}
	}
}
