﻿using System.Text;

namespace Kasikoi.Data.Parsers
{
	static class WordParser
	{
		public static Word Parse(string[] textSplittedByLine, ref int currentLine)
		{
			var id = int.Parse(textSplittedByLine[currentLine++]);
			var kanjiNotations = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
			var kanaNotations = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
			var meanings = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
			var noKanjiTest = bool.Parse(textSplittedByLine[currentLine++]);
			var sentence = textSplittedByLine[currentLine++]; 
			var sentenceMeaning = textSplittedByLine[currentLine++];
			return new Word(id, kanjiNotations, kanaNotations, meanings, noKanjiTest, sentence, sentenceMeaning);
		}

		public static void AppendTo(StringBuilder sb, Word word)
		{
			sb.AppendLine(word.Id.ToString());
			StringHelper.AppendWithSemicolonTo(sb, word.KanjiNotations);
			sb.AppendLine();
			StringHelper.AppendWithSemicolonTo(sb, word.KanaNotations);
			sb.AppendLine();
			StringHelper.AppendWithSemicolonTo(sb, word.Meanings);
			sb.AppendLine();
			sb.AppendLine(word.NoKanjiTest.ToString());
			sb.AppendLine(word.Sentence);
			sb.AppendLine(word.SentenceMeaning);
		}
	}
}
