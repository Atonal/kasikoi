﻿using System.Text;

namespace Kasikoi.Data.Parsers
{
	static class KanjiParser
	{
		public static Kanji Parse(string[] textSplittedByLine, ref int currentLine)
		{
			var id = int.Parse(textSplittedByLine[currentLine++]);
			var kanji = textSplittedByLine[currentLine++];
			var descs = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
			return new Kanji(id, kanji, descs);
		}

		public static void AppendTo(StringBuilder sb, Kanji kanji)
		{
			sb.AppendLine(kanji.Id.ToString());
			sb.AppendLine(kanji.Character);
			StringHelper.AppendWithSemicolonTo(sb, kanji.Descriptions);
			sb.AppendLine();
		}
	}
}
