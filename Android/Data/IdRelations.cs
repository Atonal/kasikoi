﻿using System;
using System.Collections.Generic;

namespace Kasikoi.Data
{
	/// <summary>
	/// 연관된 id들을 관리하는 객체
	/// </summary>
	public class IdRelations
	{
		public void AddRelation(int keyId, int otherId)
		{
			if (!relationMap.ContainsKey(keyId))
			{
				relationMap.Add(keyId, new SortedSet<int>());
			}
			relationMap[keyId].Add(otherId);
		}

		public void RemoveId(int id)
		{
			throw new NotImplementedException();
		}

		public void RemoveRelation(int id, int anotherId)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<int> GetAllKeyIds()
		{
			return relationMap.Keys;
		}

		public IEnumerable<int> GetAllRelatedIdsWith(int keyId)
		{
			if (relationMap.ContainsKey(keyId))
				return relationMap[keyId];
			else
				return empty;
		}

		List<int> empty = new List<int>();
		Dictionary<int, SortedSet<int>> relationMap = new Dictionary<int, SortedSet<int>>();
	}
}
