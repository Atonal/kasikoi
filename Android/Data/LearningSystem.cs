﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Linq;

namespace Kasikoi.Data
{
	public partial class LearningSystem
	{
		#region singleton

		private LearningSystem()
		{
			problemSelector = new ProblemSelector(wordBook, wordRecords);
		}

		static LearningSystem instance;
		public static LearningSystem Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new LearningSystem();
					instance.Load();
				}
				return instance;
			}
		}

		public LearningSystem(string data)
		{
			var lines = data.Split(new string[]{ "\r\n", "\n" }, StringSplitOptions.None);
			LearningSystem.Parse(lines, this);
		}

		public static void ChangeInstance(LearningSystem system)
		{
			system.Save();
			Instance.Load();
		}

		#endregion

		#region save load

		private static string GetSavePath()
		{
            string path = PathHelper.SaveDirectory;
			string filename = Path.Combine(path, "kasikoi.txt");
			return filename;
		}

		public void Load()
		{
			var savePath = GetSavePath();
			if (!File.Exists(savePath))
			{
				problemSelector = new ProblemSelector(wordBook, wordRecords);
			}
			else
			{
                try
                {
				    var lines = File.ReadAllLines(savePath);
				    Parse(lines, this);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    logList.Clear();
                }
			}
		}

		public void Save()
		{
			RecordLog();
			var dataString = Serialize(this);
			File.WriteAllText(GetSavePath(), dataString);
		}

		#endregion

		#region word

		public Word AddWord(Word wordToBeAdded)
		{
			var wordAdded = wordBook.Add(wordToBeAdded);
			foreach (var kanjiNotation in wordToBeAdded.KanjiNotations)
				foreach (var kanji in kanjiNotation)
					kanjiCharacterToWord.Add(kanji.ToString(), wordAdded);
			recentlyAddedWord = wordAdded;
			return wordAdded;
		}

        public void EditWord(Word wordEdited)
        {
            wordBook.Edit(wordEdited);
            kanjiCharacterToWord.RemoveById(wordEdited.Id);
            foreach (var kanjiNotation in wordEdited.KanjiNotations)
                foreach (var kanji in kanjiNotation)
                    kanjiCharacterToWord.Add(kanji.ToString(), wordEdited);
            wordList[wordList.FindIndex(x => x.Id == wordEdited.Id)] = wordEdited;

        }

        public void RemoveWord(int wordId)
        {
            wordBook.Remove(wordId);
            kanjiCharacterToWord.RemoveById(wordId);
        }

        public Word GetWordWithId(int id)
        {
            return wordBook[id];
        }

		public IReadOnlyList<Word> GetWordWithKanjiNotation(string kanjiNotation)
		{
			return wordBook.GetWordWithKanjiNotation(kanjiNotation);
		}

		public IReadOnlyList<Word> GetWordWithKanaNotation(string kanaNotation)
		{
			return wordBook.GetWordWithKanaNotation(kanaNotation);
		}

		public IReadOnlyList<Word> GetWordWithKanjiCharacter(string kanjiCharacter)
		{
			return kanjiCharacterToWord[kanjiCharacter];
		}

		public IReadOnlyList<Word> GetWordWithKanjiCharacter(char kanjiCharacter)
		{
			return GetWordWithKanjiCharacter(kanjiCharacter.ToString());
		}

		public IReadOnlyList<Word> GetAllWords()
		{
			return wordBook.GetAllWords();
		}

		public int WordCount
		{
			get
			{
				return wordBook.WordCount;
			}
		}

		public Word RecentlyAddedWord
		{
			get
			{
				return recentlyAddedWord;
			}
		}

		#endregion

		#region kanji

		public Kanji AddKanji(Kanji kanji)
		{
			var addedKanji = kanjiBook.Add(kanji);
			return addedKanji;
		}

        public void ClearKanji()
        {
            kanjiBook.Clear();
        }

		public bool ContainsKanji(char character)
		{
			return kanjiBook.Contains(character);
		}

		public bool ContainsKanji(string kanji)
		{
			if (kanji.Length != 1) return false;

			return ContainsKanji(kanji[0]);
		}

		public Kanji GetKanji(char character)
		{
			return kanjiBook[character];
		}

		public Kanji GetKanji(string kanji)
		{
			return GetKanji(kanji[0]);
		}

		public IReadOnlyList<Kanji> GetAllKanjis()
		{
			return kanjiBook.GetAllKanjis();
		}

		public int KanjiCount
		{
			get
			{
				return kanjiBook.KanjiCount;
			}
		}

		#endregion

		#region learning works

		public Problem NextProblem()
		{
			return problemSelector.NextProblem();
		}

        public void MarkCorrect(Problem problem, DateTime now)
		{
			var id = problem.Word.Id;
			if (!wordRecords.ContainsKey(id))
			{
				wordRecords.Add(id, new WordRecord(id, now));
			}
			wordRecords[id].MarkCorrect(problem.Type, now);
            AddLearnCount();
		}

        public void MarkWrong(Problem problem, DateTime now)
		{
			var id = problem.Word.Id;
			if (!wordRecords.ContainsKey(id))
			{
				wordRecords.Add(id, new WordRecord(id, now));
			}
			wordRecords[id].MarkWrong(problem.Type, now);
            AddLearnCount();
		}

		public int GetEncounteredWord()
		{
			return wordRecords.Count;
		}

		public int GetMemorizedWord()
		{
			double count = 0;
			foreach (var record in wordRecords.Values)
			{
				count += Heuristics.MemorizedProbability(record);
			}
			return (int)count;
		}

		public string GetDescription(Word word)
		{
			var kanjis = word.ComposedKanji;
			var description = new StringBuilder();
			//TODO: 관련 단어 조회 후 추가
			foreach (var kanjiCharacter in kanjis)
			{
				if (ContainsKanji(kanjiCharacter))
				{
					var kanji = GetKanji(kanjiCharacter);
					description.Append(kanji.ToString());
					description.AppendLine();
					var sameKanjiWords = GetWordWithKanjiCharacter(kanjiCharacter);
					var viewIndices = RandomHelper.GetRandomIndices(sameKanjiWords.Count, 3);
					foreach (var i in viewIndices)
					{
						if (sameKanjiWords[i] == word)
							continue;

						description.Append("  ");
						sameKanjiWords[i].ToStringBuilder(description, 3);
						description.AppendLine();
					}
					description.AppendLine();
				}
			}
			return description.ToString();
		}

		public string GetKanjiDescription(Word word)
		{
			var kanjis = word.ComposedKanji;
			var description = new StringBuilder();
			foreach (var kanjiCharacter in kanjis)
			{
				if (ContainsKanji(kanjiCharacter))
				{
                    var kanji = GetKanji(kanjiCharacter);
                    description.Append(kanji.ToString());
                    description.AppendLine();
				}
			}
            var result = description.ToString().TrimEnd('\n', '\r');
			return result;
		}

		#endregion

		#region log

		public void RecordLog()
		{
			var memorized = GetMemorizedWord();
			var encounter = GetEncounteredWord();
            var learn = 0;
			var total = WordCount;
            if (logList.Count > 0)
            {
				var lastLog = logList[logList.Count - 1];
                if (lastLog.Date == DateTime.Today)
				{
                    learn = lastLog.LearnCount;
					logList.RemoveAt(logList.Count - 1);
				}
			}
            if (logList.Count > 0)
            {
                var lastLog = logList[logList.Count - 1];
                if ((int)((DateTime.Today - lastLog.Date).TotalDays) > 1)
                {
                    AddYesterdayLog();
                }
            }
            logList.Add(new Log(DateTime.Today, total, encounter, memorized, learn));
		}

        void AddYesterdayLog()
        {
            double memorized = 0;
            DateTime yesterday = DateTime.Today - TimeSpan.FromSeconds(1);
            foreach (var r in wordRecords.Values)
            {
                memorized += Heuristics.MemorizedProbability(r, yesterday);
            }
            var total = WordCount;
            var encounter = GetEncounteredWord();
            yesterday = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day);
            logList.Add(new Log(yesterday, total, encounter, (int)memorized, 0));
        }

        void AddLearnCount()
        {
            if (logList.Count == 0 ||
                logList[logList.Count - 1].Date != DateTime.Today)
            {
                RecordLog();
            }
            logList[logList.Count - 1].AddLearnCount();
        }

        public IReadOnlyList<Log> GetAllLogs()
        {
            return logList;
        }

		#endregion

        public DateTime LastCommand
        {
            get
            {
                return lastCommand;
            }
            set
            {
                lastCommand = value;
            }
        }

        public void ResetLastCommand()
        {
            lastCommand = DateTime.MinValue;
        }

        public IReadOnlyList<WordRecord> GetAllRecords()
        {
            return new List<WordRecord>(wordRecords.Values);
        }

        public bool IsWordHasRecord(int id)
        {
            return wordRecords.ContainsKey(id);
        }

        public WordRecord GetWordRecord(int id)
        {
            return wordRecords[id];
        }

        public List<Word> wordList = new List<Word>();

        public List<Word> RefreshWordList(string search = null, string sort = null, string filter = "전체 단어")
        {
            wordList.Clear();
            var wb = wordBook.GetAllWords();
            foreach (var w in wb)
            {
                if (search != null && StringHelper.ConcatenateWithSemicolon(w.KanjiNotations).Contains(search) == false
                    && StringHelper.ConcatenateWithSemicolon(w.KanaNotations).Contains(search) == false
                    && StringHelper.ConcatenateWithSemicolon(w.Meanings).Contains(search) == false)
                {
                    continue;
                }
                else if (filter == "학습 중인 단어" && !wordRecords.ContainsKey(w.Id))
                {
                     continue;
                }
                else if (filter == "학습 중이 아닌 단어" && wordRecords.ContainsKey(w.Id))
                {
                    continue;
                }
                else if (filter == "복습이 필요한 단어" && (!wordRecords.ContainsKey(w.Id) || wordRecords[w.Id].GetDaysAfterRecent() < 3))
                {
                    continue;
                }
                else if (filter == "복습이 시급한 단어" && (!wordRecords.ContainsKey(w.Id) || Heuristics.MemorizedProbability(wordRecords[w.Id]) > 0.2))
                {
                    continue;
                }
                wordList.Add(w);
            }
            switch (sort)
            {
                case "한자 오름차순 순":
                    wordList = wordList.OrderBy(x => StringHelper.ConcatenateWithSemicolon(x.KanjiNotations)).ToList();
                    break;
                case "가나 표기":
                    wordList = wordList.OrderBy(x => StringHelper.ConcatenateWithSemicolon(x.KanaNotations)).ToList();
                    break;
                case "최근에 학습한 단어":
                    wordList = wordList.OrderBy(x => 
                        {
                            if (IsWordHasRecord(x.Id))
                                return wordRecords[x.Id].GetDaysAfterRecent();
                            else
                                return 999;
                        }).ToList();
                    break;
                case "최근에 학습하지 않은 단어":
                    wordList = wordList.OrderByDescending(x => 
                        {
                            if (IsWordHasRecord(x.Id))
                                return wordRecords[x.Id].GetDaysAfterRecent();
                            else
                                return -1;
                        }).ToList();
                    break;
                case "암기 레벨 단기 순":
                    wordList = wordList.OrderBy(x =>
                        {
                            if (IsWordHasRecord(x.Id))
                                return wordRecords[x.Id].Level;
                            else
                                return 999;
                        }).ToList();
                    break;
                case "암기 레벨 장기 순":
                    wordList = wordList.OrderByDescending(x =>
                        {
                            if (IsWordHasRecord(x.Id))
                                return wordRecords[x.Id].Level;
                            else
                                return -1;
                        }).ToList();
                    break;
                case "복습 우선 순위 순":
                    wordList = wordList.OrderByDescending(x =>
                        {
                            if (IsWordHasRecord(x.Id))
                                return Heuristics.WordWeight(wordRecords[x.Id]);
                            else
                                return 999;
                        }).ToList();
                    break;
                default:
                    break;
            }

            return wordList;

        }


        string username = null;
        DateTime lastCommand = DateTime.MinValue;
		WordBook wordBook = new WordBook();
		KanjiBook kanjiBook = new KanjiBook();
		WordMap kanjiCharacterToWord = new WordMap();
		Word recentlyAddedWord;
		ProblemSelector problemSelector;
		Dictionary<int, WordRecord> wordRecords = new Dictionary<int, WordRecord>();
        List<Log> logList = new List<Log>();
	}
}
