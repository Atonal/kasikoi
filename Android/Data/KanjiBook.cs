﻿using System.Collections.Generic;

namespace Kasikoi.Data
{
	public class KanjiBook
	{
		public KanjiBook()
		{
		}

		public KanjiBook(IEnumerable<Kanji> kanjis)
		{
			foreach (var k in kanjis)
				AddKanji(k);
		}

		public Kanji Add(Kanji kanji)
		{
			while (idToKanjiMap.ContainsKey(idNextEmpty))
				idNextEmpty++;

			var kanjiWithId = new Kanji(idNextEmpty, kanji);
			AddKanji(kanjiWithId);
			idNextEmpty++;

			return kanjiWithId;
		}

        public void Clear()
        {
            idNextEmpty = 0;
            allKanjis.Clear();
            kanjiMap.Clear();
            idToKanjiMap.Clear();
        }

		void AddKanji(Kanji kanji)
		{
			allKanjis.Add(kanji);
			kanjiMap.Add(kanji.Character[0], kanji);
			idToKanjiMap.Add(kanji.Id, kanji);
		}

		public bool Contains(char kanjiCharacter)
		{
			return kanjiMap.ContainsKey(kanjiCharacter);
		}

		public Kanji this[char kanjiCharacter]
		{
			get
			{
				return kanjiMap[kanjiCharacter];
			}
		}

		public Kanji this[int id]
		{
			get
			{
				return idToKanjiMap[id];
			}
		}

		public int KanjiCount
		{
			get
			{
				return allKanjis.Count;
			}
		}

		public IReadOnlyList<Kanji> GetAllKanjis()
		{
			return allKanjis;
		}

		List<Kanji> allKanjis = new List<Kanji>();
		Dictionary<char, Kanji> kanjiMap = new Dictionary<char, Kanji>();
		Dictionary<int, Kanji> idToKanjiMap = new Dictionary<int, Kanji>();
		int idNextEmpty = 0;
	}
}
