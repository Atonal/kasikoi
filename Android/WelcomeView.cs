﻿using System;
using Android.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.InputMethodServices;
using System.Threading;
//using Kasikoi.Data;
using Android.Content;

namespace Kasikoi
{
	public class WelcomeView
	{
		MainActivity activity;

		bool loginState;
		bool registerState;

		Space spaceTop;
		TextView txtInstr;
		LinearLayout boxLogin;
		EditText etextId;
		EditText etextPw;
		EditText etextPwCheck;
		Button btnSubmit;
		TextView txtError;
		Button btnOldUser;
		Button btnNewUser;

		public WelcomeView(MainActivity activity)
		{
			this.activity = activity;
			activity.SetContentView(Resource.Layout.Welcome);

			loginState = false;
			registerState = false;

			spaceTop = activity.FindViewById<Space>(Resource.Id.spaceTop);
			txtInstr = activity.FindViewById<TextView>(Resource.Id.txtInstr);
			boxLogin = activity.FindViewById<LinearLayout>(Resource.Id.boxLogin);
			etextId = activity.FindViewById<EditText>(Resource.Id.etxtId);
			etextPw = activity.FindViewById<EditText>(Resource.Id.etxtPassword);
			etextPwCheck = activity.FindViewById<EditText>(Resource.Id.etxtPasswordCheck);
			btnSubmit = activity.FindViewById<Button>(Resource.Id.btnSubmit);
			txtError = activity.FindViewById<TextView>(Resource.Id.txtError);
			btnOldUser = activity.FindViewById<Button>(Resource.Id.btnOldUser);
			btnNewUser = activity.FindViewById<Button>(Resource.Id.btnNewUser);

            //etextId.PrivateImeOptions = "defaultInputmode=english";


			btnSubmit.Click += delegate
			{
                var view = activity.CurrentFocus;
                if (view != null)
                {
                    InputMethodManager manager = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                    manager.HideSoftInputFromWindow(view.WindowToken, 0);
                }
				if (loginState)
				{
					Login(etextId.Text, etextPw.Text);
					return;
				}
				else if (registerState)
				{
					Register(etextId.Text, etextPw.Text, etextPwCheck.Text);
					return;
				}
			};

			btnOldUser.Click += delegate
			{
				txtInstr.Text = "로그인하세요.";
				txtInstr.Visibility = ViewStates.Visible;
				boxLogin.Visibility = ViewStates.Visible;
				etextPwCheck.Text = "";
				etextPwCheck.Visibility = ViewStates.Gone;
				btnSubmit.Text = "다음";
				txtError.Visibility = ViewStates.Invisible;
				loginState = true;
				registerState = false;
			};

			btnNewUser.Click += delegate
			{
				txtInstr.Text = "아이디와 비밀번호를 등록하세요.";
				txtInstr.Visibility = ViewStates.Visible;
				boxLogin.Visibility = ViewStates.Visible;
				etextPwCheck.Visibility = ViewStates.Visible;
				btnSubmit.Text = "등록";
				txtError.Visibility = ViewStates.Invisible;
				registerState = true;
				loginState = false;
			};
		}

		bool isValid(string s)
		{
			foreach (char c in s) 
				if (!isValid (c))
					return false;
			return true;
		}

		bool isValid (char c)
		{
			return (c == '-' || c == '_' || 
					('0' <= c && c <= '9') || 
					('A' <= c && c <= 'Z') ||
					('a' <= c && c <= 'z'));
		}

		void Login(string id, string pw)
		{
			if (string.IsNullOrEmpty(id)) 
			{
				txtError.Text = "아이디를 입력하세요.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (!isValid (id)) 
			{
				txtError.Text = "아이디는 알파벳 대소문자와 숫자, _ - 만 사용하실 수 있습니다.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (id.Length < 4 || 20 < id.Length || pw.Length < 4 || 20 < pw.Length) 
			{
				txtError.Text = "아이디와 비밀번호는 최소 4자, 최대 20자여야 합니다.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (string.IsNullOrEmpty(pw))
			{
				txtError.Text = "비밀번호를 입력하세요.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
				
			boxLogin.Visibility = ViewStates.Gone;
			txtError.Visibility = ViewStates.Invisible;
			btnNewUser.Visibility = ViewStates.Invisible;
			btnOldUser.Visibility = ViewStates.Invisible;

			txtInstr.Text = "로그인중입니다...";

			if (LoginService.Login (id, pw)) {
				new MainPageView (activity);
			}
			else {
				txtInstr.Text = "아이디와 비밀번호를 등록하세요.";
				boxLogin.Visibility = ViewStates.Visible;
				txtError.Text = "로그인에 실패했습니다.";
				txtError.Visibility = ViewStates.Visible;
				btnNewUser.Visibility = ViewStates.Visible;
				btnOldUser.Visibility = ViewStates.Visible;
			}
		}

		void Register(string id, string pw, string pw2)
		{
			if (string.IsNullOrEmpty(id)) 
			{
				txtError.Text = "아이디를 입력하세요.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (!isValid (id)) 
			{
				txtError.Text = "아이디는 알파벳 대소문자와 숫자, _ - 만 사용하실 수 있습니다.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (id.Length < 4 || 20 < id.Length || pw.Length < 4 || 20 < pw.Length) 
			{
				txtError.Text = "아이디와 비밀번호는 최소 4자, 최대 20자여야 합니다.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (string.IsNullOrEmpty(pw))
			{
				txtError.Text = "비밀번호를 입력하세요.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (string.IsNullOrEmpty(pw2))
			{
				txtError.Text = "비밀번호를 확인해주세요.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}
			if (pw != pw2) 
			{
				txtError.Text = "비밀번호가 일치하지 않습니다.";
				txtError.Visibility = ViewStates.Visible;
				return;
			}

			boxLogin.Visibility = ViewStates.Gone;
			txtError.Visibility = ViewStates.Invisible;
			btnNewUser.Visibility = ViewStates.Invisible;
			btnOldUser.Visibility = ViewStates.Invisible;

			txtInstr.Text = "전송중입니다...";

			if (LoginService.Register (id, pw)) {
				new MainPageView (activity);
			}
			else {
				txtInstr.Text = "아이디와 비밀번호를 등록하세요.";
				boxLogin.Visibility = ViewStates.Visible;
				txtError.Text = "계정 등록에 실패했습니다.";
				txtError.Visibility = ViewStates.Visible;
				boxLogin.Visibility = ViewStates.Visible;
				btnNewUser.Visibility = ViewStates.Visible;
				btnOldUser.Visibility = ViewStates.Visible;
			}
		}
			
	}
}
