﻿using System;
using Android.Widget;
using Android.Views;
using System.Threading;
//using Kasikoi.Data;

namespace Kasikoi
{
	public class ImsiView
	{
		MainActivity activity;
		Button buttonLogout;

		public ImsiView(MainActivity activity)
		{
			this.activity = activity;
			activity.SetContentView (Resource.Layout.Imsi);
			buttonLogout = activity.FindViewById<Button> (Resource.Id.btnLogout);
			buttonLogout.Click += (object sender, EventArgs e) => {
				Logout();
			};
		}

		void Logout()
		{
			new WelcomeView(activity);
		}

	}

}
