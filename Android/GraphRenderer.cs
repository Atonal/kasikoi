﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.Graphics;

namespace Kasikoi
{
	public struct LineGraphData
	{
		public LineGraphData(string x, int y1, int y2)
		{
			X = x;
			Y1 = y1;
			Y2 = y2;
		}

		public string X;
		public int Y1;
		public int Y2;
	}

	public struct BarGraphData
	{
		public BarGraphData(string x, int y)
		{
			X = x;
			Y = y;
		}

		public string X;
		public int Y;
	}

	public struct ScatterGraphData
	{
		public ScatterGraphData(double dayAfterLearn, double probability)
		{
			DayAfterLearn = dayAfterLearn;
			Probability = probability;
		}

		public double DayAfterLearn;
		public double Probability;
	}

	public static class GraphRenderer
	{
		const int bitmapWidth = 500;
		const int bitmapHeight = 300;
		const int xAxisMargin = 20;
		const int yAxisMargin = 50;
		const int margin = 10;
		const int barWidth = 20;
		const int maxDays = 15;

		public static void RenderLineGraph(ImageView view, IReadOnlyList<LineGraphData> data, int auxDataCount)
		{
			Bitmap bitmap = Bitmap.CreateBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.Argb8888);
			Paint paint = new Paint();
			paint.AntiAlias = true;
			using (Canvas canvas = new Canvas(bitmap))
			{
				canvas.DrawARGB(255, 255, 255, 255);

				int yMax = 0;
				foreach (var d in data)
				{
					yMax = Math.Max(yMax, d.Y1);
					yMax = Math.Max(yMax, d.Y2);
				}
					
					
				paint.Color = Color.Black;
				paint.StrokeWidth = 1;
				int yUnit;
				int yPartition;
				GetYAxisParam(ref yMax, out yUnit, out yPartition);

				Func<int, int> toYPos = (yValue) =>
				{
					return bitmapHeight - xAxisMargin - margin - (int)((float)(bitmapHeight - xAxisMargin - margin * 2) * ((float)(yValue) / yMax));
				};

				Func<int, int> toXPos = (xIndex) =>
				{
					return yAxisMargin + (int)((float)(bitmapWidth - yAxisMargin - margin) * ((float)(xIndex + 1) / (data.Count + 1)));
				};

				for (int i = 0; i <= yPartition; i++)
				{
					int yPos = toYPos(i * yUnit);
					canvas.DrawLine(yAxisMargin - margin, yPos, bitmapWidth - margin, yPos, paint);
					yPos += 5;
					int yValue = (i * yUnit);
					if (yValue >= 1000)
						canvas.DrawText((yValue / 1000.0f) + "k", margin, yPos, paint);
					else
						canvas.DrawText(yValue.ToString(), margin, yPos, paint);
				}

				for (int i = -1; i <= data.Count; i++)
				{
					int xPos = toXPos(i);
					canvas.DrawLine(xPos, bitmapHeight - xAxisMargin, xPos, margin, paint);
					if (0 <= i && i < data.Count)
					{
						var textWidth = paint.MeasureText(data[i].X);
						canvas.DrawText(data[i].X, xPos - textWidth / 2, bitmapHeight - margin, paint);
					}
				}

				paint.StrokeWidth = 5;
				paint.Color = Color.CornflowerBlue;
				for (int i = 1; i < data.Count - auxDataCount; i++)
				{
					int startX = toXPos(i - 1);
					int stopX = toXPos(i);
					int startY = toYPos(data[i - 1].Y2);
					int stopY = toYPos(data[i].Y2);
					canvas.DrawLine(startX, startY, stopX, stopY, paint);
				}

				paint.Color = Color.Orange;
				for (int i = 1; i < data.Count - auxDataCount; i++)
				{
					int startX = toXPos(i - 1);
					int stopX = toXPos(i);
					int startY = toYPos(data[i - 1].Y1);
					int stopY = toYPos(data[i].Y1);
					canvas.DrawLine(startX, startY, stopX, stopY, paint);
				}

				var blue = Color.Orange;
				blue.A = 128;
				paint.Color = blue;
				for (int i = data.Count - auxDataCount; i < data.Count; i++)
				{
					int startX = toXPos(i - 1);
					int stopX = toXPos(i);
					int startY = toYPos(data[i - 1].Y1);
					int stopY = toYPos(data[i].Y1);
					canvas.DrawLine(startX, startY, stopX, stopY, paint);
				}
			}
			view.SetImageBitmap(bitmap);
		}

		public static void RenderBarGraph(ImageView view, IReadOnlyList<BarGraphData> data)
		{
			Bitmap bitmap = Bitmap.CreateBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.Argb8888);
			Paint paint = new Paint();
			paint.AntiAlias = true;
			using (Canvas canvas = new Canvas(bitmap))
			{
				canvas.DrawARGB(255, 255, 255, 255);

				int yMax = 0;
				foreach (var d in data)
				{
					yMax = Math.Max(yMax, d.Y);
				}
					
				paint.Color = Color.Black;
				paint.StrokeWidth = 1;
				int yUnit;
				int yPartition;
				GetYAxisParam(ref yMax, out yUnit, out yPartition);

				Func<int, int> toYPos = (yValue) =>
				{
					return bitmapHeight - xAxisMargin - margin - (int)((float)(bitmapHeight - xAxisMargin - margin * 2) * ((float)(yValue) / yMax));
				};

				Func<int, int> toXPos = (xIndex) =>
				{
					return yAxisMargin + (int)((float)(bitmapWidth - yAxisMargin - margin) * ((xIndex + 0.5f) / data.Count));
				};

				for (int i = 0; i <= yPartition; i++)
				{
					int yPos = toYPos(i * yUnit);
					canvas.DrawLine(yAxisMargin - margin, yPos, bitmapWidth - margin, yPos, paint);
					yPos += 5;
					int yValue = (i * yUnit);
					if (yValue >= 1000)
						canvas.DrawText((yValue / 1000.0f) + "k", margin, yPos, paint);
					else
						canvas.DrawText(yValue.ToString(), margin, yPos, paint);
				}

				canvas.DrawLine(yAxisMargin, bitmapHeight - xAxisMargin, yAxisMargin, margin, paint);
				canvas.DrawLine(bitmapWidth - margin, bitmapHeight - xAxisMargin, bitmapWidth - margin, margin, paint);
				for (int i = 0; i < data.Count; i++)
				{
					int xPos = toXPos(i);
					var textWidth = paint.MeasureText(data[i].X);
					canvas.DrawText(data[i].X, xPos - textWidth / 2, bitmapHeight - margin, paint);
				}

				paint.Color = Color.CornflowerBlue;
				for (int i = 0; i < data.Count; i++)
				{
					int xPos = toXPos(i);
					int yPos = toYPos(data[i].Y);
					int yBase = toYPos(0);
					Rect rect = new Rect(xPos - barWidth / 2, yPos, xPos + barWidth / 2, yBase);
					canvas.DrawRect(rect, paint);
				}
			}
			view.SetImageBitmap(bitmap);
		}

		public static void RenderScatterGraph(ImageView view, IReadOnlyList<ScatterGraphData> data)
		{
			Bitmap bitmap = Bitmap.CreateBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.Argb8888);
			Paint paint = new Paint();
			paint.AntiAlias = true;
			using (Canvas canvas = new Canvas(bitmap))
			{
				canvas.DrawARGB(255, 255, 255, 255);

				paint.Color = Color.Black;
				paint.StrokeWidth = 1;

				Func<double, int> toYPos = (yValue) =>
				{
					return bitmapHeight - xAxisMargin - margin - (int)((double)(bitmapHeight - xAxisMargin - margin * 2) * yValue);
				};

				Func<double, int> toXPos = (daysAfter) =>
				{
					return yAxisMargin + (int)((float)(bitmapWidth - yAxisMargin - margin) * daysAfter / maxDays);
				};

				var xBase = toXPos(0);
				var yBase = toYPos(0);
				canvas.DrawLine(xBase, yBase + margin, xBase, margin, paint);
				var days = new int[]{ 1, 3, 7, maxDays };
				for (int i = 0; i < days.Length; i++)
				{
					var xPos = toXPos(days[i]);
					canvas.DrawLine(xPos, yBase + margin, xPos, margin, paint);
					var textWidth = paint.MeasureText(days[i].ToString());
					canvas.DrawText(days[i].ToString(), xPos - textWidth / 2, bitmapHeight - margin, paint);
				}

				var prob = new int[]{ 100, 80, 50, 0 };
				for (int i = 0; i < prob.Length; i++)
				{
					var yPos = toYPos(prob[i] / 100.0);
					canvas.DrawLine(yAxisMargin - margin, yPos, bitmapWidth - margin, yPos, paint);
					canvas.DrawText(prob[i].ToString() + "%", margin, yPos + 5, paint);
				}

				var color = Color.CornflowerBlue;
				foreach (var d in data)
				{
					if (d.DayAfterLearn >= maxDays)
						continue;
					int xPos = toXPos(d.DayAfterLearn);
					int yPos = toYPos(d.Probability);

					color.A = 64;
					paint.Color = color;
					canvas.DrawCircle(xPos, yPos, 4, paint);
					canvas.DrawCircle(xPos, yPos, 2, paint);
					canvas.DrawCircle(xPos, yPos, 1, paint);
				}
			}
			view.SetImageBitmap(bitmap);
		}

		static void GetYAxisParam(ref int max, out int unit, out int partition)
		{
			int[] units = { 1, 2, 5, 10, 20, 25, 50, 100, 150, 200, 250, 500, 1000, 1250, 1500, 2000, 2500, 3000, 4000, 5000, 7500, 10000, 20000, 25000, 50000, 75000, 100000 };
			partition = 5;
			for (int i = 0; i < units.Length; i++)
			{
				if (max <= units[i] * partition)
				{
					max = units[i] * partition;
					unit = units[i];
					return;
				}
			}

			unit = 100000;
			partition = (int)(max + unit - 1) / unit;
			max = unit * partition;
		}
	}
}

