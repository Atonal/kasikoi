﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Android.Util;
using System.Diagnostics;

namespace Kasikoi
{
	public static class AjaxService
	{
		static readonly string ServerUrl = "http://143.248.233.58:2525/";

		public static string SendRequest(string jsonString, string requestUrl)
		{
			requestUrl.TrimStart('\\', '/');

			var url = ServerUrl + requestUrl;
			var request = WebRequest.Create(url);
			var byteArray = Encoding.UTF8.GetBytes(jsonString);
			Debug.WriteLine(string.Format("sending url='{0}' json='{1}'", url, jsonString));
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = byteArray.Length;
			using (var requestStream = request.GetRequestStream())
			{
				requestStream.Write(byteArray, 0, byteArray.Length);
			}

			var response = request.GetResponse();
			using (var responseStream = new StreamReader(response.GetResponseStream()))
			{
				var result = responseStream.ReadToEnd();
				Debug.WriteLine(result);
				return result;
			}
		}
	}
}

