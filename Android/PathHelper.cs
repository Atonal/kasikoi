﻿using System;

namespace Kasikoi
{
    public static class PathHelper
    {
#if DEBUG
        public static string SaveDirectory = Android.OS.Environment.ExternalStorageDirectory.Path;
#else
        public static string SaveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
#endif
        //for retail version: Environment.GetFolderPath(Environment.SpecialFolder.Personal)
    }
}

