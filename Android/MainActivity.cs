﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using System.Text;
using System.IO;
using Kasikoi.Data;
using Debug = System.Diagnostics.Debug;
using Android.Util;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;

namespace Kasikoi
{
	[Activity(Label = "Kasikoi", MainLauncher = true, Icon = "@drawable/icon", Theme = "@android:style/Theme.NoTitleBar")]
	public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

			//dummy layout
			SetContentView (Resource.Layout.Imsi);

            metrics = new DisplayMetrics();
            WindowManager.DefaultDisplay.GetMetrics(metrics);

            TryAutoLogin();
        }


        DisplayMetrics metrics;
        const int touchIndexUndefined = -1;
        float touchIndex = touchIndexUndefined;
        Queue<PointF> pointQueue = new Queue<PointF>();
        Queue<long> timeQueue = new Queue<long>();
        Stopwatch stopwatch = new Stopwatch();
        public override bool OnTouchEvent(MotionEvent e)
        {
            if (touchIndex == touchIndexUndefined)
            {
                if (e.Action == MotionEventActions.Down)
                {
                    stopwatch.Reset();
                    stopwatch.Start();
                    touchIndex = e.ActionIndex;
                    pointQueue.Clear();
                    pointQueue.Enqueue(new PointF(e.GetX(), e.GetY()));
                    timeQueue.Clear();
                    timeQueue.Enqueue(stopwatch.ElapsedMilliseconds);
                    return true;
                }
            }
            else if (touchIndex == e.ActionIndex)
            {
                long time = stopwatch.ElapsedMilliseconds;
                pointQueue.Enqueue(new PointF(e.GetX(), e.GetY()));
                timeQueue.Enqueue(time);
                while (timeQueue.Count > 0 && timeQueue.Peek() < time - 1000)
                {
                    pointQueue.Dequeue();
                    timeQueue.Dequeue();
                }

                if (e.Action == MotionEventActions.Cancel)
                {
                    touchIndex = touchIndexUndefined;
                    stopwatch.Stop();
                }
                else if (e.Action == MotionEventActions.Up)
                {
                    touchIndex = touchIndexUndefined;
                    stopwatch.Stop();

                    var startPoint = pointQueue.Peek();
                    var endX = e.GetX();
                    var endY = e.GetY();
                    var startX = startPoint.X;
                    var startY = startPoint.Y;
                    float minX, maxX;
                    float minY, maxY;
                    minX = maxX = startX;
                    minY = maxY = startY;
                    foreach (var p in pointQueue)
                    {
                        minX = Math.Min(minX, p.X);
                        maxX = Math.Max(maxX, p.X);
                        minY = Math.Min(minY, p.Y);
                        maxY = Math.Max(maxY, p.Y);
                    }
                    var dx = endX - startX;
                    var dy = endY - startY;
                    var angle = Math.Atan2(dy, dx) / Math.PI * 180;

                    minX = minX / metrics.Xdpi;
                    maxX = maxX / metrics.Xdpi;
                    startX = startX / metrics.Xdpi;
                    endX = endX / metrics.Xdpi;
                    minY = minY / metrics.Xdpi;
                    maxY = maxY / metrics.Xdpi;
                    startY = startY / metrics.Xdpi;
                    endY = endY / metrics.Xdpi;
                    const double dAngle = 40;
                    const double moveThresholdInInch = 0.2;
                    if (-dAngle <= angle && angle <= dAngle) // RIGHT
                    {
                        if (endX - startX > moveThresholdInInch && startX - minX < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe right");
                            SwipeRaised(SwipeDirection.Right);
                        }
                    }
                    else if (dAngle - dAngle <= angle && angle <= 90 + dAngle) // DOWN
                    {
                        if (endY - startY > moveThresholdInInch && startY - minY < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe down");
                            SwipeRaised(SwipeDirection.Down);
                        }
                    }
                    else if (180 - dAngle <= angle || angle <= -180 + dAngle) // LEFT
                    {
                        if (startX - endX > moveThresholdInInch && maxX - startX < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe left");
                            SwipeRaised(SwipeDirection.Left);
                        }
                    }
                    else if (-90 - dAngle <= angle && angle <= -90 + dAngle) // UP
                    {
                        if (startY - endY > moveThresholdInInch && maxY - startY < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe up");
                            SwipeRaised(SwipeDirection.Up);
                        }
                    }
                    Debug.WriteLine("dx={0}, dy={1}, angle={2}", dx, dy, angle);
                }
            }

            return base.OnTouchEvent(e);
        }

        public event SwipeHandler SwipeRaised = delegate {};

        public event Action BackButtonPressd = delegate {};

        public void MarkBackButtonHandled()
        {
            backButtonHandled = true;
        }

        bool backButtonHandled = false;
        public override void OnBackPressed()
        {
            backButtonHandled = false;
            BackButtonPressd();

            if (!backButtonHandled)
            {
                base.OnBackPressed();
            }
        }

        void TryAutoLogin()
        {
            bool autoLogin = false;
            try
            {
                autoLogin = LoginService.AutoLogin();
                if (autoLogin)
                {
                    new MainPageView (this);
                }
                else
                {
                    new WelcomeView(this);
                }
            }
            catch (WebException)
            {
                ShowAlertPopup("인터넷 접속이 불안정합니다. 다시시도합니다.", TryAutoLogin);
            }
        }

        protected override void OnStop()
        {
            if (LoginService.SessionId != -1)
                LearningSystem.Instance.Save();
            base.OnStop();
        }

        public void ShowAlertPopup(string content, Action onConfirmed)
        {
            var dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("알림");
            dialog.SetMessage(content);
            dialog.SetPositiveButton("확인", (sender, e) => { onConfirmed(); });
            dialog.Show();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data) 
        {     
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok) {
                if (data.GetBooleanExtra("reloadList", false) == true)
                {
                    new WordListView(this);
                }
            }
        }

    }

    public enum SwipeDirection
    {
        Up,
        Down,
        Left,
        Right
    }

    public delegate void SwipeHandler(SwipeDirection direction);



}


