﻿using System;
using Android.Widget;
using Android.Views.InputMethods;
using Android.Content;

namespace Kasikoi
{
    public class WordFilterView
    {
        MainActivity activity;

        EditText etxtSearch;
        Spinner spnSort;
        Spinner spnFilter;
        Button btnApply;
        Button btnCancel;


        public WordFilterView(MainActivity activity, string search = null, string sort = "한자 오름차순 순", string filter = "전체 단어")
        {
            this.activity = activity;    
            activity.SetContentView(Resource.Layout.WordFilter);

            etxtSearch = activity.FindViewById<EditText>(Resource.Id.etxtSearch);
            spnSort = activity.FindViewById<Spinner>(Resource.Id.spnSort);
            spnFilter = activity.FindViewById<Spinner>(Resource.Id.spnFilter);
            btnApply = activity.FindViewById<Button>(Resource.Id.btnApply);
            btnCancel = activity.FindViewById<Button>(Resource.Id.btnCancel);

            var adapterSort = ArrayAdapter.CreateFromResource(activity, Resource.Array.chooseSort, Android.Resource.Layout.SimpleSpinnerItem);
            adapterSort.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spnSort.Adapter = adapterSort;

            var adapterFilter = ArrayAdapter.CreateFromResource(activity, Resource.Array.chooseFilter, Android.Resource.Layout.SimpleSpinnerItem);
            adapterFilter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spnFilter.Adapter = adapterFilter;

            etxtSearch.Text = search;

            switch (sort) {
                case "한자 오름차순 순":
                    spnSort.SetSelection(0);
                    break;
                case "가나 표기":
                    spnSort.SetSelection(1);
                    break;
                case "최근에 학습한 단어":
                    spnSort.SetSelection(2);
                    break;
                case "최근에 학습하지 않은 단어":
                    spnSort.SetSelection(3);
                    break;
                case "암기 레벨 단기 순":
                    spnSort.SetSelection(4);
                    break;
                case "암기 레벨 장기 순":
                    spnSort.SetSelection(5);
                    break;
                case "복습 우선 순위 순":
                    spnSort.SetSelection(6);
                    break;
                default:
                    break;
            }

            switch (filter) {
                case "전체 단어":
                    spnFilter.SetSelection(0);
                    break;
                case "학습 중인 단어":
                    spnFilter.SetSelection(1);
                    break;
                case "학습 중이 아닌 단어":
                    spnFilter.SetSelection(2);
                    break;
                case "복습이 필요한 단어":
                    spnFilter.SetSelection(3);
                    break;
                case "복습이 시급한 단어":
                    spnFilter.SetSelection(4);
                    break;
                default:
                    break;
            }


            btnApply.Click += delegate
            {
                    var view = activity.CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager manager = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                        manager.HideSoftInputFromWindow(view.WindowToken, 0);
                    }

                    string searchTxt = etxtSearch.Text;
                    string sortTxt = spnSort.SelectedItem.ToString();
                    string filterTxt = spnFilter.SelectedItem.ToString();
                    new WordListView(activity, searchTxt, sortTxt, filterTxt);
            };

            btnCancel.Click += delegate
                {
                    var view = activity.CurrentFocus;
                    if (view != null)
                    {
                        InputMethodManager manager = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                        manager.HideSoftInputFromWindow(view.WindowToken, 0);
                    }

                    new WordListView(activity, search, sort, filter);
                };

            activity.BackButtonPressd += HandleBackButton;

        }

        void HandleBackButton()
        {
            activity.MarkBackButtonHandled();
            activity.BackButtonPressd -= HandleBackButton;
            btnCancel.CallOnClick();
        }


    }
}

