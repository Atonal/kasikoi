﻿using System;
using Android.Widget;
using Android.Views;
using Android.Graphics.Drawables;
using Android.Graphics;
using Kasikoi.Data;
using System.Text;
using System.Collections.Generic;

namespace Kasikoi
{
	public class MainPageView
	{
		MainActivity activity;

		Button btnLearningState;
		Button btnWordList;
		Button btnSetting;
		TextView txtTitle;
        TextView txtLearningInfo;
        ScrollView boxLearningState;
		Button btnStartLearning;
		Button btnAddWord;
		ImageView lineGraphTest;
		ImageView barGraphTest;
		ImageView scatterGraphTest;

		Color colorBtnSelect;
		Color colorBtnDeselect;

		public MainPageView(MainActivity activity)
		{
			this.activity = activity;
			activity.SetContentView(Resource.Layout.MainPage);

			btnLearningState = activity.FindViewById<Button>(Resource.Id.btnLearningState);
			btnWordList = activity.FindViewById<Button>(Resource.Id.btnWordList);
            btnSetting = activity.FindViewById<Button>(Resource.Id.btnLogout);
			txtTitle = activity.FindViewById<TextView>(Resource.Id.txtMainPage);
            txtLearningInfo = activity.FindViewById<TextView>(Resource.Id.txtLearningInfo);
            boxLearningState = activity.FindViewById<ScrollView>(Resource.Id.boxLearningState);
			btnStartLearning = activity.FindViewById<Button>(Resource.Id.btnStartLearning);
			btnAddWord = activity.FindViewById<Button>(Resource.Id.btnAddWord);
			lineGraphTest = activity.FindViewById<ImageView>(Resource.Id.lineGraphTest);
			barGraphTest = activity.FindViewById<ImageView>(Resource.Id.barGraphTest);
			scatterGraphTest = activity.FindViewById<ImageView>(Resource.Id.scatterGraphTest);

			colorBtnSelect = (btnLearningState.Background as ColorDrawable).Color;
			colorBtnDeselect = (btnWordList.Background as ColorDrawable).Color;


			btnWordList.Click += delegate
			{
                    new WordListView(activity);
			};

			btnSetting.Click += delegate
			{
				if (LoginService.Logout())
				{
					new WelcomeView(activity);
				}
			};
			btnStartLearning.Click += delegate
			{
                    if (LearningSystem.Instance.WordCount == 0)
                    {
                        activity.ShowAlertPopup("학습할 단어가 없습니다.", delegate {});
                    }
                    else
                    {
                        new LearningView(activity);
                    }
			};
			btnAddWord.Click += delegate
			{
                    new WordAddView(activity);
			};

            ReflectRecords();
		}

        void ReflectRecords()
        {
            SetMainInfo();
            SetLineGraph();
            SetBarGraph();
            SetScatterGraph();
        }

        void SetMainInfo()
        {
            var sb = new StringBuilder();
            AppendDaysAfter(sb);
            AppendConsecutiveLearn(sb);
            AppendRecentLearn(sb);
            AppendNewEncounter(sb);
            AppendCountSummary(sb);
            AppendMemory(sb);
            AppendUrgentReview(sb);
            txtLearningInfo.Text = sb.ToString();
        }

        void AppendCountSummary(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var memorized = system.GetMemorizedWord();
            var learning = system.GetEncounteredWord();
            var total = system.GetAllWords().Count;
            sb.AppendLine();
            sb.AppendLine("단어 수");
            sb.AppendLine(string.Format("- 암기:{0} / 학습 중:{1} / 전체:{2}", memorized, learning, total));
        }

        void AppendConsecutiveLearn(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var log = system.GetAllLogs();
            if (log.Count <= 1)
                return;

            var last = log[log.Count - 1];
            var today = DateTime.Today;
            var deltaDate = (int)((today - last.Date).TotalDays);
            if (deltaDate >= 2)
            {
                sb.AppendLine(string.Format("{0}일만에 오셨습니다.", deltaDate + 1));
            }

            for (int i = log.Count - 1; i >= 0; i--)
            {
                var l = log[i];
                if ((int)((today - l.Date).TotalDays) != deltaDate)
                {
                    break;
                }
                deltaDate++;
            }

            if (deltaDate >= 2)
            {
                sb.AppendLine(string.Format("{0}일 연속 학습 중입니다.", deltaDate));
            }
        }

        void AppendRecentLearn(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var log = system.GetAllLogs();
            if (log.Count <= 1)
                return;

            var last = log[log.Count - 1];
            if (last.LearnCount == 0)
                return;
            
            var today = DateTime.Today;
            var deltaDate = (int)((today - last.Date).TotalDays);
            var dateString = string.Format("{0}일 전", deltaDate);
            if (deltaDate == 0)
                dateString = "오늘";
            else if (deltaDate == 1)
                dateString = "어제";
            else if (deltaDate == 2)
                dateString = "그저께";

            sb.AppendLine(string.Format("{0} 학습을 {1}회 했습니다.", dateString, last.LearnCount));
        }

        void AppendUrgentReview(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var records = system.GetAllRecords();
            var urgentCount = 0;
            foreach (var r in records)
            {
                if (Heuristics.MemorizedProbability(r) < 0.2)
                    urgentCount++;
            }
            if (urgentCount >= 10)
            {
                sb.AppendLine(string.Format("복습이 시급한 단어가 {0}개 있습니다.", urgentCount));
            }
        }

        void AppendDaysAfter(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var log = system.GetAllLogs();
            if (log.Count == 0)
                return;

            var elapsed = (int)((DateTime.Today - log[0].Date).TotalDays);
            if (elapsed == 0)
                return;
            sb.AppendLine(string.Format("이 앱을 사용한지 {0}일 지났습니다.", elapsed));

            var used = 0;
            foreach (var l in log)
                if (l.LearnCount > 0)
                    used++;

            int missed = elapsed - used + 1;
            if (missed > 0)
                sb.AppendLine(string.Format("그 중 {0}일 학습을 빼먹었습니다.", missed));
        }

        void AppendNewEncounter(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var log = system.GetAllLogs();
            var weekBefore = 0;
            for (int i = log.Count - 1; i >= 0; i--)
            {
                if ((int)(DateTime.Today - log[i].Date).TotalDays >= 7)
                {
                    weekBefore = log[i].EncounteredWordCount;
                    break;
                }
            }
            var newEncounter = system.GetEncounteredWord() - weekBefore;
            if (newEncounter > 0)
            {
                sb.AppendLine(string.Format("최근 일주일간 {0}개의 단어를 새로 학습했습니다.", newEncounter));
            }
        }

        void AppendMemory(StringBuilder sb)
        {
            var system = LearningSystem.Instance;
            var record = system.GetAllRecords();
            int reviewMemory;
            double shortMemory, midMemory, longMemory;
            shortMemory = midMemory = longMemory = 0;
            reviewMemory = 0;
            foreach (var r in record)
            {
                var prob = Heuristics.MemorizedProbability(r);
                if (r.Level <= 3)
                {
                    shortMemory += prob;
                }
                else if (4 <= r.Level && r.Level <= 5)
                {
                    midMemory += prob;
                }
                else
                {
                    longMemory += prob;
                }
                if (prob <= Heuristics.MemoryProbabilityAtForgetStarts)
                {
                    reviewMemory++;
                }
            }

            sb.AppendLine(string.Format("- 단기 기억:{0} / 중기 기억:{1} / 장기 기억:{2}", (int)shortMemory, (int)midMemory, (int)longMemory));
            sb.AppendLine(string.Format("- 복습이 필요한 단어 수: {0}", reviewMemory));
        }

        void SetLineGraph()
        {
            var memorized = new int[7];
            var encountered = new int[7];
            var system = LearningSystem.Instance;
            var logs = system.GetAllLogs();
            var today = DateTime.Today;
            for (int i = 0; i < logs.Count; i++)
            {
                var l = logs[i];
                var daysBefore = (int)(today - l.Date).TotalDays;
                if (0 <= daysBefore && daysBefore < memorized.Length)
                {
                    memorized[daysBefore] = l.MemorizedWordCount;
                    encountered[daysBefore] = l.EncounteredWordCount;
                    if (i > 0)
                    {
                        var p = logs[i - 1];
                        var prevDaysBefore = (int)(today - p.Date).TotalDays;
                        for (int j = daysBefore + 1; j < prevDaysBefore && j < encountered.Length; j++)
                        {
                            if (encountered[j] > 0)
                                break;
                            var prevToDays = prevDaysBefore - daysBefore;
                            var jToDays = j - daysBefore;
                            var pAlpha = jToDays / (float)prevToDays;
                            memorized[j] = (int)(p.MemorizedWordCount * pAlpha + l.MemorizedWordCount * (1 - pAlpha));
                            encountered[j] = (int)(p.EncounteredWordCount * pAlpha + l.EncounteredWordCount * (1 - pAlpha));
                        }
                    }
                }
            }
            memorized[0] = system.GetMemorizedWord();
            encountered[0] = system.GetEncounteredWord();
            var data = new List<LineGraphData>();
            for (int i = memorized.Length - 1; i >= 0; i--)
            {
                var date = today - TimeSpan.FromDays(i);
                data.Add(new LineGraphData(date.ToString("MM/dd"), memorized[i], encountered[i]));
            }
            var records = system.GetAllRecords();
            for (int i = 1; i <= 3; i++)
            {
                var mCount = 0.0;
                var date = today + TimeSpan.FromDays(i);
                foreach (var r in records)
                {
                    mCount += Heuristics.MemorizedProbability(r, date);
                }
                data.Add(new LineGraphData(date.ToString("MM/dd"), (int)mCount, encountered[0]));
            }
            GraphRenderer.RenderLineGraph(lineGraphTest, data, 3);
        }

        void SetBarGraph()
        {
            int totalCount = 0;
            int thisMonthCount = 0;
            int lastWeekCount = 0;
            int thisWeekCount = 0;
            int[] dayCounts = new int[7];
            for (int i = 0; i < dayCounts.Length; i++)
                dayCounts[i] = 0;
            var today = DateTime.Today;
            var firstDayOfThisMonth = new DateTime(today.Year, today.Month, 1);
            var firstDayOfThisWeek = today - TimeSpan.FromDays((int)today.DayOfWeek);
            var firstDayOfLastWeek = firstDayOfThisWeek - TimeSpan.FromDays(7);
            var logs = LearningSystem.Instance.GetAllLogs();
            foreach (var l in logs)
            {
                totalCount += l.LearnCount;
                if (firstDayOfThisMonth <= l.Date)
                    thisMonthCount += l.LearnCount;
                if (firstDayOfLastWeek <= l.Date && l.Date < firstDayOfThisWeek)
                    lastWeekCount += l.LearnCount;
                if (firstDayOfThisWeek <= l.Date)
                    thisWeekCount += l.LearnCount;
                int elapsed = (int)(today - l.Date).TotalDays;
                if (0 <= elapsed && elapsed < dayCounts.Length)
                    dayCounts[elapsed] += l.LearnCount;
            }

            totalCount /= (int)(today - logs[0].Date).TotalDays + 1;
            thisMonthCount /= (int)(today - firstDayOfThisMonth).TotalDays + 1;
            lastWeekCount /= 7;
            thisWeekCount /= (int)(today - firstDayOfThisWeek).TotalDays + 1;
            GraphRenderer.RenderBarGraph(barGraphTest, new BarGraphData[]
                {
                    new BarGraphData("전체", totalCount),
                    new BarGraphData("이번달", thisMonthCount),
                    new BarGraphData("지난주", lastWeekCount),
                    new BarGraphData("이번주", thisWeekCount),
                    new BarGraphData("그저께", dayCounts[2]),
                    new BarGraphData("어제", dayCounts[1]),
                    new BarGraphData("오늘", dayCounts[0]),
                }
            );
        }

        void SetScatterGraph()
        {
            List<ScatterGraphData> data = new List<ScatterGraphData>();
            foreach (var r in LearningSystem.Instance.GetAllRecords())
            {
                var elapsedDays = (DateTime.Now - r.RecentTime).TotalDays;
                var prob = Heuristics.MemorizedProbability(r);
                data.Add(new ScatterGraphData(elapsedDays, prob));
            }
            GraphRenderer.RenderScatterGraph(scatterGraphTest, data);
        }
	}
}
