﻿using System;
using System.Collections.Generic;

namespace Kasikoi
{
	public static class RandomHelper
	{
		static Random rand = new Random();

		public static IEnumerable<int> GetRandomIndices(int indexLength, int count)
		{
			List<int> result = new List<int>();
			for (int i = 0; i < count; i++)
			{
				const int retry = 20;
				for (int j = 0; j < retry; j++)
				{
					int c = rand.Next(indexLength);
					foreach (var r in result)
						if (c == r) goto nextc;

					result.Add(c);
					break;
					nextc: ;
				}
			}

			return result;
		}

		public static int GetIndexWithWeight(IReadOnlyList<double> weight)
		{
			double totalSum = 0;
			for (int i = 0; i < weight.Count; i++)
				totalSum += weight[i];

			double r = rand.NextDouble() * totalSum;
			double partialSum = 0;
			for (int i = 0; i < weight.Count; i++)
			{
				partialSum += weight[i];
				if (r < partialSum)
					return i;
			}
			return weight.Count - 1;
		}

		public static bool EventWithProbability(double eventWeight, double otherWeight)
		{
			return rand.NextDouble() <= (eventWeight / (eventWeight + otherWeight));
		}

		public static bool EventWithProbability(double probabilityRatio)
		{
			return rand.NextDouble() <= probabilityRatio;
		}

		public static int Rand(int max)
		{
			return rand.Next(max);
		}
	}
}
