﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Kasikoi.Data;
using Android.Util;
using System.Drawing;
using Debug = System.Diagnostics.Debug;
using System.Diagnostics;

namespace Kasikoi
{
    [Activity(Label = "WordInfoActivity", Theme = "@android:style/Theme.NoTitleBar")]		
    public class WordInfoActivity : Activity
    {
        Button btnBack;
        Button btnEdit;
        Button btnPrevious;
        Button btnNext;
        TextView txtKanji;
        TextView txtKana;
        TextView txtMeaning;
        TextView txtExample;
        TextView txtExampleMeaning;
        TextView txtKanjiBook;
        TextView txtMemoLevel;
        TextView txtMemoProb;
        TextView txtWeight;
        TextView txtAnswerRate;
        TextView txtDateEncounter;
        TextView txtDateRecent;
        TextView txtDateForget;
        TextView txtPage;

        int wordOffset;
        Word word;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.WordInfo);

            btnBack = FindViewById<Button>(Resource.Id.btnBack);
            btnEdit = FindViewById<Button>(Resource.Id.btnEdit);
            btnPrevious = FindViewById<Button>(Resource.Id.btnPrevious);
            btnNext = FindViewById<Button>(Resource.Id.btnNext);
            txtKanji = FindViewById<TextView>(Resource.Id.txtKanji);
            txtKana = FindViewById<TextView>(Resource.Id.txtKana);
            txtMeaning = FindViewById<TextView>(Resource.Id.txtMeaning);
            txtExample = FindViewById<TextView>(Resource.Id.txtExample);
            txtExampleMeaning = FindViewById<TextView>(Resource.Id.txtExampleMeaning);
            txtKanjiBook = FindViewById<TextView>(Resource.Id.txtKanjiBook);
            txtMemoLevel = FindViewById<TextView>(Resource.Id.txtMemoLevel);
            txtMemoProb = FindViewById<TextView>(Resource.Id.txtMemoProb);
            txtWeight = FindViewById<TextView>(Resource.Id.txtWeight);
            txtAnswerRate = FindViewById<TextView>(Resource.Id.txtAnswerRate);
            txtDateEncounter = FindViewById<TextView>(Resource.Id.txtDateEncounter);
            txtDateRecent = FindViewById<TextView>(Resource.Id.txtDateRecent);
            txtDateForget = FindViewById<TextView>(Resource.Id.txtDateForget);
            txtPage= FindViewById<TextView>(Resource.Id.txtPage);

            wordOffset = Intent.GetIntExtra("wordOffset", 0);
            var LS = LearningSystem.Instance;
            word = LS.wordList[wordOffset];
            FillWordData(word);
            txtPage.Text = (wordOffset + 1) + "/" + LS.wordList.Count; 

            btnBack.Click += delegate
            {
                    Finish();
            };
            btnEdit.Click += delegate
            {
                    var editIntent = new Intent(this, typeof(WordEditActivity));
                    editIntent.PutExtra("wordId", word.Id);
                    StartActivityForResult(editIntent, 0);
            };
            btnNext.Click += delegate
            {
                    int newOffset = wordOffset + 1;
                    if (LS.wordList.Count - 1 < newOffset)
                    {
                        Toast.MakeText(ApplicationContext, "단어 목록의 끝입니다.", ToastLength.Short).Show();
                        return;
                    }
                    FillWordData(LS.wordList[newOffset]);
                    txtPage.Text = (newOffset + 1) + "/" + LS.wordList.Count; 
                    wordOffset = newOffset;
                    word = LS.wordList[wordOffset];
                    return;
            };

            btnPrevious.Click += delegate
            {
                    int newOffset = wordOffset - 1;
                    if (newOffset < 0)
                    {
                        Toast.MakeText(ApplicationContext, "단어 목록의 시작입니다.", ToastLength.Short).Show();
                        return;
                    }
                    FillWordData(LS.wordList[newOffset]);
                    txtPage.Text = (newOffset + 1) + "/" + LS.wordList.Count; 
                    wordOffset = newOffset;
                    word = LS.wordList[wordOffset];
                    return;
            };
            
            metrics = new DisplayMetrics();
            WindowManager.DefaultDisplay.GetMetrics(metrics);
            this.SwipeRaised += HandleSwipe;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data) 
        {     
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok) {
                if (data.GetIntExtra("edit0delete1", 0) == 0)
                {
                    var LS = LearningSystem.Instance;
                    word = LS.wordList[wordOffset];
                    FillWordData(word);
                    Intent resultIntent = new Intent(this, typeof(MainActivity));
                    resultIntent.PutExtra("reloadList", true);
                    SetResult(Result.Ok, resultIntent);
                }
                else if (data.GetIntExtra("edit0delete1", 0) == 1)
                {
                    Intent resultIntent = new Intent(this, typeof(MainActivity));
                    resultIntent.PutExtra("reloadList", true);
                    SetResult(Result.Ok, resultIntent);
                    Finish();
                }
            }
        }

        DisplayMetrics metrics;
        const int touchIndexUndefined = -1;
        float touchIndex = touchIndexUndefined;
        Queue<PointF> pointQueue = new Queue<PointF>();
        Queue<long> timeQueue = new Queue<long>();
        Stopwatch stopwatch = new Stopwatch();
        public override bool OnTouchEvent(MotionEvent e)
        {
            if (touchIndex == touchIndexUndefined)
            {
                if (e.Action == MotionEventActions.Down)
                {
                    stopwatch.Reset();
                    stopwatch.Start();
                    touchIndex = e.ActionIndex;
                    pointQueue.Clear();
                    pointQueue.Enqueue(new PointF(e.GetX(), e.GetY()));
                    timeQueue.Clear();
                    timeQueue.Enqueue(stopwatch.ElapsedMilliseconds);
                    return true;
                }
            }
            else if (touchIndex == e.ActionIndex)
            {
                long time = stopwatch.ElapsedMilliseconds;
                pointQueue.Enqueue(new PointF(e.GetX(), e.GetY()));
                timeQueue.Enqueue(time);
                while (timeQueue.Count > 0 && timeQueue.Peek() < time - 1000)
                {
                    pointQueue.Dequeue();
                    timeQueue.Dequeue();
                }

                if (e.Action == MotionEventActions.Cancel)
                {
                    touchIndex = touchIndexUndefined;
                    stopwatch.Stop();
                }
                else if (e.Action == MotionEventActions.Up)
                {
                    touchIndex = touchIndexUndefined;
                    stopwatch.Stop();

                    var startPoint = pointQueue.Peek();
                    var endX = e.GetX();
                    var endY = e.GetY();
                    var startX = startPoint.X;
                    var startY = startPoint.Y;
                    float minX, maxX;
                    float minY, maxY;
                    minX = maxX = startX;
                    minY = maxY = startY;
                    foreach (var p in pointQueue)
                    {
                        minX = Math.Min(minX, p.X);
                        maxX = Math.Max(maxX, p.X);
                        minY = Math.Min(minY, p.Y);
                        maxY = Math.Max(maxY, p.Y);
                    }
                    var dx = endX - startX;
                    var dy = endY - startY;
                    var angle = Math.Atan2(dy, dx) / Math.PI * 180;

                    minX = minX / metrics.Xdpi;
                    maxX = maxX / metrics.Xdpi;
                    startX = startX / metrics.Xdpi;
                    endX = endX / metrics.Xdpi;
                    minY = minY / metrics.Xdpi;
                    maxY = maxY / metrics.Xdpi;
                    startY = startY / metrics.Xdpi;
                    endY = endY / metrics.Xdpi;
                    const double dAngle = 40;
                    const double moveThresholdInInch = 0.2;
                    if (-dAngle <= angle && angle <= dAngle) // RIGHT
                    {
                        if (endX - startX > moveThresholdInInch && startX - minX < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe right");
                            SwipeRaised(SwipeDirection.Right);
                        }
                    }
                    else if (dAngle - dAngle <= angle && angle <= 90 + dAngle) // DOWN
                    {
                        if (endY - startY > moveThresholdInInch && startY - minY < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe down");
                            SwipeRaised(SwipeDirection.Down);
                        }
                    }
                    else if (180 - dAngle <= angle || angle <= -180 + dAngle) // LEFT
                    {
                        if (startX - endX > moveThresholdInInch && maxX - startX < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe left");
                            SwipeRaised(SwipeDirection.Left);
                        }
                    }
                    else if (-90 - dAngle <= angle && angle <= -90 + dAngle) // UP
                    {
                        if (startY - endY > moveThresholdInInch && maxY - startY < moveThresholdInInch)
                        {
                            Debug.WriteLine("Swipe up");
                            SwipeRaised(SwipeDirection.Up);
                        }
                    }
                    Debug.WriteLine("dx={0}, dy={1}, angle={2}", dx, dy, angle);
                }
            }

            return base.OnTouchEvent(e);
        }

        public event SwipeHandler SwipeRaised = delegate {};


        void HandleSwipe(SwipeDirection direction)
        {
            if (direction == SwipeDirection.Left)
            {
                btnNext.CallOnClick();
            }
            else if (direction == SwipeDirection.Right)
            {
                btnPrevious.CallOnClick();
            }
        }

        void FillWordData(Word word)
        {
            var LS = LearningSystem.Instance;
            txtKanji.Text = StringHelper.ConcatenateWithSemicolon(word.KanjiNotations);
            txtKana.Text = StringHelper.ConcatenateWithSemicolon(word.KanaNotations);
            txtMeaning.Text = StringHelper.ConcatenateWithSemicolon(word.Meanings);
            txtExample.Text = word.Sentence;
            txtExampleMeaning.Text = word.SentenceMeaning;
            txtKanjiBook.Text = LS.GetKanjiDescription(word);
            if (LS.IsWordHasRecord(word.Id))
            {
                var record = LS.GetWordRecord(word.Id);
                txtMemoLevel.Text = "암기 레벨: " + record.Level;

                txtMemoProb.Text = "암기 확률: " + Heuristics.MemorizedProbability(record).ToString("P2");
                txtWeight.Text = "복습 가중치: " + Heuristics.WordWeight(record).ToString("##.00");
                float rate = ((record.CorrectCount + record.WrongCount) == 0) ? 0 : (record.CorrectCount / (record.CorrectCount + record.WrongCount));
                txtAnswerRate.Text = "정답률: " + rate.ToString("P1");
                txtDateEncounter.Text = "암기 시작일: " + record.EncounterTime.ToShortDateString();
                txtDateRecent.Text = "최근 학습일: " + record.RecentTime.ToShortDateString() + " (" + record.GetDaysAfterRecent() + "일 경과)";
                double x, y;
                Heuristics.GetForgetCurve(record, out x, out y);
                txtDateForget.Text = "망각 시작일: " + record.RecentTime.AddDays(x).ToShortDateString();
            }
            else
            {
                txtMemoLevel.Text = "아직 학습되지 않은 단어입니다.";
                txtMemoProb.Text = "";
                txtWeight.Text = "";
                txtAnswerRate.Text = "";
                txtDateEncounter.Text = "";
                txtDateRecent.Text = "";
                txtDateForget.Text = "";
            }
        }


    }
}

