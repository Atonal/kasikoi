﻿using System;
using System.Runtime.Serialization;
using System.Json;
using System.IO;
using System.Diagnostics;
using Kasikoi.Data;
using System.Text;


namespace Kasikoi
{
    public static class SyncService
    {
        public static bool SynchonizeKanji()
        {
            string localTimestamp = (new DateTime(1990, 1, 1)).ToString("yyyy-MM-ddTHH:mm:ss.000Z");
            Debug.WriteLine(string.Format("date1 : {0}", localTimestamp));

            var filePath = LearningSystem.KanjiFilePath;
            if (File.Exists (filePath)) {
                localTimestamp = File.ReadAllLines(filePath)[0];
                Debug.WriteLine(string.Format("file : {0}", localTimestamp));
            }

            string json = "{ \"cmd\": \"KANJI\", \"timestamp\": \"" + localTimestamp + "\" }";

            string response = AjaxService.SendRequest (json, "ajax/kanji");
            JsonValue responseValue = JsonValue.Parse (response);
            JsonObject responseObject = responseValue as JsonObject;

            string remoteTimestamp = (string)responseObject["timestamp"];
            if (remoteTimestamp == localTimestamp)
            {
                return true;
            }

            if (!responseObject.ContainsKey("data"))
                return true;

            JsonObject dataObject = responseObject["data"] as JsonObject;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(remoteTimestamp);
            foreach (var x in dataObject)
            {
                sb.AppendLine(x.Key);
                sb.AppendLine(x.Value);
            }
            File.WriteAllText(filePath, sb.ToString());
            return true;
        }


        public static bool SynchronizeUserData(int sessionId)
        {
            var system = LearningSystem.Instance;
            string localTimestamp = system.LastCommand.ToString("yyyy-MM-ddTHH:mm:ss.000Z");
            Debug.WriteLine(string.Format("date1 : {0}", localTimestamp));

            string json = "{ \"cmd\": \"SYNC\", \"session\": \"" + sessionId + "\", \"timestamp\": \"" + localTimestamp + "\" }";
            string response = AjaxService.SendRequest (json, "ajax/sync");
            JsonValue responseValue = JsonValue.Parse (response);
            JsonObject responseObject = responseValue as JsonObject;

            var remoteTimestamp = DateTime.Parse((string)responseObject["timestamp"]) - TimeSpan.FromHours(9);
            if (remoteTimestamp <= system.LastCommand)
                return true;
            
            if (!responseObject.ContainsKey("data"))
                return true;

            JsonObject dataObject = responseObject["data"] as JsonObject;
            system.ParseJson(dataObject, remoteTimestamp, system);
            system.Save();
            return true;
        }
    }
}

