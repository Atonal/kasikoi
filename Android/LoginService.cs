﻿using System;
using System.Runtime.Serialization;
using System.Json;
using System.IO;
using System.Diagnostics;
using Kasikoi.Data;


namespace Kasikoi
{
    public static class LoginService
    {
        public static int SessionId { get; private set; }
        public static CommandService commandService;

        public static bool Logout()
        {
            int session_id;
            var sdCardPath = PathHelper.SaveDirectory;
            var filePath = Path.Combine(sdCardPath, "session.txt");
            if (!File.Exists(filePath))
            {
                return false;
            }
            var text = File.ReadAllText(filePath);
            session_id = Int32.Parse(text);

            string json = "{ \"cmd\": \"LOGOUT\", \"session\": " + session_id + "}";
            string response = AjaxService.SendRequest(json, "ajax/logout");
            JsonValue value = JsonValue.Parse(response);
            JsonObject result = value as JsonObject;

            if ((string)result["cmd"] != "LOGOUT")
            {
                return false;
            }
            if ((bool)result["result"] != true)
            {
                return false;
            }

            SessionId = -1;
            File.Delete(filePath);
            File.Delete(Path.Combine(sdCardPath, "kasikoi.txt"));
            LearningSystem.Instance.ResetLastCommand();

            return true;
        }

        public static bool AutoLogin()
        {
            var sdCardPath = PathHelper.SaveDirectory;
            var filePath = Path.Combine(sdCardPath, "session.txt");
            if (!File.Exists(filePath))
            {
                return false;
            }
            var text = File.ReadAllText(filePath);
            int sessionId = Int32.Parse(text);

            string json = "{ \"cmd\": \"AUTO_LOGIN\", \"session\": " + sessionId + "}";
            string response = AjaxService.SendRequest(json, "ajax/autoLogin");
            JsonValue value = JsonValue.Parse(response);
            JsonObject result = value as JsonObject;
  			
            if ((string)result["cmd"] == "ERROR")
            {
                return false;
            }

            if ((bool)result["result"] != true)
            {
                return false;
            }

            SyncData(sessionId);
            return true;
        }

        public static bool Login(string id, string pw)
        {
            string json = "{ \"cmd\": \"LOGIN\", \"id\": \"" + id + "\", \"pw\": \"" + pw + "\" }";
            string response = AjaxService.SendRequest(json, "ajax/login");
            JsonValue value = JsonValue.Parse(response);
            JsonObject result = value as JsonObject;

            if ((string)result["cmd"] != "LOGIN")
            {
                return false;
            }
            if ((bool)result["result"] != true)
            {
                return false;
            }

            int sessionId = (int)result["session"];
            SyncData(sessionId);
            return true;
        }

        public static bool Register(string id, string pw)
        {
            string json = "{ \"cmd\": \"REGISTER\", \"id\": \"" + id + "\", \"pw\": \"" + pw + "\" }";
            string response = AjaxService.SendRequest(json, "ajax/register");
            JsonValue value = JsonValue.Parse(response);
            JsonObject result = value as JsonObject;
            if ((string)result["cmd"] == "REGISTER")
            {
                if ((bool)result["result"] != true)
                {
                    return false;
                }
            }

            int sessionId = (int)result["session"];
            SyncData(sessionId);
            return true;
        }

        static void SyncData(int sessionId)
        {
            var filePath = Path.Combine(PathHelper.SaveDirectory, "session.txt");
            File.WriteAllText(filePath, sessionId.ToString());

            SyncService.SynchonizeKanji();
            LearningSystem.Instance.Load();
            SyncService.SynchronizeUserData(sessionId);

            SessionId = sessionId;

            commandService = new CommandService(sessionId);
        }
    }
}

