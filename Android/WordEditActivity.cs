﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Kasikoi.Data;

namespace Kasikoi
{
    [Activity(Label = "WordEditActivity", Theme = "@android:style/Theme.NoTitleBar")]			
    public class WordEditActivity : Activity
    {
        EditText etxtAddKanji;
        EditText etxtAddKana;
        EditText etxtAddMeaning;
        EditText etxtAddExample;
        EditText etxtAddExampleMeaning;
        CheckBox cbxNoKanji;
        LinearLayout boxExample1;
        LinearLayout boxExample2;
        LinearLayout boxExample3;
        LinearLayout boxExample4;
        TextView txtExample1;
        TextView txtExample2;
        TextView txtExample3;
        TextView txtExample4;

        List<LinearLayout> boxExamples = new List<LinearLayout>();
        List<TextView> txtExamples = new List<TextView>();

        Button btnEditWord;
        Button btnCancel;
        Button btnDelete;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.WordEdit);

            etxtAddKanji = FindViewById<EditText>(Resource.Id.etxtAddKanji);
            etxtAddKana = FindViewById<EditText>(Resource.Id.etxtAddKana);
            etxtAddMeaning = FindViewById<EditText>(Resource.Id.etxtAddMeaning);
            etxtAddExample = FindViewById<EditText>(Resource.Id.etxtAddExample);
            etxtAddExampleMeaning = FindViewById<EditText>(Resource.Id.etxtAddExampleMeaning);
            cbxNoKanji = FindViewById<CheckBox>(Resource.Id.cbxNoKanji);
            boxExample1 = FindViewById<LinearLayout>(Resource.Id.boxExample1);
            boxExample2 = FindViewById<LinearLayout>(Resource.Id.boxExample2);
            boxExample3 = FindViewById<LinearLayout>(Resource.Id.boxExample3);
            boxExample4 = FindViewById<LinearLayout>(Resource.Id.boxExample4);
            txtExample1 = FindViewById<TextView>(Resource.Id.txtExample1);
            txtExample2 = FindViewById<TextView>(Resource.Id.txtExample2);
            txtExample3 = FindViewById<TextView>(Resource.Id.txtExample3);
            txtExample4 = FindViewById<TextView>(Resource.Id.txtExample4);
            btnEditWord = FindViewById<Button>(Resource.Id.btnEditWord);
            btnCancel = FindViewById<Button>(Resource.Id.btnCancel);
            btnDelete = FindViewById<Button>(Resource.Id.btnDelete);

            boxExamples.Add(boxExample1);
            boxExamples.Add(boxExample2);
            boxExamples.Add(boxExample3);
            boxExamples.Add(boxExample4);
            txtExamples.Add(txtExample1);
            txtExamples.Add(txtExample2);
            txtExamples.Add(txtExample3);
            txtExamples.Add(txtExample4);

            boxExample1.Visibility = Android.Views.ViewStates.Gone;
            boxExample2.Visibility = Android.Views.ViewStates.Gone;
            boxExample3.Visibility = Android.Views.ViewStates.Gone;
            boxExample4.Visibility = Android.Views.ViewStates.Gone;

            int wordIdToEdit = Intent.GetIntExtra("wordId", 0);
            var LS = LearningSystem.Instance;
            Word wordToEdit = LS.GetWordWithId(wordIdToEdit);
                

            etxtAddKanji.Text = StringHelper.ConcatenateWithSemicolon(wordToEdit.KanjiNotations);
            etxtAddKana.Text = StringHelper.ConcatenateWithSemicolon(wordToEdit.KanaNotations);
            etxtAddMeaning.Text =  StringHelper.ConcatenateWithSemicolon(wordToEdit.Meanings);
            etxtAddExample.Text = wordToEdit.Sentence;
            etxtAddExampleMeaning.Text = wordToEdit.SentenceMeaning;
            cbxNoKanji.Checked = wordToEdit.NoKanjiTest;

            btnDelete.Click += delegate
            {
                    LS.RemoveWord(wordIdToEdit);
                    LoginService.commandService.DeleteWord(wordIdToEdit);
                    Toast.MakeText(ApplicationContext, "단어가 삭제되었습니다.", ToastLength.Short).Show();

                    Intent resultIntent = new Intent(this, typeof(WordEditActivity));
                    resultIntent.PutExtra("edit0delete1", 1);
                    SetResult(Result.Ok, resultIntent);
                    Finish();
            };

            btnEditWord.Click += delegate
                {

                    if (string.IsNullOrWhiteSpace(etxtAddKanji.Text) && !cbxNoKanji.Checked)
                    {
                        return;
                    }
                    if (string.IsNullOrWhiteSpace(etxtAddKana.Text))
                    {
                        return;
                    }
                    if (string.IsNullOrWhiteSpace(etxtAddMeaning.Text))
                    {
                        return;
                    }

                    var word = new Word(wordToEdit.Id,
                        StringHelper.SplitWithPunctuation(etxtAddKanji.Text),
                        StringHelper.SplitWithPunctuation(etxtAddKana.Text),
                        StringHelper.SplitWithPunctuation(etxtAddMeaning.Text),
                        cbxNoKanji.Checked,
                        etxtAddExample.Text,
                        etxtAddExampleMeaning.Text);

                    LS.EditWord(word);

                    LoginService.commandService.EditWord(word.Id,
                        etxtAddKanji.Text,
                        etxtAddKana.Text,
                        etxtAddMeaning.Text,
                        cbxNoKanji.Checked,
                        etxtAddExample.Text,
                        etxtAddExampleMeaning.Text);

                    Toast.MakeText(ApplicationContext, "단어가 수정되었습니다.", ToastLength.Short).Show();

                    Intent resultIntent = new Intent(this, typeof(WordEditActivity));
                    resultIntent.PutExtra("edit0delete1", 0);
                    SetResult(Result.Ok, resultIntent);
                    Finish();

                };
            btnCancel.Click += delegate
            {
                    Finish();
            };


        }
    }
}

