﻿using System;
using System.Collections.Generic;
using System.Json;
using Kasikoi.Data;
using System.Diagnostics;
using System.Threading;
using System.Net;

namespace Kasikoi
{
    public class CommandService
    {
        public CommandService(int sessionId)
        {
            this.sessionId = sessionId;
        }


        public void Learn(int wordId, ProblemType pType, DateTime now, bool memorized)
        {
            string pTypeString;
            if (pType == ProblemType.HideKanji)
                pTypeString = "KANJI";
            if (pType == ProblemType.HideKana)
                pTypeString = "KANA";
            else
                pTypeString = "MEANING";

            KeyValuePair<string, JsonValue> cmdPair = new KeyValuePair<string, JsonValue>("cmd", new JsonPrimitive("LEARN"));
            KeyValuePair<string, JsonValue> timestampPair = new KeyValuePair<string, JsonValue>("timestamp", new JsonPrimitive(now.ToString("yyyy-MM-ddTHH:mm:ss.000Z")));
            KeyValuePair<string, JsonValue> wordIdPair = new KeyValuePair<string, JsonValue>("wordId", new JsonPrimitive(wordId));
            KeyValuePair<string, JsonValue> pTypePair = new KeyValuePair<string, JsonValue>("problemType", new JsonPrimitive(pTypeString));
            KeyValuePair<string, JsonValue> memorizedPair = new KeyValuePair<string, JsonValue>("memorized", new JsonPrimitive(memorized));

            var mylist = new List<KeyValuePair<string, JsonValue>>{ cmdPair, timestampPair, wordIdPair, pTypePair, memorizedPair };

            JsonObject content = new JsonObject(mylist);

            Queue(new Command(now, content));

        }

        public void AddWord(int wordId, string kanji, string kana, string meaning, bool noKanjiTest, string example, string exampleMeaning)
        {


            var now = DateTime.Now;
            var cmdPair = new KeyValuePair<string, JsonValue>("cmd", new JsonPrimitive("ADD_WORD"));
            var timestampPair = new KeyValuePair<string, JsonValue>("timestamp", new JsonPrimitive(now.ToString("yyyy-MM-ddTHH:mm:ss.000Z")));
            var wordIdPair = new KeyValuePair<string, JsonValue>("wordId", new JsonPrimitive(wordId));
            var kanjiPair = new KeyValuePair<string, JsonValue>("kanji", new JsonPrimitive(kanji));
            var kanaPair = new KeyValuePair<string, JsonValue>("kana", new JsonPrimitive(kana));
            var meaningPair = new KeyValuePair<string, JsonValue>("meaning", new JsonPrimitive(meaning));
            var noKanjiTestPair = new KeyValuePair<string, JsonValue>("noKanjiTest", new JsonPrimitive(noKanjiTest));
            var examplePair = new KeyValuePair<string, JsonValue>("example", new JsonPrimitive(example));
            var exampleMeaningPair = new KeyValuePair<string, JsonValue>("exampleMeaning", new JsonPrimitive(exampleMeaning));

            var mylist = new List<KeyValuePair<string, JsonValue>>{ cmdPair, timestampPair, wordIdPair, kanjiPair, kanaPair, meaningPair, noKanjiTestPair, examplePair, exampleMeaningPair };
            JsonObject content = new JsonObject(mylist);

            Queue(new Command(now, content));

        }

        public void EditWord(int wordId, string kanji, string kana, string meaning, bool noKanjiTest, string example, string exampleMeaning)
        {
            var now = DateTime.Now;
            var cmdPair = new KeyValuePair<string, JsonValue>("cmd", new JsonPrimitive("EDIT_WORD"));
            var timestampPair = new KeyValuePair<string, JsonValue>("timestamp", new JsonPrimitive(now.ToString("yyyy-MM-ddTHH:mm:ss.000Z")));
            var wordIdPair = new KeyValuePair<string, JsonValue>("wordId", new JsonPrimitive(wordId));
            var kanjiPair = new KeyValuePair<string, JsonValue>("kanji", new JsonPrimitive(kanji));
            var kanaPair = new KeyValuePair<string, JsonValue>("kana", new JsonPrimitive(kana));
            var meaningPair = new KeyValuePair<string, JsonValue>("meaning", new JsonPrimitive(meaning));
            var noKanjiTestPair = new KeyValuePair<string, JsonValue>("noKanjiTest", new JsonPrimitive(noKanjiTest));
            var examplePair = new KeyValuePair<string, JsonValue>("example", new JsonPrimitive(example));
            var exampleMeaningPair = new KeyValuePair<string, JsonValue>("exampleMeaning", new JsonPrimitive(exampleMeaning));

            var mylist = new List<KeyValuePair<string, JsonValue>>{ cmdPair, timestampPair, wordIdPair, kanjiPair, kanaPair, meaningPair, noKanjiTestPair, examplePair, exampleMeaningPair };
            JsonObject content = new JsonObject(mylist);

            Queue(new Command(now, content));

        }

        public void DeleteWord(int wordId)
        {
            var now = DateTime.Now;
            var cmdPair = new KeyValuePair<string, JsonValue>("cmd", new JsonPrimitive("DELETE_WORD"));
            var timestampPair = new KeyValuePair<string, JsonValue>("timestamp", new JsonPrimitive(now.ToString("yyyy-MM-ddTHH:mm:ss.000Z")));
            var wordIdPair = new KeyValuePair<string, JsonValue>("wordId", new JsonPrimitive(wordId));

            var mylist = new List<KeyValuePair<string, JsonValue>>{ cmdPair, timestampPair, wordIdPair};
            JsonObject content = new JsonObject(mylist);

            Queue(new Command(now, content));

        }

        void Queue(Command cmd)
        {
            Debug.WriteLine("Command enqueued: " + cmd.getJsonString());

            LearningSystem.Instance.LastCommand = cmd.Timestamp;

            bool runThread = false;
            lock (commandQueue)
            {
                commandQueue.Enqueue(cmd);
                if (sendingThread == null)
                {
                    sendingThread = new Thread(SendCommand);
                    runThread = true;
                }
            }
            if (runThread)
                sendingThread.Start();
        }

        void SendCommand()
        {
            while (true)
            {
                var cmdList = new List<JsonValue>();
                lock (commandQueue)
                {
                    if (commandQueue.Count == 0)
                    {
                        sendingThread = null;
                        return;
                    }
                        
                    foreach (var cmd in commandQueue)
                    {
                        cmdList.Add(cmd.JsonObject);
                    }
                }
                var cmdArray = new JsonArray(cmdList);
                var cmdPair = new KeyValuePair<string, JsonValue>("cmd", new JsonPrimitive("COMMAND"));
                var sessionPair = new KeyValuePair<string, JsonValue>("session", new JsonPrimitive(this.sessionId));
                var commandsPair = new KeyValuePair<string, JsonValue>("commands", cmdArray);
                var jsonObject = new List<KeyValuePair<string, JsonValue>>{ cmdPair, sessionPair, commandsPair };
                var jsonString = new JsonObject(jsonObject).ToString();

                string response;
                try
                {
                    response = AjaxService.SendRequest(jsonString, "ajax/command");
                }
                catch (WebException)
                {
                    break;
                }

                var responseObject = JsonValue.Parse(response) as JsonObject;
                var timestamp = DateTime.Parse((string)responseObject["timestamp"]) - TimeSpan.FromHours(9) + TimeSpan.FromSeconds(1);
                lock (commandQueue)
                {
                    while (commandQueue.Count > 0)
                    {
                        var cmd = commandQueue.Peek();
                        if (cmd.Timestamp <= timestamp)
                            commandQueue.Dequeue();
                        else
                            break;
                    }
                }
            }

            sendingThread = null;
        }

        Queue<Command> commandQueue = new Queue<Command>();
        Thread sendingThread = null;
        int sessionId;
    }

    class Command
    {
        public Command(DateTime timestamp, JsonObject jsonObject)
        {
            JsonObject = jsonObject;
            Timestamp = timestamp;
        }

        public string getJsonString()
        {
            return JsonObject.ToString();
        }
            
        public readonly DateTime Timestamp;
        public readonly JsonObject JsonObject;
    }
}

