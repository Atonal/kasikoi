﻿using System;
using Android.Widget;
using Android.Views;
using Kasikoi.Data;
using Android.Content;

namespace Kasikoi
{
    public class LearningView
    {
        MainActivity activity;

        Button btnBack;
        Button btnEdit;

        TextView txtSendStatus;
        TextView txtKanji;
        TextView txtKana;
        TextView txtMeaning;
        TextView txtAnsKanji;
        LinearLayout boxWordWrap;

        CheckBox cbxMemorized;
        CheckBox cbxForgot;

        Button btnNextWord;

        bool isProblemState = false;
        Problem currentProblem;

        public LearningView(MainActivity activity)
        {
            this.activity = activity;
            activity.SetContentView(Resource.Layout.Learning);

            boxWordWrap = activity.FindViewById<LinearLayout>(Resource.Id.boxWordWrap);
            btnBack = activity.FindViewById<Button>(Resource.Id.btnBack);
            btnEdit = activity.FindViewById<Button>(Resource.Id.btnEdit);
            txtSendStatus = activity.FindViewById<TextView>(Resource.Id.txtSendStatus);
            txtKanji = activity.FindViewById<TextView>(Resource.Id.txtKanji);
            txtKana = activity.FindViewById<TextView>(Resource.Id.txtKana);
            txtMeaning = activity.FindViewById<TextView>(Resource.Id.txtMeaning);
            txtAnsKanji = activity.FindViewById<TextView>(Resource.Id.txtAnsKanji);
            cbxMemorized = activity.FindViewById<CheckBox>(Resource.Id.cbxMemorized);
            cbxForgot = activity.FindViewById<CheckBox>(Resource.Id.cbxForgot);
            btnNextWord = activity.FindViewById<Button>(Resource.Id.btnNextWord);

            btnEdit.Visibility = ViewStates.Invisible;

            btnBack.Click += delegate { MoveToMainPage(); };

            btnEdit.Click += delegate
            {
                    var editActivity = new Intent(activity, typeof(WordEditActivity));
                    editActivity.PutExtra("wordId", currentProblem.Word.Id);
                    activity.StartActivity(editActivity);
            };

            btnNextWord.Click += (object sender, EventArgs e) => {
                NextWord();
            };

            activity.SwipeRaised += HandleSwipe;
            activity.BackButtonPressd += HandleBackButton;
                        
            cbxMemorized.Click += CheckClicked;
            cbxForgot.Click += CheckClicked;
            currentProblem = null;
            NextWord();
        }

        void MoveToMainPage()
        {
            LearningSystem.Instance.Save();
            activity.SwipeRaised -= HandleSwipe;
            activity.BackButtonPressd -= HandleBackButton;
            new MainPageView(activity);
        }

        void HandleBackButton()
        {
            activity.MarkBackButtonHandled();
            MoveToMainPage();
        }

        void HandleSwipe(SwipeDirection direction)
        {
            if (direction == SwipeDirection.Left)
            {
                if (!isProblemState)
                {
                    btnNextWord.CallOnClick();
                }
            }
            else if (direction == SwipeDirection.Up)
            {
                cbxMemorized.CallOnClick();
            }
            else if (direction == SwipeDirection.Down)
            {
                cbxForgot.CallOnClick();
            }
        }

        void CheckClicked(object sender, EventArgs args)
        { 
            if (sender == cbxMemorized)
                boxWordWrap.SetBackgroundColor(new Android.Graphics.Color(0x80, 0xff, 0x83));
            else
                boxWordWrap.SetBackgroundColor(new Android.Graphics.Color(0xff, 0x80, 0x80));

            cbxMemorized.Checked = false;
            cbxForgot.Checked = false;
            (sender as CheckBox).Checked = true;

            if (isProblemState)
                ShowAnswer();

            btnEdit.Visibility = ViewStates.Visible;
        }

        void ShowAnswer()
        {
            isProblemState = false;
            btnNextWord.Enabled = true;

            var word = currentProblem.Word;
            var firstNotation = StringHelper.ConcatenateWithSemicolon(word.KanjiNotations);
            var secondNotation = StringHelper.ConcatenateWithSemicolon(word.KanaNotations);
            var meaning = StringHelper.ConcatenateWithSemicolon(word.Meanings);
            if (string.IsNullOrWhiteSpace(firstNotation))
            {
                firstNotation = secondNotation;
                secondNotation = "";
            }
            txtKanji.Text = firstNotation;
            txtKana.Text = secondNotation;
            txtMeaning.Text = meaning;

            var system = LearningSystem.Instance;
            var kanjiDesc = system.GetKanjiDescription(word);
            if (!string.IsNullOrEmpty(kanjiDesc))
            {
                txtAnsKanji.Text = kanjiDesc;
                txtAnsKanji.Visibility = ViewStates.Visible;
            }
        }


        void NextWord()
        {
            var system = LearningSystem.Instance;
            if (currentProblem != null)
            {
                DateTime now = DateTime.Now;
                if (cbxMemorized.Checked)
                {
                    system.MarkCorrect(currentProblem, now);
                    LoginService.commandService.Learn(currentProblem.Word.Id, currentProblem.Type, now, true);
                }
                else
                {
                    system.MarkWrong(currentProblem, now);
                    LoginService.commandService.Learn(currentProblem.Word.Id, currentProblem.Type, now, false);
                }
            }
            var problem = system.NextProblem();
            var word = problem.Word;
            var firstNotation = StringHelper.ConcatenateWithSemicolon(word.KanjiNotations);
            var secondNotation = StringHelper.ConcatenateWithSemicolon(word.KanaNotations);
            var meaning = StringHelper.ConcatenateWithSemicolon(word.Meanings);
            if (problem.Type == ProblemType.HideKana)
            {
                secondNotation = "■";
            }
            else if (problem.Type == ProblemType.HideKanji)
            {
                firstNotation = "■";
            }
            else if (problem.Type == ProblemType.HideMeaning)
            {
                meaning = "■";
            }
            if (string.IsNullOrWhiteSpace(firstNotation))
            {
                firstNotation = secondNotation;
                secondNotation = "";
            }

            txtKanji.Text = firstNotation;
            txtKana.Text = secondNotation;
            txtMeaning.Text = meaning;
            txtAnsKanji.Visibility = ViewStates.Invisible;
            btnEdit.Visibility = ViewStates.Invisible;

            cbxMemorized.Checked = false;
            cbxForgot.Checked = false;
            isProblemState = true;
            btnNextWord.Enabled = false;
            currentProblem = problem;

            boxWordWrap.SetBackgroundColor(Android.Graphics.Color.White);
        }


    }
}

