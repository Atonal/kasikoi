﻿Windows 서버 설치 조건
- Python 2.7 (64bit)
- pip, setuptools
- 환경 변수 PATH에 Python 설치 폴더와 Scripts 폴더 등록
(최신 Python 2.7 windows installer에서는 위 사항을 한꺼번에 처리해주는 듯)

Step 0. (최초 실행시) Execute install.bat (Flask 등 필요 라이브러리 설치)

Step 1. Execute execute.bat

Step 2. Connect to 127.0.0.1:5000

Step 3. Work Time!

(다시 setup하는 경우 venv 폴더를 지우고 Step 0 부터 진행)
