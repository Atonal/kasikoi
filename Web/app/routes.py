# Created by Jinsung Lee, Joo Dong Hyun
# Flask Main 
import json
import time
from datetime import datetime
from flask import Flask, render_template, request, jsonify, Response, send_file
from sqlalchemy.orm.exc import NoResultFound

from database import db_session
from models import User
from common import datetime_to_jsonstr as dtjson, jsonstr_to_datetime as jsondt
from service.session import new_session, find_session
from service.user import get_user, create_new_user, NewUserResult
from service.login import login, autologin, logout
from service.learning import kanji_dictionary_timestamp, kanji_dictionary,\
    user_data_timestamp, user_data,\
    command_learn, command_addword, command_editword, command_deleteword,\
    init_data, wordbook_import, wordbook_export

app = Flask(__name__)
ERROR_DATA = {'cmd': "ERROR", 'reason': "InvalidateSession", 'invalidateSession': True}

def json_response(json_data):
    return Response(response=json.dumps(json_data),
        mimetype='application/json',
        status=200)

@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()

@app.route('/')
def home():
  return render_template('home.html')

@app.route('/_add_numbers')
def add_numbers():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    return jsonify(result=a + b)

@app.route('/body/study')
def body_study():
    return render_template('study.html')

@app.route('/body/wordadd')
def body_wordadd():
    return render_template('wordadd.html')

@app.route('/body/myWordBook')
def body_myWordBook():
    return render_template('myWordBook.html')

@app.route('/body/wordedit')
def body_wordedit():
    return render_template('wordedit.html')

@app.route('/body/wordBookList')
def body_wordBookList():
    return render_template('wordBookList.html')

@app.route('/body/login')
def body_login():
    return render_template('login.html')

@app.route('/body/main')
def body_main():
    return render_template('main.html')

@app.route('/body/sidebar')
def body_sidebar():
    return render_template('sidebar.html')

@app.route('/body/members')
def body_members():
    return render_template('members.html')

@app.route('/body/information')
def body_information():
    return render_template('information.html')

@app.route('/body/notice')
def body_notice():
    return render_template('notice.html')

@app.route('/body/mobile')
def body_mobile():
    return render_template('tempMobilePage.html')




@app.route('/query_test')
def query_test():
    data = list()
    for row in db_session.query(User).all():
        data.append({'user_index': row.user_index, 'user_name': row.user_name})
    
    return json_response(data)

@app.route('/ajax/login', methods=['POST'])
def loginUser():
    logindata = request.get_json()
    inputId = logindata.get("id")
    inputPwd = logindata.get("pw")

    loginresult = login(inputId, inputPwd)
    data = {'cmd': "LOGIN", 'result': loginresult[0]}
    if loginresult[0] == True:
        data['session'] = loginresult[1]
    else:
        data['reason'] = loginresult[2]
    return json_response(data)

@app.route('/ajax/register', methods=['POST'])
def registUser():
    logindata = request.get_json()
    inputId = logindata.get("id")
    inputPwd = logindata.get("pw")

    registresult = create_new_user(inputId, inputPwd)
    data = {'cmd': "REGISTER", 'result': registresult == NewUserResult.Success}

    if registresult == NewUserResult.Success:
        loginresult = login(inputId, inputPwd)
        if loginresult[0] == True:
            data['session'] = loginresult[1]
        else:
            data['reason'] = loginresult[2]
    elif registresult == NewUserResult.Duplicated:
        data['reason'] = "ID_ALREADY_EXISTS"
    else:
        data['reason'] = "UNKNOWN_ERROR"

    return json_response(data)

@app.route('/ajax/autoLogin', methods=['POST'])
def autoLoginUser():
    logindata = request.get_json()
    session = logindata.get("session")

    try:
        autologin(session)
        data = {'cmd': "AUTO_LOGIN", 'result': True}
    except Exception, e:
        print session, e
        data = ERROR_DATA
    finally:
        return json_response(data)

@app.route('/ajax/logout', methods=['POST'])
def LogoutUser():
    logindata = request.get_json()
    session = logindata.get("session")

    try:
        logout(session)
        data = {'cmd': "LOGOUT", 'result': True}
    except Exception, e:
        print session, e
        data = ERROR_DATA
    finally:
        return json_response(data)

@app.route('/ajax/kanji', methods=['POST'])
def KanjiSync():
    receivedData = request.get_json()
    timestamp = receivedData.get("timestamp")

    req_dt = jsondt(timestamp)
    cur_dt = kanji_dictionary_timestamp()

    data = {'cmd': "KANJI", 'timestamp': dtjson(cur_dt)}
    if req_dt < cur_dt:
        kanji_data = kanji_dictionary()
        data['data'] = kanji_data

    return json_response(data)
        

@app.route('/ajax/sync', methods=['POST'])
def UserSync():
    receivedData = request.get_json()
    session = receivedData.get("session")
    timestamp = receivedData.get("timestamp")

    try:
        req_dt = jsondt(timestamp)
        sync_dt = user_data_timestamp(session)

        data = {'cmd' : "SYNC", 'timestamp': dtjson(sync_dt)}
        if req_dt < sync_dt:
            userdata = user_data(session, sync_dt)
            data['data'] = userdata
    except NoResultFound, e:
        print session, e
        data = ERROR_DATA
    finally:
        return json_response(data)

@app.route('/ajax/command', methods=['POST'])
def UserCommand():
    receivedData = request.get_json()
    session = receivedData.get("session")
    commands = receivedData.get("commands")
    print 'UserCommand1: session: %s,\tcommands: %d' % (session, len(commands))

    try:
        user_index = find_session(session)
        user = get_user(user_index)
    except Exception, e:
        print e
        data = ERROR_DATA
        return json_response(data)

    done_cmd_count = 0
    last_cmd_dt = user.last_command
    for c in commands:
        cmd = c.get('cmd')
        cmd_dt = jsondt(c.get('timestamp'))
        if cmd_dt <= last_cmd_dt:
            continue

        if cmd == "LEARN":
            command_learn(user, cmd_dt, c.get('wordId'), c.get('problemType'), c.get('memorized'))
        elif cmd == "ADD_WORD":
            command_addword(user, cmd_dt, c.get('wordId'),\
                c.get('kanji', None), c.get('kana'), c.get('meaning', None),\
                c.get('noKanjiTest', None), c.get('example', None), c.get('exampleMeaning', None))
        elif cmd == "EDIT_WORD":
            command_editword(user, cmd_dt, c.get('wordId'),\
                c.get('kanji', None), c.get('kana', None), c.get('meaning', None),\
                c.get('noKanjiTest', None), c.get('example', None), c.get('exampleMeaning', None))
        elif cmd == "DELETE_WORD":
            command_deleteword(user, cmd_dt, c.get('wordId'))
        
        last_cmd_dt = cmd_dt
        done_cmd_count += 1

    if user.last_command < last_cmd_dt:
        user.last_command = last_cmd_dt
        db_session.add(user)
        db_session.commit()

    data = {'cmd': "COMMAND", 'timestamp': dtjson(user.last_command)}
    print 'UserCommand2: session: %s,\tcommands: %d,\ttimestamp: %s' % (session, done_cmd_count, data['timestamp'])
    return json_response(data)

@app.route('/ajax/initialize', methods=['POST'])
def WordbookInitinalize():
    receivedData = request.get_json()
    session = receivedData.get("session")
    try:
        user_index = find_session(session)
        user = get_user(user_index)
        init_data(user_index)
    except Exception, e:
        data = ERROR_DATA
        return json_response(data)

    user.last_command = datetime.now()
    db_session.add(user)
    db_session.commit()
    data = {'cmd': "INITIALIZE", 'timestamp': dtjson(user.last_command)}
    return json_response(data)

@app.route('/ajax/import/<session>', methods=['POST'])
def WordbookImport(session):
    session = int(session)
    wordfile = request.files['wordfile']
    try:
        user_index = find_session(session)
        user = get_user(user_index)
        words = wordbook_import(user_index, wordfile)
    except Exception, e:
        data = ERROR_DATA
        return json_response(data)

    user.last_command = datetime.now()
    db_session.add(user)
    db_session.commit()
    data = {'cmd': "IMPORT", 'timestamp': dtjson(user.last_command), 'words': words}
    return json_response(data)

@app.route('/ajax/export', methods=['POST'])
def WordbookExport():
    receivedData = request.get_json()
    session = receivedData.get("session")
    try:
        user_index = find_session(session)
        user = get_user(user_index)
        filetext = wordbook_export(user_index)
    except Exception, e:
        data = ERROR_DATA
        return json_response(data)

    response = Response(filetext, mimetype='text/csv')
    response.headers["Content-Disposition"] = "attachment; filename=wordbook_%s.csv" % user.user_name
    return response

@app.route('/ajax/appdownload', methods=['POST'])
def AppDownload():
    return send_file("data/com.cs408.kasikoi.apk",  as_attachment=True)

@app.route('/ajax/testEcho', methods=['POST'])
def TestEcho():
    data = request.get_json()
    print data
    return json_response(data)

@app.route('/new_session')
def req_new_session():
    session_id = new_session(request.args.get('user_index', 0, type=int))
    data = {'session_id': session_id}
    return json_response(data)

if __name__ == '__main__':
    from admin import admin
    app.register_blueprint(admin)
    app.run(host='0.0.0.0', port=2525, debug=True)
