var kanjiDictionary;
var usrData;
var wordDictionary;

$(function() 
{
  $('#modifyButton').bind('click', function() 
  {
     modifyWord();
  });

  $('#removeButton').bind('click', function() 
  {
    deleteWord();
  });

  

  $('#cancelButton').bind('click', cancle); 

  usrData = getUserDataCookie();
  kanjiDictionary = getKanjiDataCookie();
  wordDictionary = usrData.learningPool;
  //console.log(wordDictionary);

  var word = wordDictionary[currentModifyingWordId];

  $('input[id="KanjiInput"]').val(word[0]);
  $('input[id="KanaInput"]').val(word[1]);
  $('input[id="meaningInput"]').val(word[2]);
  $('input[id="exampleJapInput"]').val(word[4]);
  $('input[id="exampleKorInput"]').val(word[5]);
  if(word[3] == true)
  	$('input[id="checkiz"]').val("on");

  findKanji();
  //findKana();
  //wordDictionary =

});


function findKanji()
{
  
  var kanjiword = $('input[id="KanjiInput"]').val();
  $(".includeContext").empty();
  var kanjiarray = kanjiword.split(';');

  var numOfKanji = 0;
  var addedKanji = new Array();
  for(var i=0;i<kanjiarray.length;i++)
  {
    var word = kanjiarray[i];

    for(var j=0;j<word.length;j++)
    {
      var singleKanji = word.charAt(j);

      if((singleKanji in kanjiDictionary)
        && 0 > $.inArray(singleKanji, addedKanji))
      {
        numOfKanji = $(".includeKanji").length;
        if(numOfKanji > 3)
          break;

        $(".includeContext").prepend("<p class=\"includeKanji\">["
          +singleKanji+"]"+kanjiDictionary[singleKanji] +"</p>");
        addedKanji.push(singleKanji);
      }
      
    }
  }

}


/*
function findKana()
{
  
  var kanaword = $('input[id="KanaInput"]').val();
  $(".includeContext2").empty();
  if(kanaword == "")
    return;
  var numOfWord = 0;

  var addedWord = new Array();
  for(var wordIdx in wordDictionary)
  {
    var word = wordDictionary[wordIdx];
    if(word[1].indexOf(kanaword) > -1
      && 0 > $.inArray(wordIdx, addedWord))
    {  
      var cln = $(".similarWord").first().clone();
      cln.removeAttr( "style" );
      cln.find("a.includeKana").text(word[0]+"["+word[1]+"]:"+word[2]);
      $(".includeContext2").append(cln);

      addedWord.push(wordIdx);
      numOfWord = $(".includeKana").length;
      if(numOfWord > 4)
        break;
    }
  }

}
*/

function findSimillarWord()
{
  var kanaword = $('input[id="KanaInput"]').val();
  var kanjiword = $('input[id="KanjiInput"]').val();
  $(".includeContext2").empty();

  $('input[id="KanaInput"]').css("background-color","White");
  $('input[id="KanjiInput"]').css("background-color","White");
  if(kanaword == "" && kanjiword == "")
    return;

  var numOfWord = 0;

  var addedWord = new Array();
  for(var wordIdx in wordDictionary)
  {
    var word = wordDictionary[wordIdx];
    if((word[1].indexOf(kanaword) > -1 
        || (word[0] != null && word[0] != undefined && word[0].indexOf(kanjiword)))
      && 0 > $.inArray(wordIdx, addedWord))
    {
      var kanjiList;
      if(word[0] == null || word[0] == undefined)
        kanjiList = [];
      else
        kanjiList = word[0].split(';'); 
      var kanaList = word[1].split(';');

      var isSimilar = false;
      for(var i=0;i<kanjiList.length;i++)
      {
        if(kanjiList[0] == kanjiword)
        {
          $('input[id="KanjiInput"]').css("background-color","SkyBlue");
          isSimilar = true;
          break;
        }
      }

      for(var i=0;i<kanaList.length;i++)
      {
        if(kanaList[0] == kanaword)
        {
          $('input[id="KanaInput"]').css("background-color","SkyBlue");
          isSimilar = true;
          break;
        }
      }

      if(!isSimilar)
        continue;

      var cln = $(".similarWord").first().clone();
      cln.removeAttr( "style" );
      cln.find("a.includeKana").text(word[0]+"["+word[1]+"]:"+word[2]);
      cln.find(".editLinkButton").attr( "value", wordIdx );
      cln.find(".editLinkButton").bind("click", function() 
      {
        var id = $(this).attr("value");
        moveToModifyFromAdd(id);
      });
        

      $(".includeContext2").append(cln);

      addedWord.push(wordIdx);
      numOfWord = $(".includeKana").length;
      if(numOfWord > 4)
        break;
    }
  }
}

function modifyWord()
{
	console.log("modifyThisWord!!");
	var currentStatus = wordDictionary[currentModifyingWordId];

	var kanji = $('input[id="KanjiInput"]').val();
	if(currentStatus[0] == kanji)
		kanji = "";
	else
		currentStatus[0] = kanji;
	var meaning = $('input[id="meaningInput"]').val(); 
	if(currentStatus[2] == meaning)
		meaning = "";
	else
		currentStatus[2] = meaning;
	var ex = $('input[id="exampleJapInput"]').val();
	if(currentStatus[4] == ex)
		ex = "";
	else
		currentStatus[4] = ex;
	var exMeaning = $('input[id="exampleKorInput"]').val()
	if(currentStatus[5] == exMeaning)
		exMeaning = "";
	else
		currentStatus[5] = exMeaning;
	var nkTest = false;
  	if($('input[id="checkiz"]').val() == "on")
  		nkTest = true;
  	
  	if(currentStatus[3] == nkTest)
		nkTest = "";
	else
		currentStatus[3] = nkTest;


	modifyWordCommand(currentModifyingWordId
					  ,$('input[id="KanaInput"]').val()
					  ,kanji
					  ,meaning
					  ,nkTest
					  ,ex
					  ,exMeaning);

	var modifiedWord = usrData.learningPool[currentModifyingWordId];
	setUserDataCookie(usrData);

	history.back();
}

function cancle()
{
	history.back();
}

function deleteWord()
{
	console.log("deleteThisWord!!");
	deleteWordCommand(currentModifyingWordId);

	if(currentModifyingWordId in usrData.learningPool)
		delete usrData.learningPool[currentModifyingWordId];
	if(currentModifyingWordId in usrData.learningRecord)
		delete usrData.learningRecord[currentModifyingWordId];

	setUserDataCookie(usrData);

	history.back();
}

function deleteWordCommand(id)
{
    var newCommand ={
                      cmd : "DELETE_WORD",
                      timestamp : "",
                      wordId : id
                    };
    newCommand.timestamp = getCurrentTime();
  	commandList.push(newCommand);
  	console.log("new Command [deleteWord] Added");
  	console.log(newCommand);
  	commandToServer();

    delete getUserDataCookie().learningPool[id];
    renewUserDataCookie();

    history.go(0);

}

function modifyWordCommand(id, kana, kanji, meaning, nkTest, ex, exMeaning)
{
    var newCommand ={
                      cmd : "EDIT_WORD",
                      timestamp : "",
                      kana : kana,
                      wordId : id
                    };

    newCommand.timestamp = getCurrentTime();

    if(kanji != "")
        newCommand.kanji = kanji;
    if(meaning != "")
        newCommand.meaning = meaning;
    if(nkTest != "")
        newCommand.noKanjiTest = nkTest;
    if(ex != "")
        newCommand.example = ex;
    if(exMeaning != "")
        newCommand.exampleMeaning = exMeaning;


  	commandList.push(newCommand);
  	console.log("new Command [editWord] Added");
  	console.log(newCommand);
  	commandToServer();
}