var isLoginUI = true;

$(function() 
{
  /*
  $('.loginButton').bind({
    click: processLogin,
    d: function(e)
    {
      console.log(e.which);
    }
  });
  */
  $('.loginButton').bind('click', processLogin);
  $('.registButton').bind('click', processRegist);
  $(document).bind('keyup',function(e)
    {
      if(e.which == 13)
      {
        if(isLoginUI)
          processLogin();
        else
          processRegist();

      }
      return false;
    });

/*
  $('.loginButton').bind('keyup', function(e)
  {
    console.log(e.which);
  });
*/

  $('.toRegistButton').bind('click', function() 
  {
    isLoginUI = false;
    $('#registerBox').show();
    $('#loginBox').hide();
    $('.serverMessage').css('visibility', 'hidden');
    return false;
  });

  $('.toLoginButton').bind('click', function() 
  {
    isLoginUI = true;
    $('#registerBox').hide();
    $('#loginBox').show();
    $('.serverMessage').css('visibility', 'hidden');
    return false;
  });



  if(checkLoginSessionCookie() == true)
  {
    var sessionId = getSessionCookie();
    console.log(sessionId);

    var logindata = { 
                      cmd:"AUTO_LOGIN",
                      session: sessionId
                    };

    console.log(JSON.stringify(logindata));
    $.ajax
    ({
      type : "POST",
      url : $SCRIPT_ROOT + '/ajax/autoLogin',
      data : JSON.stringify(logindata),
      contentType: 'application/json;charset=UTF-8',
      success : function(data)
      { 
        console.log(data);
        load(History.getState().url+"body/main", ".main" );
        load(History.getState().url+"body/sidebar", ".sidebar");
      }

    });

  }

  $('.serverMessage').css('visibility', 'hidden');
  $('#registerBox').hide();
  
});

function processLogin()
{
    var logindata = {
                      cmd:"LOGIN",
                      id:"",
                      pw:""
                    };

    logindata['id'] = $('input[name="id"]').val();
    logindata['pw'] = $('input[name="pwd"]').val();

    if(logindata['id'] == "")
    {
      $('.serverMessage').css('visibility', 'visible');
      $(".serverMessage b").text("아이디를 입력하세요.");
      return;
    }

    if(logindata['pw'] == "")
    {
      $('.serverMessage').css('visibility', 'visible');
      $(".serverMessage b").text("비밀번호를 입력하세요.");
      return;
    }

    console.log(JSON.stringify(logindata));
    $.ajax
    ({
      type : "POST",
      url : $SCRIPT_ROOT + '/ajax/login',
      data : JSON.stringify(logindata),
      contentType: 'application/json;charset=UTF-8',
      success : function(data)
      { 
        console.log(data);
        if(data.result == true)
        {
          setSessionCookie(data.session);
          setUserNameCookie(logindata['id']);
          load(History.getState().url+"body/main", ".main" );
          load(History.getState().url+"body/sidebar", ".sidebar");
        }
        else
        {
          $('.serverMessage').css('visibility', 'visible');
          //$(".serverMessage b").text(data.reason);

          if(data.reason == "USER_NOT_FOUND")
          {
            $(".serverMessage b").text("등록되지 않은 아이디입니다.");
          }
          else if(data.reason == "WRONG_PASSWORD")
          {
            $(".serverMessage b").text("비밀번호가 틀립니다.");
          }
          else
            $(".serverMessage b").text(data.reason);
        }
        //$("#result").text(data.result);
      }

    });
    return false;
  }

function processRegist()
{
  var logindata = { 
                      cmd:"REGISTER",
                      id:"",
                      pw:""
                    };

    logindata['id'] = $('input[name="newId"]').val();
    logindata['pw'] = $('input[name="newPwd"]').val();

    if(logindata['id'] == "")
    {
      $('.serverMessage').css('visibility', 'visible');
      $(".serverMessage b").text("아이디를 입력하세요.");
      return;
    }

    if(logindata['pw'] == "")
    {
      $('.serverMessage').css('visibility', 'visible');
      $(".serverMessage b").text("비밀번호를 입력하세요.");
      return;
    }

    if(logindata['pw'] != $('input[name="NewPwdRe"]').val())
    {
      $('.serverMessage').css('visibility', 'visible');
      $(".serverMessage b").text("다시 입력하신 비밀번호가 다릅니다.");
      return;
    }

    console.log(JSON.stringify(logindata));
    $.ajax
    ({
      type : "POST",
      url : $SCRIPT_ROOT + '/ajax/register',
      data : JSON.stringify(logindata),
      contentType: 'application/json;charset=UTF-8',
      success : function(data)
      { 
        console.log(data);
        if(data.result == true)
        {
          setSessionCookie(data.session);
          setUserNameCookie(logindata['id']);
          load(History.getState().url+"body/main", ".main" );
          load(History.getState().url+"body/sidebar", ".sidebar");
        }
        else
        {
          $('.serverMessage').css('visibility', 'visible');
          if(data.reason == "ID_ALREADY_EXISTS")
          {
            $(".serverMessage b").text("이미 등록된 아이디입니다.");
          }
          else if(data.reason == "UNKNOWN_ERROR")
          {
            $(".serverMessage b").text("알 수 없는 이유로 실패했습니다. 나중에 다시 시도해주세요.");
          }
          else
            $(".serverMessage b").text(data.reason);
          
          //console.log(data.reason);

        }
        //$
        //$("#result").text(data.result);
      }

    });
    return false;
}