var wordPoolList;
var wordRecordList;

var sortedWordList = new Array();
var originalList = new Array();
var probList = {};
var currentList = new Array();
var currentPage = 0;
var maxWordPerPage = 25;
var maxPageNumberPerPage = 5;

var currentFilter = '1';
var currentSearchWord = "";
var isOriginal = false;

var currentSortType = 7;
var isDescending = true;

$(function() 
{
	//this is deep copy..!
	var usrData = getUserDataCookie();
	wordPoolList = usrData.learningPool;
	wordRecordList = usrData.learningRecord;
	
	initializeArray();

	currentpage = lastVewingWordPage;
	if(currentPage < 0)
		currentPage = 1;
	var maxPage = Math.ceil( sortedWordList.length / maxWordPerPage );
	if(currentPage >= maxPage)
		currentPage = maxPage-1;

	initializeList(currentpage);
	$(':selected').attr("disabled", true);

}); 

function initializeArray()
{
	sortedWordList.length = 0;
	probList = {};
	var today = new Date();

	for(var wordIdx in wordPoolList)
  	{
  		originalList.push(wordIdx);
  		sortedWordList.push(wordIdx);
  		if(wordIdx in wordRecordList)
  		{
  			var record = wordRecordList[wordIdx];
  			var wrongCount = (record[9].match(/X/g) || []).length;
  			var startDate = getDateFromISO(record[0]);
  			var recentDate = getDateFromISO(record[1]);
  			var prob = memorizedProbability(record[2], wrongCount , recentDate, 0);
  			var reviewtiming = getForgetCurve(record[2], wrongCount).ends;

  			var diff = (today - recentDate)/(24*3600*1000);
  			var reviewEmergency = false;
  			if(diff > reviewtiming)
  				reviewEmergency = true;

  			if(getDateFromISO(record[1]))
  			var correctCount = record[3]+record[4]+record[5];
  			var wrongCount = record[6]+record[7]+record[8];
  			var prob2 = (correctCount) / (correctCount + wrongCount);
  			probList[wordIdx] = {mprob : prob, cprob : prob2, review : reviewEmergency, start: startDate, recent: recentDate};
  		}
  		else
  		{
  			probList[wordIdx] = {mprob : 0, cprob : 0, review : false, start:-1, recent:-1};
  		}
  	}
  	sortedWordList.sort(kanaSort);
  	isOriginal = true;
  	/*
  	console.log(wordPoolList);
  	console.log(wordList);
  	*/
}

function returnToOriginalArray()
{
	sortedWordList.length = 0;
	for(var i=0; i<originalList.length;i++)
	{
		sortedWordList.push(originalList[i]);
	}
	sortedWordList.sort(kanaSort);
	isOriginal = true;
	initializeList(currentPage);
}

function searchWord()
{
	currentSearchWord = $('input[id="searchBox"]').val();
	if(currentSearchWord == "")
	{
		if(isOriginal == false)
			returnToOriginalArray();
		return;
	}

	currentPage = 0;
	isOriginal = false;

	sortedWordList.length = 0;
	for(var wordIdx in wordPoolList)
  	{
  		var word = wordPoolList[wordIdx];
  		console.log(word);
    	if(((word[0] != null && word[0] != "")
    		&& word[0].indexOf(currentSearchWord) < 0)
    		&& word[1].indexOf(currentSearchWord) < 0
    		&& word[2].indexOf(currentSearchWord) < 0)
    		continue;
  		sortedWordList.push(wordIdx);
  	}

	initializeList(currentPage);
	//if(wordList)
}


function initializeList(page)
{
	//console.log("fucked up");
	$("#wordList").empty();
	var addedWordCount = 0;
	var startWordIdx = page * maxWordPerPage;
	currentList.length = 0;
	
	for(var i = startWordIdx ; i < startWordIdx + maxWordPerPage ; i++)
  	{
  		if(i >= sortedWordList.length)
  			break;
    	var wordIdx = sortedWordList[i];
    	var word = wordPoolList[wordIdx];


    	var cln = $(".wordRow").first().clone();
    	if (i % 2 == 0)
    		cln.addClass("even");
    	else
    		cln.addClass("odd");
      	cln.removeAttr( "style" );
      	cln.attr( "value", wordIdx );
      	if(word[0] == null || word[0] == "")
      		cln.find("div.leftAttach").text(word[1]+" : "+word[2]);
      	else
			cln.find("div.leftAttach").text(word[0]+"["+word[1]+"]: "+word[2]);
      	if(wordIdx in wordRecordList)
      	{
      		var startDate = getDateFromISO(wordRecordList[wordIdx][0]);
      		var sdStr = (startDate.getMonth() + 1) + "-" + startDate.getDate();
      		var recentDate = getDateFromISO(wordRecordList[wordIdx][1]);
      		var rdStr = (recentDate.getMonth() + 1) + "-" + recentDate.getDate();
      		var level = wordRecordList[wordIdx][2];
      		var mprob = Math.floor(probList[wordIdx]['mprob'] * 100);
      		var cprob = Math.floor(probList[wordIdx]['cprob'] * 100);

      		var levelTxt;
      		if(level < 4)
    		{
     			levelTxt = "단기";
    		}
    		else if(level >= 6)
    		{
      			levelTxt = "장기";
    		}
    		else
    		{
      			levelTxt = "중기";
    		}
      		cln.find("div.rightAttach").text(sdStr+"/"+rdStr+"/ Lv."+level+" ("+levelTxt+")/"+mprob+"%/"+cprob+"%");

      	}
      	else
      	{
      		cln.find("div.rightAttach").text("학습하지 않은 단어");
      	}
      	
      	cln.bind("click", function() 
      	{
      		var id = $(this).attr("value");
      		moveToWordModify(id);
      	});
      	
      	$("#wordList").append(cln);

    	addedWordCount++;
  	}

  	$("#wordBookCounterContext").empty();

  	var clnBox = $(".pagination").first().clone();
    clnBox.removeAttr( "style" );
    clnBox.attr( "value", "<" );
    clnBox.text( "<" );
    clnBox.bind("click", function() 
    {
      	var id = $(this).attr("value");
      	movePage(id);
    });
    $("#wordBookCounterContext").append(clnBox);

    var maxPage = Math.ceil( sortedWordList.length / maxWordPerPage );
    var pagelen = Math.min(maxPage, maxPageNumberPerPage);

    var midPageCount = Math.floor(maxPageNumberPerPage / 2);
    
    var startPage = currentPage - midPageCount;

    if(currentPage + midPageCount >= maxPage)
    	startPage -= 1 + (currentPage + midPageCount) - maxPage;

    if(startPage < 0)
    	startPage = 0;

    for(var i=startPage;i<startPage+maxPageNumberPerPage;i++)
    {
    	if(i >= maxPage)
    	{
    		console.log(maxPage);
    		break;
    	}

		clnBox = $(".pagination").first().clone();
		clnBox.removeAttr( "style" );
		clnBox.attr( "value", i );
		
		if(i == currentPage)
		{
			clnBox.text( i+1 );
			clnBox.css("background-color","white");
		}
		else
		{
			clnBox.text( i+1 );
	    	clnBox.bind("click", function() 
	    	{
	      		var id = $(this).attr("value");
	      		id *=1;
	      		movePage(id);
	    	});
    	}
    	$("#wordBookCounterContext").append(clnBox);
    }
 

    clnBox = $(".pagination").first().clone();
    clnBox.removeAttr( "style" );
    clnBox.attr( "value", ">" );
    clnBox.text( ">" );
    clnBox.bind("click", function() 
    {
      	var id = $(this).attr("value");
      	movePage(id);
    });
    $("#wordBookCounterContext").append(clnBox);
}


var currentSortDiv = null;

function sortList(sortType)
{
	console.log(sortType);
	if(currentSortType == sortType)
	{
		isDescending = !isDescending;

		if(isDescending)
		{
			sortedWordList.reverse();
			currentSortDiv.text(sortDivInfo[currentSortType].text+"▼");
			sortedWordList.sort(sortDivInfo[currentSortType].descend);
		}
		else
		{
			sortedWordList.reverse();
			currentSortDiv.text(sortDivInfo[currentSortType].text+"▲");
			sortedWordList.sort(sortDivInfo[currentSortType].ascend);
		}
		initializeList(currentPage);
		return;
	}
	else
	{
		isDescending = true;
		if(currentSortDiv != null)
			currentSortDiv.text(sortDivInfo[currentSortType].text);
		currentSortType = sortType;
		currentSortDiv = $(sortDivInfo[currentSortType].id);

		currentSortDiv.text(sortDivInfo[currentSortType].text+"▼");
		sortedWordList.sort(sortDivInfo[currentSortType].descend);
		initializeList(currentPage);
	}



/*
	switch(currentSortType)
	{
		case 1: //학습 시작일
			sortedWordList.sort(startDateSort);
			break;
		case 2: //최근 학습일
			sortedWordList.sort(recentDateSort); 
			break;
		case 3: //암기 레벨
			sortedWordList.sort(levelSort); 
			break;
		case 4: //암기율 
			sortedWordList.sort(memorizeProbSort); 
			break;
		case 5: //정답률 
			sortedWordList.sort(correctProbSort); 
			break;
		case 6: //한자. 
			sortedWordList.sort(kanjiSort); 
			break;
		case 7: // 가나 
			sortedWordList.sort(kanaSort); 
			break;
		case 8: // 의미
			sortedWordList.sort(meaningSort); 
			break;
		default:
			console.log("BUG");
			return;
	}
	*/
}


function filterList()
{

	//console.log();
	$('option').attr("disabled", false);
	currentFilter = $('select[name="two"]').val();
	$(':selected').attr("disabled", true)
	
	console.log(currentFilter);
	switch(currentFilter)
	{
		case '1': //전체 단어
			returnToOriginalArray();
			break;
		case '2': //학습 중 단어
			filterLearning(true);
			break;
		case '3': //학습 중이 아닌 단어
			filterLearning(false);
			break;
		case '4': //복습이 필요한 단어
			filterNeedReview();
			break;
		case '5': //복습이 시급한 단어
			filterEmergency(); 
			break;
		default:
			console.log("BUG");
			return;
	}


  	if(isDescending)
		sortedWordList.sort(sortDivInfo[currentSortType].descend);
	else
		sortedWordList.sort(sortDivInfo[currentSortType].ascend);

	initializeList(currentPage);
}

function filterLearning(isLearning)
{
	currentPage = 0;
	isOriginal = false;
	sortedWordList.length = 0;
	for(var wordIdx in wordPoolList)
  	{
    	if(wordIdx in wordRecordList != isLearning)
    		continue;
  		sortedWordList.push(wordIdx);
  	}

}

function filterNeedReview()
{
	currentPage = 0;
	isOriginal = false;
	sortedWordList.length = 0;
	for(var wordIdx in wordPoolList)
  	{
    	if((probList[wordIdx]['mprob'] == 0
    		|| probList[wordIdx]['mprob'] > 0.8)
    		&& probList[wordIdx]['review'] == false)
    		continue;
  		sortedWordList.push(wordIdx);
  	}

}

function filterEmergency()
{
	currentPage = 0;
	isOriginal = false;
	sortedWordList.length = 0;
	for(var wordIdx in wordPoolList)
  	{
    	if(probList[wordIdx]['review'] == false)
    		continue;
  		sortedWordList.push(wordIdx);
  	}

}

function movePage(newPage)
{
	//console.log("new page is " + newPage);
	if(newPage == "<")
		newPage = currentPage-1;
	if(newPage == ">")
		newPage = currentPage+1;

	var maxPage = Math.ceil( sortedWordList.length / maxWordPerPage );
	if(newPage < 0 || newPage >= maxPage)
		return;

	currentPage = newPage;
	initializeList(currentPage);
}

function moveToWordModify(id)
{
	console.log("it\'s " + id);
	currentModifyingWordId = id;
	event.preventDefault();
    load(History.getState().url+"body/wordedit", ".main" );
    history.pushState("body/wordedit","wordedit", "");

    lastVewingWordPage = currentPage;
}



//SORT FUNCTIONS

function kanjiSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		if(a in wordRecordList)
			return -1;
		else
			return 1;
	}
	else
	{
		if(wordPoolList[a][0] == null) return 1;
		if(wordPoolList[b][0] == null) return -1;
			
		if ( wordPoolList[a][0] < wordPoolList[b][0]) return -1;
		else if( wordPoolList[a][0] > wordPoolList[b][0] ) return 1;
		else return 0;
	}

}

function kanjiSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		if(a in wordRecordList)
			return -1;
		else
			return 1;
	}
	else
	{
		if(wordPoolList[a][0] == null) return -1;
		if(wordPoolList[b][0] == null) return 1;

		if ( wordPoolList[a][0] < wordPoolList[b][0]) return 1;
		else if( wordPoolList[a][0] > wordPoolList[b][0] ) return -1;
		else return 0;
	}

}

function kanaSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		if(a in wordRecordList)
			return -1;
		else
			return 1;
	}
	else
	{
		if ( wordPoolList[a][1] < wordPoolList[b][1]) return -1;
		else if( wordPoolList[a][1] > wordPoolList[b][1] ) return 1;
		else return 0;
	}
}


function kanaSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		if(a in wordRecordList)
			return -1;
		else
			return 1;
	}
	else
	{
		if ( wordPoolList[a][1] < wordPoolList[b][1]) return 1;
		else if( wordPoolList[a][1] > wordPoolList[b][1] ) return -1;
		else return 0;
	}
}

function meaningSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		if(a in wordRecordList)
			return -1;
		else
			return 1;
	}
	else
	{
		if ( wordPoolList[a][2] < wordPoolList[b][2]) return -1;
		else if( wordPoolList[a][2] > wordPoolList[b][2] ) return 1;
		else return 0;
	}
}

function meaningSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		if(a in wordRecordList)
			return -1;
		else
			return 1;
	}
	else
	{
		if ( wordPoolList[a][2] < wordPoolList[b][2]) return 1;
		else if( wordPoolList[a][2] > wordPoolList[b][2] ) return -1;
		else return 0;
	}
}

function startDateSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return _startDateSort(a,b);
	}
}

function startDateSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return _startDateSort(a,b)* -1;
	}
}

function _startDateSort(a,b)
{
	return probList[b]['start'] - probList[a]['start'];
}

function recentDateSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return _recentDataSort(a,b);
	}

}

function recentDateSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return _recentDataSort(a,b)* -1;
	}

}

function _recentDataSort(a, b)
{
	return probList[b]['recent'] - probList[a]['recent'];
}


function levelSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return _levelSort(a,b);
	}
}

function levelSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return _levelSort(a,b) * -1;
	}
}

function _levelSort(a,b)
{
	var aa, bb;
	if(a in wordRecordList)
		aa = wordRecordList[a][2];
	else
		aa = -1;

	if(b in wordRecordList)
		bb = wordRecordList[b][2];
	else
		bb = -1;

	return bb - aa;
}

function memorizeProbSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return probList[b]['mprob'] - probList[a]['mprob'];
	}
}
function memorizeProbSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return probList[a]['mprob'] - probList[b]['mprob'];
	}
}
function correctProbSort(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return probList[b]['cprob'] - probList[a]['cprob'];
	}
}
function correctProbSortReverse(a, b)
{
	if((a in wordRecordList) ^ (b in wordRecordList))
	{
		return sortHelper_learningStartCheck(a);
	}
	else
	{
		return probList[a]['cprob'] - probList[b]['cprob'];
	}
}

function sortHelper_learningStartCheck(a)
{
	if(a in wordRecordList)
		return -1;
	else
		return 1;
}

var sortDivInfo = 
{
	1: { text:"학습 시작일", id:"#learningStartDateSelect", ascend: recentDateSortReverse, descend: startDateSort },
	2: { text:"최근 학습일", id:"#recentLearningDateSelect", ascend: recentDateSortReverse, descend: recentDateSort  },
	3: { text:"암기 레벨", id:"#memorizeLevelSelect", ascend: levelSortReverse, descend: levelSort  },
	4: { text:"암기율", id:"#memorizeRateSelect", ascend: memorizeProbSortReverse, descend: memorizeProbSort  },
	5: { text:"정답률", id:"#answerRateSelect", ascend: correctProbSortReverse , descend: correctProbSort},
	6: { text:"한자", id:"#kanjiSelect", ascend: kanjiSortReverse, descend: kanjiSort  },
	7: { text:"[가나]", id:"#kanaSelect", ascend: kanaSortReverse, descend: kanaSort  },
	8: { text:" :뜻", id:"#meaningSelect", ascend: meaningSortReverse, descend: meaningSort  }
};