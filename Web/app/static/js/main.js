
var drawnUsrData;

$(function() 
{
    var synctime = getUserDataSyncTimeStampCookie();
    var kanjitime = getKanjiDataSyncTimeStampCookie();
    //console.log(synctime);
    //console.log(kanjitime);

    var sessionId = getCookie("kasikoi_session");
    var kanjidata = {
                      cmd:"KANJI",
                      timestamp: kanjitime ,
                    };

    var syncdata = {
                      cmd:"SYNC",
                      session : sessionId,
                      timestamp: synctime,
                    };


    console.log(JSON.stringify(kanjidata));
    console.log(JSON.stringify(syncdata));
    
    $.ajax
    ({
      type : "POST",
      url : $SCRIPT_ROOT + '/ajax/kanji',
      data : JSON.stringify(kanjidata),
      contentType: 'application/json;charset=UTF-8',
      success : function(data)
      { 
        console.log(data);

        if('data' in data)
        {
            setKanjiDataSyncTimeStampCookie(data.timestamp);
            console.log(data.data);
            setKanjiDataCookie(data.data);
            //var obj = eval("("+data.data+")");
            console.log(data);
        }
        else
        {
            console.log("Not need to sync");
        }

      }

    });

    $.ajax
    ({
      type : "POST",
      url : $SCRIPT_ROOT + '/ajax/sync',
      data : JSON.stringify(syncdata),
      contentType: 'application/json;charset=UTF-8',
      success : function(data)
      { 
        console.log(data);
        if('data' in data)
        {
            setUserDataSyncTimeStampCookie(data.timestamp);
            console.log(data.data);
            setUserDataCookie(data.data);
            //var obj = eval("("+data.data+")");
            console.log(data);
        }
        else
        {
            console.log("Not need to sync");
        }

        drawnUsrData = getUserDataCookie();
        $("#userName").text(drawnUsrData['userName']);
        calculateLearningCountStatus();
        calculateLearningInfoStatus();
        
        drawGraph();
        fillLearningStatusParagraph();
        fillSidebarInfo();
      }

    });

    $('.statisticButton#LearningWord').bind('click', function() 
    {
    });

    //window.onresize = drawGraph;
});

var maxGraphDataCnt = 8;
var memorizeData1 = new Array();
var memorizeData2 = new Array();
var data2 = new Array(); 
var data3 = new Array();


var main_todayLearningCnt = 0;
var main_reviewCnt = 0;

var main_passedDay = 0;
var main_skippedDay = 0;
var main_continuousLearningDate = 0;

var main_newLearningCnt = 0;
var main_emergencyCnt = 0;
var main_forgetWordCnt = 0;

var main_levelLow = 0;
var main_levelMiddle = 0;
var main_levelHigh = 0;

function fillLearningStatusParagraph()
{
  var box = $('#statusParagraphContext');
  box.empty();
  if(main_passedDay == 0)
  {
    box.append("<p>아직 학습을 시작하지 않으셨습니다.</p>");
  }
  else
  {
    box.append("<p>학습을 시작한 지 "+main_passedDay+"일 지났습니다.</p>");
    if(main_skippedDay > 0)
        box.append("<p>그 중 "+main_skippedDay+"일 학습을 빼먹었습니다.</p>");
  }
  if(main_continuousLearningDate > 0)
    box.append("<p>"+main_continuousLearningDate+"일 연속 학습 중입니다.</p>");
  else
  {
    var day = Math.floor(main_continuousLearningDate*-1);
    if(day != 0)
      box.append("<p>"+day+"일 만에 왔습니다.</p>");
  }

  if(main_newLearningCnt > 0)
    box.append("<p>최근 1주일간 "+main_newLearningCnt+"개 단어의 학습을 시작했습니다.</p>");
  box.append("<br/>");
  box.append("<p>단어 수</p>");

  box.append("<p>-암기:"+Math.floor(sidebarInfo_MemorizedWordCnt)+"/학습 중:"+sidebarInfo_LearningWordCnt+"/전체:"+sidebarInfo_WordCnt+"</p>");
  box.append("<p>-단기 기억:"+main_levelLow+"/중기 기억:"+main_levelMiddle+"/장기 기억:"+main_levelHigh+"</p>");
  box.append("<p>-복습이 필요한 단어 수 : "+main_forgetWordCnt+"</p>");
  
  if(main_emergencyCnt > 0)
    box.append("<p>복습이 시급한 단어가 "+main_emergencyCnt+"개 있습니다.</p>");
  

  //box.append("<p>최근 1주일간 "+main_reviewCnt+"개의 단어를 학습했습니다.</p>");
  //box.append("<p>오늘 "+main_todayLearningCnt+"개 단어를 학습했습니다.</p>");
}

function drawGraph()
{
  drawMemorizeStatusGraph();
  drawLearningStatusGraph();
  drawWordStatusGraph();
}

function calculateLearningCountStatus()
{
  data2.length = 0;
  data3.length = 0;
  var learningDateData = drawnUsrData['log'];
  console.log(learningDateData);

  var all = 0;
  var thisMonth = 0;
  var lastWeek = 0;
  var thisWeek = 0;
  var beforeYesterday = 0;
  var yesterday = 0;
  var todaycnt = 0;

  var today = new Date();
  var todayDay = today.getDay();
  var maxdiff = 0; 
  main_passedDay = 0;
  var learnedDay = 0;
  //날짜, 총단어수 , 학습중 단어수 , 암기한 단어수, 학습횟수 
  var previousDiff = -1;
  main_continuousLearningDate = 0;
  for(var i=0;i<learningDateData.length;i++)
  {
    
    var learningData = learningDateData[i];
    var date = getDateFromISO(learningData[0]);
    var diff = (today - date)/(24*3600*1000);

    if((previousDiff < 0 || Math.floor(previousDiff - diff) <= 1)
      && learningDateData[4] > 0)
    {
      previousDiff = diff;
      main_continuousLearningDate++;
    }
    else
    {
      main_continuousLearningDate=0;
      previousDiff = -1
    }
    if(maxdiff < diff)
      maxdiff = diff;

    if(learningData[4]>0)
      learnedDay++;


    //console.log(learnedDay);

    all += learningData[4];
    if( diff < todayDay+1)
      thisWeek+= learningData[4];

    if(diff < 1)
    {
      todaycnt += learningData[4];  
      continue;
    }
    
    if(date.getMonth() == today.getMonth())
      thisMonth+= learningData[4];
    if(diff >= 7+todayDay && diff < 13+todayDay)
      lastWeek+= learningData[4];

    if(diff > 2 && diff < 3)
      beforeYesterday+= learningData[4];
    if(diff > 1 && diff < 2)
      yesterday+= learningData[4];
    //if(date.get)
  }

  if(previousDiff > 1)
    main_continuousLearningDate = -main_continuousLearningDate;

  console.log(maxdiff);
  main_passedDay = Math.floor(maxdiff) + 1;
  main_skippedDay = main_passedDay - learnedDay;

  if(main_passedDay == main_skippedDay)
  {
    main_passedDay = 0;
    main_skippedDay = 0;
  }
  
  all /= maxdiff;
  thisMonth /= today.getDate();
  lastWeek /= 7;
  thisWeek /= todayDay+1;


  data2 = [ ["전체", all], 
            ["이번달", thisMonth], 
            ["지난주", lastWeek], 
            ["이번주", thisWeek], 
            ["그저께", beforeYesterday], 
            ["어제", yesterday],
            ["오늘", todaycnt]];
}

function calculateLearningInfoStatus()
{
  var wordData = drawnUsrData['learningRecord'];
  var logData = drawnUsrData['log'];
  var wordDataArray = new Array();
  var probData = {};
  var cntData = {};

  var currentLearningWordCount = 0;
  var todayStudied = false;

  var today = new Date();
  var todayDay = today.getDay(); 

  for(var logIdx in logData)
  {
    var past = getDateFromISO(logData[logIdx][0]);
    var pastStr = (past.getMonth()+1)+"/"+(past.getDate());
    currentLearningWordCount = logData[logIdx][2];

    if(!(pastStr in cntData))
      cntData[pastStr] = currentLearningWordCount;

    if((today - past)/(24*3600*1000) <= 1)
    {
      todayStudied = true;
      probData[pastStr] = 0;
    }
    else
    {
        probData[pastStr] = logData[logIdx][3];
    }


  }

  sidebarInfo_WordCnt = 0;

  for(var i in drawnUsrData['learningPool'])
    sidebarInfo_WordCnt++;

  sidebarInfo_LearningWordCnt = 0;
  sidebarInfo_MemorizedWordCnt = 0;
  sidebarInfo_RecentDate = "";

  var minDiff = Number.MAX_SAFE_INTEGER;
  main_todayLearningCnt = 0;
  main_emergencyCnt = 0;
  main_forgetWordCnt = 0;
  main_newLearningCnt = 0;
  main_reviewCnt = 0;


  main_levelLow = 0;
  main_levelMiddle = 0;
  main_levelHigh = 0;


  for(var idx in wordData)
  {
    sidebarInfo_LearningWordCnt++;
    var learningData = wordData[idx];
    var date = getDateFromISO(learningData[1]);
    var diff = (today - date)/(24*3600*1000);

    var learningStatDate = (today - getDateFromISO(learningData[0]))/(24*3600*1000);

    if(learningStatDate < 7)
      main_newLearningCnt++;

    if(diff < 7)
      main_reviewCnt++;

    if(diff <= 1)
      main_todayLearningCnt++;
    if(minDiff > diff)
    {
      minDiff = diff;
      sidebarInfo_RecentDate = (date.getMonth()+1)+"/"+date.getDate();
    }

    var dateString = (today.getMonth()+1)+"/"+today.getDate();

    var recentResultString = learningData[9];
    var recentWrongCount = 0;     
    for(var i=0;i<recentResultString.length;i++)
      if(recentResultString.charAt(i) == 'X')
        recentWrongCount++;

    if(learningData[2] < 4)
    {
      main_levelLow++;
    }
    else if(learningData[2] >= 6)
    {
      main_levelHigh++;
    }
    else
    {
      main_levelMiddle++;
    }

    var prob = memorizedProbability(learningData[2],recentWrongCount, date, 0);
    if(prob == 0)
    {
      console.log("-------------------------------");
      console.log(idx);
      console.log(learningData);
      console.log(recentWrongCount);
      console.log(date);
      console.log(diff);
    }
    sidebarInfo_MemorizedWordCnt += prob;
    wordDataArray.push([diff, prob]);
    
    if(prob < 0.2)
    {
      main_emergencyCnt++;
    }
    
    if(dateString in probData)
    {
        probData[dateString] += prob; 
    }
    else
    {
      probData[dateString] = prob;
      if(!(dateString in cntData))
          cntData[dateString] = currentLearningWordCount;
    }

    var forgot = getForgetCurve(learningData[2], recentWrongCount);
    if(forgot.starts <= 1)
    {
      main_forgetWordCnt++;
    }
    
    /*
    console.log("---------------------------------");
    //console.log("prob        : " +prob);
    
    console.log("level : " + learningData[2]);
    console.log("wrongCount : " + recentWrongCount);
    console.log("date : " + date);
    console.log("result : " + prob);
    */

    for(var overDate=0;overDate<3;overDate++)
    {
      var newDate = new Date(new Date().getTime() + ((overDate+1) * 24 * 60 * 60 * 1000));
      dateString = (newDate.getMonth()+1)+"/"+(newDate.getDate());
      //console.log(dateString);
      prob = memorizedProbability(learningData[2] ,recentWrongCount, date, diff + overDate+1);
      //console.log("Date over " + (overDate+1) + " : " + prob);
      if(dateString in probData)
      {
        probData[dateString] += prob; 
      }
      else
      {
        probData[dateString] = prob;
        if(!(dateString in cntData))
          cntData[dateString] = currentLearningWordCount;
      }

    }
    

  }

  for(var dataDate in probData)
  {
    if(maxGraphDataCnt > memorizeData1.length)
    {
      memorizeData1.push([dataDate, probData[dataDate]]);
      memorizeData2.push([dataDate, cntData[dataDate]]);
    }
    else
    {
      memorizeData1.shift();
      memorizeData2.shift();
      memorizeData1.push([dataDate, probData[dataDate]]);
      memorizeData2.push([dataDate, cntData[dataDate]]);
    }
  }
  //memorizeData2.push([dateString, currentLearningWordCount]);

  data3.push( {data: wordDataArray, points:{symbol: "circle"}} );
  console.log(data3);
}




function drawMemorizeStatusGraph()
{
    $.plot("#placeholder", [{ data: memorizeData1,
                              lines: {show: true},
                              points: {show: true}}, 
                            { data: memorizeData2,
                              lines: {show: true},
                              points: {show: true}} ], 
      {
      xaxis: {
        mode: "categories"
      },
      yaxis:
      {
        min:0
      }
    });
}

function drawLearningStatusGraph()
{
    $.plot("#placeholder2", [ data2 ], 
    {
      series: {
        bars: {
          show: true,
          barWidth: 0.4,
          align: "center"
        }
      },
      xaxis: {
        mode: "categories",
        tickLength: 0
       
      },
      yaxis:
      {
        min:0
      }
    });
}

function drawWordStatusGraph()
{
    $.plot("#placeholder3",  data3 , 
    {
      series: {
        points: {
          show: true,
          radius: 3
        }
      },
      grid: {
        hoverable: true
      },
      xaxis: 
      { 
        min:0, max: 14,  tickSize: 1
      },

      yaxis:
      {
        min:0, max: 1,  tickSize: 0.1
      }
    });
}
/*
xaxis: 
      { 
        mode: "time", 
        min: (new Date("2010/11/01")).getTime(),
        max: (new Date("2011/02/01")).getTime()
      },
*/