
$(function() 
{
  $('.studyButton#memorize').bind('click', function() 
  {
     changeButtonStatus($('#memorize'), $('#noMemorize'));
  }); 

  $('.studyButton#noMemorize').bind('click', function() 
  {
    changeButtonStatus($('#noMemorize'), $('#memorize'));
  });

  $('.nextButton').bind('click', function() 
  {
     nextProblem();
  });


  $(document).bind('keyup',  function(e) 
  {
    switch(e.which)
    {
      case 67: // c
        changeButtonStatus($('#memorize'), $('#noMemorize'));
        break;
      case 83: // s
        changeButtonStatus($('#noMemorize'), $('#memorize'));
        break;
      case 65: // a
        nextProblem();
        break;
      default:
        break;
    }
  });

  initializeButtonState();
  initializeStudy();
}); 

function changeButtonStatus(selectedButton, unselectedButton)
{ 
  if(selectedButton.hasClass("selectedButton"))
  {
    return;
  }

  if($('#nextWord').hasClass("avoid-clicks"))
  {
    $('#nextWord').removeClass("avoid-clicks");
  }

  if(!selectedButton.hasClass("unselectedButton"))
  {

    var word = getCurrentWord();
    switch(currentProblemType)
    {
      case "kanji":
        $('#kanjiClock').attr('class', 'studyNoClockContext');
        break;
      case "kana":
        $('[id=kanaClock]').attr('class', 'studyNoClockContext');
        break;
      case "meaning":
        $('#meaningClock').attr('class', 'studyNoClockContext');
        break;
    }
  }

  selectedButton.removeClass("unselectedButton");
  unselectedButton.removeClass("selectedButton");

  selectedButton.addClass("selectedButton");
  unselectedButton.addClass("unselectedButton");

  if(selectedButton.attr("id") == "memorize")
  {
    selectedButton.addClass("memorizeButtonColor");
    unselectedButton.removeClass("noMemorizeButtonColor");
  }
  else
  {
    unselectedButton.removeClass("memorizeButtonColor");
    selectedButton.addClass("noMemorizeButtonColor");
  }

  $('.studyExampleContext').css("visibility", "visible");
  $('.studyStatisticContext').css("visibility", "visible");
}

function initializeStudy()
{
  var word = getNextWord();
  setProblem(word);
}

function initializeButtonState()
{
  $('#memorize').removeClass("selectedButton");
  $('#memorize').removeClass("unselectedButton");
  $('#memorize').removeClass("memorizeButtonColor");
  $('#noMemorize').removeClass("selectedButton");
  $('#noMemorize').removeClass("unselectedButton");
  $('#noMemorize').removeClass("noMemorizeButtonColor");
  $('#nextWord').addClass("avoid-clicks");
}


function setProblem(word)
{
  currentProblemType = getProblemType(word);

  if(word.kanji == "")
  {
    $('.studyOnlyKanaContext').show();
    $('.studyHiraKanjiContext').hide();
    $('p.kana').text(word.kana);
  }
  else
  {
    $('.studyOnlyKanaContext').hide();
    $('.studyHiraKanjiContext').show();
    $('p.kanji').text(word.kanji);
    $('p.kana').text(word.kana);
  }
  $('p.meaning').text(word.meaning);

  if(word.exampleStatement != "" && word.exampleStatement != null)
  {
    $('.studyExampleParagraphContext').show();
    $('p.paragraph#exampleParagraph').text(word.exampleStatement);
    $('p.paragraph#exampleMeaning').text(word.exampleStatementMeaning);
  }
  else
  {
    $('.studyExampleParagraphContext').hide();
  }

  $('.studyExampleContext').css("visibility", "hidden");
  $('.studyStatisticContext').css("visibility", "hidden");

  console.log(currentProblemType);
  switch(currentProblemType)
  {
    case "kanji":
      $('#kanjiClock').attr('class', 'studyClockContext');
      break;
    case "kana":
      $('[id=kanaClock]').attr('class', 'studyClockContext');
      break;
    case "meaning":
      $('#meaningClock').attr('class', 'studyClockContext');
      break;
  }

  var levelTxt;
  if(word.level < 4)
  {
    levelTxt = "단기 기억 레벨";
  }
  else if(word.level >= 6)
  {  
    levelTxt = "장기 기억 레벨";
  }
  else
  {
    levelTxt = "중기 기억 레벨";
  }

  $('.level').text("암기레벨 : " + word.level+"("+levelTxt+")");
  $('.probability').text("암기확률 : " + word.level);
  $('.heuristic').text("복습 가중치 : " + word.level);
  var overallCount =  word.overallMatchCount.correct 
                      + word.overallMatchCount.wrong ;
  $('.overallRatio').text("정답/전체 : " + word.overallMatchCount.correct + "/" + overallCount);
  $('.overallPercentage').text("정답률 : " + getProb(word.overallMatchCount) + "%");
  $('.kanjiPercentage').text("한자 정답률 : " + getProb(word.kanjiMatchCount) + "%");
  $('.kanaPercentage').text("가나 정답률 : " + getProb(word.kanaMatchCount) + "%");
  $('.meaningPercentage').text("뜻 정답률 : " + getProb(word.meaningMatchCount) + "%");
  var recentresultstring = "";
  var recentwrongCount = 0;
  for(var i=0;i<word.recentResult.length;i++)
  {  
    if(word.recentResult[i] == true)
      recentresultstring += "O";
    else
    {
      recentresultstring += "X";
      recentwrongCount++;
    }
  }
  $('.recentResult').text("최근 : " + recentresultstring);
  $('.startDate').text("암기 시작일 : " + word.startDate.replace("T"," ").replace("Z","").substring(0,16));
  $('.recentDate').text("최근 학습일 : " + word.recentDate.replace("T"," ").replace("Z","").substring(0,16));

  var forgetCurve = getForgetCurve(word.level, recentwrongCount);
  console.log(getDateFromISO(word.recentDate));
  console.log(forgetCurve['starts']);
  var datedata = getDateFromISO(word.recentDate).getTime();
  datedata += forgetCurve['starts'] * 24 * 60 * 60 * 1000;
  datedata += 9*3600*1000;
  var forgetStarts = new Date(datedata);
  console.log(forgetStarts);
  $('.forgetDate').text("망각 시작일 : " + forgetStarts.toISOString().replace("T"," ").replace("Z","").substring(0,16));
  
  if(checkLevelChangeCondition(word, true) && word.level != maxLevel)
    $('.LevelUp').text("이번에 암기로 체크되면 레벨업");
  else
    $('.LevelUp').text("");

  setRelatedData(word);

}

function setRelatedData(word)
{
  $(".studyExampleRelativeContext").empty();
  if(word.kanji.length == 0)
    return;
  var wordlist = getUserDataCookie()['learningPool'];
  var kanjilist = getKanjiDataCookie();

  var addedKanji = new Array();

  for(var i=0;i<word.kanji.length;i++)
  {   
    var singleword = word.kanji[i];
    for(var j=0;j<singleword.length;j++)
    {
      var singleKanji = singleword.charAt(j);
      if((singleKanji in kanjilist)
        && 0 > $.inArray(singleKanji, addedKanji))
      {
        addedKanji.push(singleKanji);
      }
    }
  }

  console.log(addedKanji);
  var kanjiCnt = Math.min(addedKanji.length, 3);

  console.log(kanjiCnt);
  var addedword = new Array();
  for(var i=0;i<kanjiCnt;i++)
  {
    var cln = $(".studyExampleRelativeWordContext").first().clone();
    cln.removeAttr( "style" );
    cln.append("<p class = \"associatedKanji\">"+addedKanji[i]+" : "+kanjilist[addedKanji[i]]+"</p>");

    var relatedWordCnt = 0;
    for(var wordIdx in wordlist)
    {
      if(word.idx == wordIdx)
        continue;
      var relatedWord = wordlist[wordIdx];
      if(relatedWord[0] == null)
        continue;
      if(relatedWord[0].indexOf(addedKanji[i]) >= 0)
      {
        cln.append("<p class = \"associatedWord\">"+
          relatedWord[0]+"["+relatedWord[1]+"]:"+relatedWord[2]+"</p>");
        relatedWordCnt++;
        if(relatedWordCnt > 2)
          break;
      }
    }

    if(relatedWordCnt >= 1)
      $(".studyExampleRelativeContext").append(cln);
  }
  
  /*       
  cln.bind("click", function() 
  {
    var id = $(this).attr("value");
    moveToWordModify(id);
  });
  */
  
}

function getProb(record)
{
  var overall = record.correct + record.wrong;
  if(overall == 0)
    return 0;
  return (record.correct / (record.correct + record.wrong) * 100).toFixed(2);
}

function nextProblem()
{
  var word = getCurrentWord();
  var result;
  if($('#memorize').hasClass("selectedButton"))
  { 
    result = true;
  }
  else if($('#noMemorize').hasClass("selectedButton"))
  {
    result = false;
  }
  else  
  {
    return;
  }

  console.log(word);
  record(word, result, currentProblemType);
  addLearnSyncCommand(word, currentProblemType, result);
  commandToServer();
  word = getNextWord();
  setProblem(word);
  
  initializeButtonState();
}


