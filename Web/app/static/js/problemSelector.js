
var records = {};

var workingList = new Array();
var candidateList = {};
var candidateListLength = 0;
var maxWeights = new Array();

var waitingSet = new Array();
var newWordSet = new Array();

var wordBook;
var recordData;
var logData;

var currentWordIdx;
var currentProblemType;

var workingListTargetSize = 25;
var levelChangeThreshold = 3;
var maxRecentResult = 5;
var maxLevel = 10;
var minLevel = 1;

var initialRecordData = 
[
	getCurrentTime(),
	getCurrentTime(),
	minLevel,
	0,0,0,
	0,0,0,
	""
];


$(function() 
{
	//get allWords;
	setInitialData();

	var allWords = wordBook;

	for(var wordId in allWords)
	{
		newWordSet.push(wordId);
	}

	for(var recordedId in records)
	{
		var newWordIdx = newWordSet.indexOf(recordedId);
		newWordSet.splice(newWordIdx, 1);
		waitingSet.push(recordedId);
	}
	
	currentWordIdx = 0;
	currentProblemType = "kanji";

/*
  	console.log("시작이다요~!");
  	console.log(newWordSet);
  	console.log(waitingSet);
  	console.log(workingList);
  	*/
});


function setInitialData()
{
	
	var userData = getUserDataCookie();
	wordBook = userData.learningPool;
	recordData = userData.learningRecord;
	logData = userData.log;

	for(var wordId in recordData)
	{
		if(!(wordId in records) && (wordId in wordBook))
		{
			var record = recordData[wordId];
			var word = wordBook[wordId];
   			makeRecord(wordId, word, record);
		}
	}
	
	//console.log(records);
}

function getWordRecord(wordIdx)
{
	var id = wordIdx *1;

	if(!(id in records))
	{
		addNewRecord(wordIdx, wordBook[wordIdx]);
	}
	
	return records[wordIdx];
}

//미리 해놓는다.
function addNewRecord(wordIdx, wordInfo)
{
	/*
	console.log("ADD NEW RECORD");
	console.log(wordIdx);
	console.log(wordInfo);*/
   	var word = wordBook[wordIdx];
   	makeRecord(wordIdx, word, initialRecordData);
}

function makeRecord(wordId, word, wordInfo)
{
	var record = new Object();

	var intId = wordId*1;
    record.idx = intId;
    if(word[0] == null)
    	record.kanji = "";
    else
    	record.kanji = word[0].split(";");
    record.kana = word[1];
    record.meaning = word[2];

    record.hardKanji = word[3]; 

    record.exampleStatement = word[4];
    record.exampleStatementMeaning = word[5];
    //read from server
    record.level = wordInfo[2];
    record.kanjiMatchCount = {'correct' : wordInfo[3], 'wrong' : wordInfo[6]};
    record.kanaMatchCount = {'correct' : wordInfo[4], 'wrong' : wordInfo[7]};
   	record.meaningMatchCount = {'correct' : wordInfo[5], 'wrong' : wordInfo[8]};

    record.overallMatchCount = {'correct' : record.kanjiMatchCount.correct 
                                      		+ record.kanaMatchCount.correct
                                            + record.meaningMatchCount.correct, 
                                'wrong' : record.kanjiMatchCount.wrong 
                                            + record.kanaMatchCount.wrong
                                            + record.meaningMatchCount.wrong};  
    record.startDate = wordInfo[0];
    record.recentDate = wordInfo[1];  
    record.recentResult = new Array();
    
    var recentResultString = wordInfo[9]; 
    for(var i=0;i<recentResultString.length;i++)
    {
    	if(recentResultString.charAt(i) == 'X')
    	{
    		record.recentResult.push(false);
    	}
    	else
    	{
    		record.recentResult.push(true);
    	}
    }
	
    record.syncNeeded = false;  

    records[intId] = record; 
}


function getCurrentWord()
{
	var wordId = workingList[currentWordIdx];
	return getWordRecord(wordId);
}

function getNextWord()
{
	currentWordIdx++;
	if(currentWordIdx >= workingList.length)
	{
		endLearning();
		fillWorkingList();
		shuffleWorkingList();
  		/*
  		console.log(workingList);
  		for(var i=0;i<workingList.length;i++)
  		{
  			var sample = workingList[i];
  			if(sample in newWordSet)
  			{
  				console.log("new word set");
  			}
  			else if(sample in waitingSet)
  			{
  				console.log("waitingSet");
  			}
  			else
  			{
  				console.log(sample);
  			}
  		}
  		*/
		currentWordIdx = 0;
	}

	if(workingList.length == 0)
	{
		console.log("error!!!");
		return null;

	}
	
	var wordId = workingList[currentWordIdx];

	//console.log(workingList);
	//console.log(wordId);

	return getWordRecord(wordId);
}

function endLearning()
{
	for(var i=0;i<workingList.length;i++)
	{
		var wordId = workingList[i];
		var record = records[wordId];

		if(record.length != 0 &&
			record.recentResult[record.recentResult.length - 1] == true)
		{
			workingList.splice(i,1);
			waitingSet.push(wordId);
			i--;
		}
	}
}

function fillWorkingList()
{
	var from = workingList.length;
	var to = Math.min(newWordSet.length + waitingSet.length, workingListTargetSize); 

	fillWeightList();
	fillMaxList();

	var maxWeightIndex = maxWeights.length - 1;

	/*
	console.log(from);
	console.log(to);
	console.log(waitingSet);
	*/
	for(var i=from;i<to;i++)
	{
		if(newWordSet.length > 0)
		{
			var newWordProb = 1.0;
			if(maxWeightIndex >= 0)
			{
				newWordProb = newWordProbability(maxWeights[maxWeightIndex--]);
				console
			}

			if(getEventWithProbability(newWordProb))
			{
				var newWordIdx = getRandomInt(newWordSet.length);
				pickNewWord(newWordIdx);
				continue;
			}

		}

		if(candidateListLength > 0)
		{
			var waitingIdx = getValueUsingWeight(candidateList);
			pickWaiting(waitingIdx);

			//console.log(waitingSet);
		}
	}

	/*
	console.log(waitingSet);
	console.log(workingList);
	*/
}

function shuffleWorkingList()
{
	shuffle(workingList);
}

function pickNewWord(idx)
{
	var newWordIdx = newWordSet[idx];
	newWordSet.splice(idx,1);
	workingList.push(newWordIdx);
}

function pickWaiting(idx)
{

	delete candidateList[idx];
	candidateListLength--;

	var valueIdx = waitingSet.indexOf(idx);
	waitingSet.splice(valueIdx,1);
	workingList.push(idx);
}

function fillWeightList()
{
	candidateList = {};
	candidateListLength = 0;

	var minWeight = Number.MAX_VALUE;
	var minWeightIndex = 0;

	for(var idx=0;idx<waitingSet.length;idx++)
	{
		var id = waitingSet[idx];

		var record = getWordRecord(id);
		var weight = wordWeight(record);
		if(candidateListLength >= MaxWeightPool
			&& weight > minWeight)
		{
			delete candidateList[minWeightIndex];
			candidateListLength--;
			minWeight = Number.MAX_VALUE;
			for(candidteIdx in candidateList)
			{
				if(candidateList[candidteIdx] < minWeight)
				{
					minWeight = candidateList[candidteIdx];
					minWeightIndex = candidteIdx;
				}
			}
		}

		if(candidateListLength < MaxWeightPool)
		{
			if(weight < minWeight)
			{
				minWeight = weight;
				minWeightIndex = candidateListLength;
			}

			candidateList[id] = weight;
			candidateListLength++;
		}
	}
}

function fillMaxList()
{
	maxWeights.length = 0; //initialization clear

	//console.log("FILL MAX LIST !!!!!");
	//console.log(candidateList);
	for(var candidateIdx in candidateList)
	{
		var w = candidateList[candidateIdx];
		if(maxWeights.length < workingListTargetSize)
		{
			maxWeights.push(w);
			if(maxWeights.length == workingListTargetSize)
			{
				maxWeights.sort(function(a, b){return a-b});
			}
		}
		else
		{
			if(maxWeights[workingListTargetSize-1] < w)
			{
				if(w > maxWeights[0])
				{
					maxWeights.splice(0,0,w);
				}
				else
				{
					for(var i=0;i<maxWeights.length;i++)
					{
						if(maxWeights[i] < w)
						{
							maxWeights.splice(i,0,w);
							break;
						}
					}
				}

				while(maxWeights.length > workingListTargetSize)
				{
					maxWeights.pop();
				}
			}
		}

	}

}




function record(word, result, problemType)
{
  //record currnet date
  var wrongCount = 0;
  var date = getDateFromISO(word.recentDate);
	for(var i=0;i<word.recentResult.length;i++)
		if(word.recentResult[i] == false)
			wrongCount++;
  var prob = memorizedProbability(word.level, wrongCount, date, 0);
  sidebarInfo_MemorizedWordCnt -= prob;
  word.recentDate = getCurrentTime();
  var recordWord;
  if(word.idx in recordData)
  {
  	recordWord = recordData[word.idx];
  	recordWord[1] = word.recentDate;
  }
  else
  {
  	recordData[word.idx] =
  	[
  		word.recentDate,
  		word.recentDate,
  		minLevel,
  		0,0,0,
  		0,0,0,
  		""
  	];
  	recordWord = recordData[word.idx];
  	sidebarInfo_LearningWordCnt++;
  }

  var storage;
  var correctRecordIdx = 0; //345
  var wrongRecordIdx = 0; //678
  switch(problemType)
  {
    case "kanji":
    storage = word.kanjiMatchCount;
    correctRecordIdx = 3;
    wrongRecordIdx = 6;
      break;
    case "kana":
    storage = word.kanaMatchCount;
    correctRecordIdx = 4;
    wrongRecordIdx = 7;
      break;
    case "meaning":
    storage = word.meaningMatchCount;
    correctRecordIdx = 5;
    wrongRecordIdx = 8;
      break;
  }
   
  //record result count
  if(result == true)
  {
    word.overallMatchCount.correct++;
    storage.correct++;
    recordWord[correctRecordIdx]++;

    recordWord[9] += 'O';
  }
  else
  {
    word.overallMatchCount.wrong++;
    storage.wrong++;
    recordWord[wrongRecordIdx]++;

    recordWord[9] += 'X';
  }


  if(recordWord[9].length > 5)
  {
    recordWord[9] = recordWord[9].slice(1, recordWord[9].length);
  }
  //record recent result data
  if(word.recentResult.length >= maxRecentResult)
  {
    word.recentResult.shift();
  }
  word.recentResult.push(result);

  //change Level
  changeLevel(word, result);

  recordWord[2] = word.level;
  word.syncNeeded = true;

/*
  console.log(word);
  console.log(recordWord);
*/
  wrongCount = 0;
  date = getDateFromISO(word.recentDate);
  for(var i=0;i<word.recentResult.length;i++)
	if(word.recentResult[i] == false)
		wrongCount++;
  prob = memorizedProbability(word.level, wrongCount, date, 0);
  sidebarInfo_MemorizedWordCnt += prob;


  logUpdate();
  fillSidebarInfo();
}


function changeLevel(word, result)
{
  var levelChanged = checkLevelChangeCondition(word, result);
  if(levelChanged == true)
  {
    if(result == true)
    {
      word.level = Math.min(word.level+1, maxLevel);
    }
    else
    {
      word.level = Math.max(word.level-1, minLevel);
    }
  }
}

function checkLevelChangeCondition(word, result)
{
  var resultlength = word.recentResult.length;
  var len = Math.min(resultlength, levelChangeThreshold);
  var levelChanged = true;
  
  for(var i=0;i<len;i++)
  {
    if(word.recentResult[resultlength - i - 1] != result)
    {
      levelChanged = false;
    }
  }

  return levelChanged;
}

function getMatchCount(matchType)
{
  return matchType.correct + matchType.wrong;
}

function logUpdate()
{
  var today = new Date();
  var todaylog = logData[logData.length-1];
  var date = getDateFromISO(todaylog[0]);
  var diff = (today - date)/(24*3600*1000);

  console.log(getCurrentTimeBase());
  if(diff>1)
  {
  	getCurrentTimeBase();
  	var newlog =
  	[
  		getCurrentTimeBase(),
  		todaylog[1],
  		todaylog[1],
  		todaylog[1],
  		0
  	];
  	logData.push(newlog);
  	todaylog = newlog;
  }
  todaylog[4]++;

  //update log.
}

