
$(function() 
{
    $('div#study.sidebar_MainButton').bind('click', function(event) 
  {
    event.preventDefault();

    if(getUserDataCookie().learningPool == undefined ||
      jQuery.isEmptyObject(getUserDataCookie().learningPool))
    {
      load(History.getState().url+"body/wordadd", ".main" );
      history.pushState("body/wordadd","wordadd", "");
      return;
    }

    load(History.getState().url+"body/study", ".main" );
    history.pushState("body/study","study",$SCRIPT_ROOT);
        //$('.main').load($SCRIPT_ROOT + '');
    }); 

    $('div#wordadd.sidebar_MainButton').bind('click', function(event) 
	{
		event.preventDefault();
		load(History.getState().url+"body/wordadd", ".main" );
		history.pushState("body/wordadd","wordadd", "");
    }); 

    $('div#curLearningState.sidebar_MenuBox').bind('click', function(event) 
  {
    event.preventDefault();
    load(History.getState().url+"body/main", ".main" );
    history.pushState("body/main","main", ""); 
    }); 

    $('div#myWordBook.sidebar_MenuBox').bind('click', function(event) 
  {
    event.preventDefault();
    load(History.getState().url+"body/myWordBook", ".main" );
    history.pushState("body/myWordBook","myWordBook", "");
    });

    $('div#wordBookList.sidebar_MenuBox').bind('click', function(event) 
  {
    event.preventDefault();
    load(History.getState().url+"body/wordBookList", ".main" );
    history.pushState("body/wordBookList","wordBookList", "");
  });


var $idown;
  $('#appDownButton').bind('click', function(event) 
  {
        //$.get("../static/com.cs408.kasikoi.apk");
        var url = "../static/com.cs408.kasikoi.apk";
      if ($idown) {
    $idown.attr('src',url);
  } else {
    $idown = $('<iframe>', { id:'idown', src:url }).hide().appendTo('body');
  }

        /*
        var exportData = {
                            cmd : "APPDOWNLOAD",
                         };
        $.ajax(
        {
            url: $SCRIPT_ROOT + '/ajax/appdownload',
            type: "post",
            data : JSON.stringify(exportData),
            contentType: 'application/json;charset=UTF-8',
            
            success: function(data) 
            {   
               
            }, 
            error: function() 
            {

            }
            
        });
*/

  });


    $('div#logout.sidebar_BottomMenuBox').bind('click', function(event) 
	{
		event.preventDefault();

		var sessionId = getCookie("kasikoi_session");
    	console.log(sessionId);

    	var logindata = { 
                	      cmd:"LOGOUT",
            	          session: sessionId
        	            };

    	console.log(JSON.stringify(logindata));
    	$.ajax
    	({
      	type : "POST",
      	url : $SCRIPT_ROOT + '/ajax/logout',
      	data : JSON.stringify(logindata),
      	contentType: 'application/json;charset=UTF-8',
      	success : function(data)
      	{ 
       	 	console.log(data);
       	 	deleteAllCookie();
          $(".sidebar").html("");
			    load(History.getState().url+"body/login", ".main" );
			
			//history.pushState("body/login","login", "");
    	 }

    	});
		//load(History.getState().url+"body/wordadd",".main");
        //$('.main').load($SCRIPT_ROOT + '/body/wordadd');
    });


}); 