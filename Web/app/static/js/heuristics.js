var fastReview = 0.25;
var reviewVsNewWord = 0.05;
var MaxWeightPool = 50;
var memoryProbabilityAtForgetStarts = 0.8;

var days = 1000 * 60 * 60 * 24;

var forgetStartsTable = 
{
	1 	: ( 1.0 / (24.0 * 60.0) ) * 1 ,
	2 	: ( 1.0 / (24.0 * 60.0) ) * 3 ,
	3 	: ( 1.0 / (24.0 * 60.0) ) * 10 ,
	4 	: 1 ,
	5 	: 3 ,
	6 	: 7 ,
	7 	: 15 ,
	8 	: 30 ,
	9 	: 90 ,
	10 	: 180 ,
}

function shuffle(o)
{
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function getRandomInt(maxValue)
{
	var r = Math.floor(Math.random()*maxValue);
	return r;
}

function getEventWithProbability(probability)
{
	var r = Math.random();

	return ((1.0-probability) < r);
}

function getValueUsingWeight(valueWithWeight)
{
    var totalSum = 0;
    for(var value in valueWithWeight)
    {
        totalSum += valueWithWeight[value];
    }

    var r = Math.random() * totalSum;
    var partialSum = 0;

    for(var value in valueWithWeight)
    {
        partialSum += valueWithWeight[value];
        if(r <= partialSum)
            return value;
    }

    return Object.keys(valueWithWeight)[0];
 }


function getProblemType(record)
{
	var kanjicnt = record.kanjiMatchCount.correct;
	var kanacnt = record.kanaMatchCount.correct;
	var meaningcnt = record.meaningMatchCount.correct;

	var isZeroExists = false;
	if((record.kanji != "" && record.hardKanji != true && kanjicnt == 0) 
		|| kanacnt == 0
		|| meaningcnt ==0)
		isZeroExists = true;

	var sum = record.overallMatchCount.correct;
	var max = Math.max(	kanjicnt,
						kanacnt,
						meaningcnt,
						1);

	var weights = 
	{
		"kanji" 	: max + Math.pow(max - kanjicnt, 3) * 20,
		"kana"		: max + Math.pow(max - kanacnt, 3) * 20,
		"meaning"	: max + Math.pow(max - meaningcnt, 3) * 20
	}

	if(isZeroExists == true)
	{
		if(kanjicnt > 0)
			weights.kanji = 0;
		if(kanacnt > 0)
			weights.kana = 0;
		if(meaningcnt > 0)
			weights.meaning = 0;
	}

	if(record.kanji == "" || record.hardKanji == true)
	{
		weights.kanji = 0;
	}

	return getValueUsingWeight(weights);
}

function wordWeight(record)
{
	var wrongCount = 0;
	for(var i=0;i<record.recentResult.length;i++)
		if(record.recentResult[i] == false)
			wrongCount++;
	var prob = memorizedProbability(record.level, wrongCount, getDateFromISO(record.recentDate), 0);

	if(prob > memoryProbabilityAtForgetStarts)
	{
		var value = mapLinear(prob,1,0,memoryProbabilityAtForgetStarts,1);
		return value * value;

	}
	else
	{
		var weightByProb = mapLinear(prob, memoryProbabilityAtForgetStarts, 0, 0, 10);
        weightByProb = weightByProb * weightByProb + 1;

        var weight = weightByProb;

		var forgetStarts;
		var forgetEnds;
        var forgetInfo = getForgetCurve(record.level, wrongCount);

		forgetStarts = forgetInfo.starts;
		forgetEnds = forgetInfo.ends;

		var d = new Date();
		var recentDate = getDateFromISO(record.recentDate);
		var daysAfterRecent = (d.getTime() - recentDate) / days;

		if (daysAfterRecent > forgetStarts)
        {
            var weightByDayAfter = daysAfterRecent - forgetStarts;
            weight += weightByDayAfter;
        }

        if (daysAfterRecent > forgetEnds)
        {
            var dayAfterForgetEnd = daysAfterRecent - forgetEnds;
            var clamped = Math.min(dayAfterForgetEnd, 10);
            var weightByForgetAfter = clamped * clamped;
            weight += weightByForgetAfter;
        }

        return weight;
	}
}


function getForgetCurve(level, wrongCount)
{
	var forgetStarts = forgetStartsTable[level] / (wrongCount + 1);
	var forgetEnds = forgetStarts * 3; 
	return { 'starts' : forgetStarts, 'ends' : forgetEnds};
}

function memorizedProbability(level, wrongCount, recent, dayOfs)
{
	if(recent <= 0)
		return 0;
	var d = new Date();
	var daysAfterRecent =  (d.getTime() - recent.getTime()) / days + dayOfs;
	var forgetStarts;
	var forgetEnds;

	var forgetInfo = getForgetCurve(level, wrongCount);

	forgetStarts = forgetInfo.starts;
	forgetEnds = forgetInfo.ends;


	/*
	console.log("------------------------------------------");
	console.log("recentDate		: " +recent);
	console.log("today			 : " + d);	
	console.log("wrongCount		: " +wrongCount);
	console.log("daysAfterRecent : " +daysAfterRecent);
	console.log("forgetStarts    : " +forgetStarts);
	console.log("forgetEnds      : " +forgetEnds);
	*/
	if(daysAfterRecent < forgetStarts)
	{
		return mapCosine(daysAfterRecent, 0, forgetStarts, memoryProbabilityAtForgetStarts, 1);
	}
	else if( daysAfterRecent < forgetEnds )
	{
		return mapCosine(daysAfterRecent, forgetStarts, forgetEnds, 0, memoryProbabilityAtForgetStarts);
	}
	else
	{
		return 0;
	}
}


function mapCosine(domain, minDomain, maxDomain, minValue, maxValue)
{
	var ratio = (domain - minDomain) / (maxDomain - minDomain);
    var theta = ratio * Math.PI / 2.0;
    var cosine = Math.cos(theta);
    var value = (maxValue - minValue) * cosine + minValue;
	return value;
}

function mapLinear(domain, minDomain, valueAtMin, maxDomain, valueAtMax)
{
	var ratio = (domain - minDomain) / (maxDomain - minDomain);
	var value = (valueAtMax - valueAtMin) * ratio + valueAtMin;
	return value;
}

function newWordProbability(highHeuristic)
{
	if(highHeuristic < 0)
	{
		return Math.pow( 1 - highHeuristic, fastReview) * (1 - reviewVsNewWord) + reviewVsNewWord;
	}
	else
	{
		return 1.0 / highHeuristic * reviewVsNewWord;
	}
}