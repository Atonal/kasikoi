
$(function() 
{
  var nua = navigator.userAgent;
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
  {
    var is_android = true;
  }
  
  if(is_android) 
  {
    $('body').empty();

    $.ajax(
    {
        url: History.getState().url+"body/mobile",
        success: function(data) 
        {
            $('body').html(data);
        }
    });


    return;
  }

    var updateContent = function(stateObj) 
    {
    // Check to make sure that this state object is not null.
    	console.log(stateObj);
    	if (stateObj) 
    	{
    		load(History.getState().url+stateObj, ".main" );
    	}
    	else
    	{
    		load(History.getState().url+"body/login", ".main" );
    	}	
  	};

	window.addEventListener('popstate', function(event) {

		updateContent(event.state);
  	});


    $('#credits').bind('click', function(event) 
  {
    event.preventDefault();
    load(History.getState().url+"body/members", ".main" );
    history.pushState("body/members","members", "");
  });
    $('#detailedInfo').bind('click', function(event) 
  {
    event.preventDefault();
    load(History.getState().url+"body/information", ".main" );
    history.pushState("body/information","information", "");
  });
    $('#notice').bind('click', function(event) 
  {
    event.preventDefault();
    load(History.getState().url+"body/notice", ".main" );
    history.pushState("body/notice","notice", "");
  });

	load(History.getState().url+"body/login", ".main" );
  	history.pushState("","main",$SCRIPT_ROOT);

}); 

function load(url, target) 
{
    $.ajax(
    {
        url: url,
        success: function(data) 
        {
            $(document).unbind('keyup');
            $(document).unbind('keydown');
            $(target).html(data);
        }
    });
}

function changeURL(data)
{
	var url = window.location;
	window.location = data;
}


function checkLoginSessionCookie() {
    var user = getCookie("kasikoi_session");
    if (user != "") 
    {
    	return true;
    } 
    else 
    {
        return false;
    }
}

function setCookie(cname, cvalue, exdays) 
{
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function deleteCookie(cname)
{	
	var d = new Date();
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=;" + expires;
}

function getCookie(cname) 
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

//must modify this
//read key from Object ?
function deleteAllCookie()
{
    deleteCookie("kasikoi_session");
    deleteCookie("kasikoi_username");
    deleteCookie("kasikoi_userdataTS");
    deleteCookie("kasikoi_kanjiTS");

    localStorage.removeItem("kasikoi_userData");
    localStorage.removeItem("kasikoi_kanjiData");
}


function setSessionCookie (cvalue) 
{
    setCookie("kasikoi_session", cvalue, 365);
}
function setUserNameCookie (cvalue) 
{
    setCookie("kasikoi_username", cvalue, 365);
}
function setUserDataSyncTimeStampCookie (cvalue) 
{

    setCookie("kasikoi_userdataTS", cvalue, 365);
}
function setKanjiDataSyncTimeStampCookie (cvalue) 
{

    setCookie("kasikoi_kanjiTS", cvalue, 365);
}

function getSessionCookie ()
{
    return getCookie("kasikoi_session");
}

function getUserNameCookie ()
{
    return getCookie("kasikoi_username");
}

function getUserDataSyncTimeStampCookie ()
{

    var time = getCookie("kasikoi_userdataTS");
    if(time == "")
    {
        var td = new Date(1970,1,1,0,0,0,0);
        time = td.toISOString();
        setUserDataSyncTimeStampCookie(time);
    }
    return time;
}

function getKanjiDataSyncTimeStampCookie ()
{
    var time = getCookie("kasikoi_kanjiTS");
    if(time == "")
    {
        var td = new Date(1970,1,1,0,0,0,0);
        time = td.toISOString();
        setKanjiDataSyncTimeStampCookie(time);
    }
    return time;
}

var localUserData;
var IslocalUserDataFixed = false;
function getUserDataCookie ()
{
  if(!IslocalUserDataFixed)
  {
    IslocalUserDataFixed = true;
    localUserData = JSON.parse(localStorage.getItem('kasikoi_userData'));
  }
  return localUserData;
}

var localKanjiData;
var IslocalKanjiDataFixed = false;
function getKanjiDataCookie ()
{
  if(!IslocalKanjiDataFixed)
  {
    IslocalKanjiDataFixed = true;
    localKanjiData = JSON.parse(localStorage.getItem('kasikoi_kanjiData'));
  }
  return localKanjiData;
}

function setUserDataCookie (cvalue)
{
  IslocalUserDataFixed = false;
  localStorage.setItem('kasikoi_userData', JSON.stringify(cvalue));
}

function renewUserDataCookie()
{
  IslocalUserDataFixed = false;
  localStorage.setItem('kasikoi_userData', JSON.stringify(localUserData));
}

function setKanjiDataCookie (cvalue)
{
  IslocalKanjiDataFixed = false;
  localStorage.setItem('kasikoi_kanjiData', JSON.stringify(cvalue));
}

function getCurrentTime ()
{
  var date = new Date();
  var localOffset = date.getTimezoneOffset() * 60000;
  var localTime = date.getTime();
  date = localTime - localOffset;
  date = new Date(date);
  var time = date.toISOString();
  return time;
}

function getCurrentTimeBase()
{
  var date = new Date();
  var localOffset = date.getTimezoneOffset() * 60000;
  var localTime = date.getTime();
  date = localTime - localOffset;
  date = new Date(date);
  date = new Date(date.getFullYear(),date.getMonth(),date.getDate(),-15,0,0,0);
  var time = date.toISOString();
  return time;
}

function getDateFromISO(tdata)
{
    var date = new Date((tdata || "").replace(/-/g,"/").replace(/[TZ]/g," "));
    return date;
}


var commandList = new Array();
var commandIdx = 0;

function commandToServer()
{
  var sessionId = getCookie("kasikoi_session");
  var commandJson = {
                      "cmd" : "COMMAND",
                      "session" : sessionId,
                      "commands" : commandList
                    };
  console.log(commandList);
  $.ajax
  ({
    type : "POST",
    url : $SCRIPT_ROOT + '/ajax/command',
    data : JSON.stringify(commandJson),
    contentType: 'application/json;charset=UTF-8',
    success : function(data)
    { 
      console.log("command success");
      console.log(data);
      commandList.length = 0;
    }

  });
  
}


function addLearnSyncCommand(word, problemType, result)
{
  var newCommand =  {
                      cmd : "LEARN",
                      timestamp : "",
                      wordId : 0,
                      problemType : "KANJI",
                      memorized : false
                    };
  newCommand.wordId = word.idx;
  newCommand.timestamp = getCurrentTime();
  switch(problemType)
  {
    case "kanji":
    newCommand.problemType = "KANJI";
    break;
    case "kana":
    newCommand.problemType = "KANA";
    break;
    case "meaning":
    newCommand.problemType = "MEANING";
    break;
  }
  newCommand.memorized = result;

  commandList.push(newCommand);

  console.log("new Command [learnsync] Added");
  console.log(newCommand);
}

function addWordCommand(kanji, kana, meaning, noKanjiTest, example, exampleMeaning)
{
    var newCommand ={
                      cmd : "ADD_WORD",
                      timestamp : "",
                      kana : "",
                      wordId : 0,
                    };

    newCommand.timestamp = getCurrentTime();
    newCommand.wordId = getMaxWordId()*1+1;
    newCommand.kana = kana;

    console.log(meaning);
    if(kanji != "")
        newCommand.kanji = kanji;
    if(meaning != "")
        newCommand.meaning = meaning;
    if(noKanjiTest != "")
        newCommand.noKanjiTest = noKanjiTest;
    if(example != "")
        newCommand.example = example;
    if(exampleMeaning != "")
        newCommand.exampleMeaning = exampleMeaning;


  getUserDataCookie().learningPool[newCommand.wordId] = 
  [
    newCommand.kanji,
    newCommand.kana,
    newCommand.meaning,
    newCommand.noKanjiTest,
    newCommand.example,
    newCommand.exampleMeaning
  ]
  IslocalUserDataFixed = false;
  renewUserDataCookie();
  commandList.push(newCommand);
  console.log("new Command [addword] Added");
  console.log(newCommand);
}

function getMaxWordId()
{
  var pool = getUserDataCookie().learningPool;
  var Max = 0;
  for(var idx in pool)
  {
    if(idx > Max)
      Max = idx;
  }

  return Max;
}


var currentModifyingWordId = -1;

var lastVewingWordPage = 0;


var sidebarInfo_WordCnt = 0;
var sidebarInfo_LearningWordCnt = 0;
var sidebarInfo_MemorizedWordCnt = 0;
function fillSidebarInfo()
{
/*
  var sidebar = $('.sidebar_Statistics');
  sidebar.empty();
  sidebar.append("<b>학습 현황 요약</b>");
  sidebar.append("<b>단어장 총 단어 수 : "+sidebarInfo_WordCnt+"</b>");
  sidebar.append("<b>학습중인 단어 수 : "+sidebarInfo_LearningWordCnt+"</b>");
  sidebar.append("<b>암기한 단어 수 : "+sidebarInfo_MemorizedWordCnt.toFixed(2)+"</b>");
  sidebar.append("<b>최종 학습 날짜 : "+sidebarInfo_RecentDate+"</b>");
  */
}

