$(function() 
{
	$('#uploadButton').bind("click", function() {
			$('#fileUpload').trigger('click');
    });

    $('#fileUpload').change(function()
    {
    	var data = new FormData();

        $.each($('#fileUpload')[0].files, function(i, file) {          
            data.append('wordfile', file);
        });
             
        $.ajax(
        {
            url: $SCRIPT_ROOT + '/ajax/import/' + getSessionCookie(),
            type: "post",
            dataType: "text",
            data: data,
                // cache: false,
            processData: false,
            contentType: false,
            success: function(data) 
            {
                var result = JSON.parse(data);
                if(result["cmd"] == "ERROR")
                { 
                    alert("올바른 단어장 파일이 아닙니다.");
                }
                else
                    alert("단어장 파일을 서버에 업로드했습니다.");
           	}, 
            error: function(jqXHR, textStatus, errorThrown) 
            {
                alert("단어장 파일 업로드에 실패했습니다.");
            }
        });
    });

    $('#downloadButton').bind("click", function(e) 
    {
    	e.preventDefault();

    	var exportData = {
    						cmd : "EXPORT",
    						session : getSessionCookie()
    					 };
    	$.ajax(
        {
            url: $SCRIPT_ROOT + '/ajax/export',
            type: "post",
            data : JSON.stringify(exportData),
            contentType: 'application/json;charset=UTF-8',
            
            success: function(data) 
            {	
            	//var uriContent = "data:application/octet-stream;filename=filename.txt," + 
              	//				encodeURIComponent(data);
				//window.open(uriContent, 'filename.txt');
            	//var uri = 'data:application/csv;charset=UTF-8;content-disposition:attachment;filename=tiketi.csv,' + encodeURIComponent(data);
       			//window.open(uri, 'tiketi.csv');

       			var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
  				var csvDataIE = data;

				if (navigator.msSaveBlob) 
				{ // IE10
 					return navigator.msSaveBlob(new Blob([csvDataIE], { type: "text/csv;charset=utf-8;" }), "myWord.csv");
				} 
				else 
				{
 					var dLink = document.createElement("a");
 					dLink.download = "myWord.csv";
 					dLink.href = csvData;
 					dLink.click();
 				}
           	}, 
            error: function() 
            {

            }
            
        });
		
    });

}); 