var kanjiDictionary;
var wordDictionary;

var addwordshortcut_isCtrlPressed = false;
$(function() 
{
  $('#addButton').bind('click', function() 
  {
     addWord();
  }); 

  kanjiDictionary = getKanjiDataCookie();
  wordDictionary = getUserDataCookie().learningPool;
  console.log(wordDictionary);

  addwordshortcut_isCtrlPressed = false;
  $(document).bind('keyup',  function(e) 
  {
    console.log("key up : " + e.which);
    switch(e.which)
    {
      case 17: 
      case 25: // ctrl
        addwordshortcut_isCtrlPressed = false;
      break;
      default:
      break;
    }
  });

  $(document).bind('keydown',  function(e) 
  {
    switch(e.which)
    {
      case 17: // c
      case 25:
        addwordshortcut_isCtrlPressed = true;
        break;
      case 13: // enter
        if(addwordshortcut_isCtrlPressed)
          addWord();
        break;
      default:
        break;
    }
  });
  //wordDictionary = 

});

//prepend

function findKanji()
{
  
  var kanjiword = $('input[id="KanjiInput"]').val();
  $(".includeContext").empty();
  var kanjiarray = kanjiword.split(';');

  var numOfKanji = 0;
  var addedKanji = new Array();
  for(var i=0;i<kanjiarray.length;i++)
  {
    var word = kanjiarray[i];

    for(var j=0;j<word.length;j++)
    {
      var singleKanji = word.charAt(j);

      if((singleKanji in kanjiDictionary)
        && 0 > $.inArray(singleKanji, addedKanji))
      {
        numOfKanji = $(".includeKanji").length;
        if(numOfKanji > 3)
          break;

        $(".includeContext").prepend("<p class=\"includeKanji\">["
          +singleKanji+"]"+kanjiDictionary[singleKanji] +"</p>");
        addedKanji.push(singleKanji);
      }
      
    }
  }
  findSimillarWord();
}

/*
function findKana()
{
  
  var kanaword = $('input[id="KanaInput"]').val();
  $(".includeContext2").empty();
  if(kanaword == "")
    return;
  var numOfWord = 0;

  var addedWord = new Array();
  for(var wordIdx in wordDictionary)
  {
    var word = wordDictionary[wordIdx];
    if(word[1].indexOf(kanaword) > -1
      && 0 > $.inArray(wordIdx, addedWord))
    {  
      var cln = $(".similarWord").first().clone();
      cln.removeAttr( "style" );
      cln.find("a.includeKana").text(word[0]+"["+word[1]+"]:"+word[2]);
      $(".includeContext2").append(cln);

      addedWord.push(wordIdx);
      numOfWord = $(".includeKana").length;
      if(numOfWord > 4)
        break;
    }
  }

}
*/

function findSimillarWord()
{
  var kanaword = $('input[id="KanaInput"]').val();
  var kanjiword = $('input[id="KanjiInput"]').val();
  $(".includeContext2").empty();

  $('input[id="KanaInput"]').css("background-color","White");
  $('input[id="KanjiInput"]').css("background-color","White");
  if(kanaword == "" && kanjiword == "")
    return;

  var numOfWord = 0;

  var addedWord = new Array();
  for(var wordIdx in wordDictionary)
  {
    var word = wordDictionary[wordIdx];
    if(0 > $.inArray(wordIdx, addedWord))
    {
      var kanjiList;
      if(word[0] == null || word[0] == undefined)
        kanjiList = [];
      else
        kanjiList = word[0].split(';'); 
      var kanaList = word[1].split(';');

      var isSimilar = false;
      for(var i=0;i<kanjiList.length;i++)
      {
        if(kanjiList[0] == kanjiword)
        {
          $('input[id="KanjiInput"]').css("background-color","SkyBlue");
          isSimilar = true;
          break;
        }
      }

      for(var i=0;i<kanaList.length;i++)
      {
        if(kanaList[0] == kanaword)
        {
          $('input[id="KanaInput"]').css("background-color","SkyBlue");
          isSimilar = true;
          break;
        }
      }

      if(!isSimilar)
        continue;

      var cln = $(".similarWord").first().clone();
      cln.removeAttr( "style" );
      cln.find("a.includeKana").text(word[0]+"["+word[1]+"]:"+word[2]);
      cln.find(".editLinkButton").attr( "value", wordIdx );
      cln.find(".editLinkButton").bind("click", function() 
      {
        var id = $(this).attr("value");
        moveToModifyFromAdd(id);
      });
        

      $(".includeContext2").append(cln);

      addedWord.push(wordIdx);
      numOfWord = $(".includeKana").length;
      if(numOfWord > 4)
        break;
    }
  }
}

function moveToModifyFromAdd(id)
{
    console.log("it\'s " + id);
    currentModifyingWordId = id;
    event.preventDefault();
    load(History.getState().url+"body/wordedit", ".main" );
    history.pushState("body/wordedit","wordedit", "");
}

function addSimilarWordDiv(word)
{

}

function addWord()
{
  var kana = $('input[id="KanaInput"]').val();
  if(kana == "")
    return;

  var meaning = $('input[id="meaningInput"]').val();
  if(meaning == "")
    return;

  var kanji = $('input[id="KanjiInput"]').val();
  var example = $('input[id="exampleJapInput"]').val();
  var exampleMeaning = $('input[id="exampleKorInput"]').val();
  var noKanjiTest = $('input[id="checkiz"]').val();
  if(noKanjiTest == "on")
    noKanjiTest = true;
  else
    noKanjiTest = false;
  addWordCommand(kanji,kana,meaning,noKanjiTest,example,exampleMeaning);
  commandToServer();

  resetWordAddInputForm();
}

function resetWordAddInputForm() 
{
  $('input[id="KanjiInput"]').val('');
  $('input[id="KanaInput"]').val('');
  $('input[id="meaningInput"]').val('');
  $('input[id="exampleJapInput"]').val('');
  $('input[id="exampleKorInput"]').val('');
  $('input[id="checkiz"]').val('');

  $(".includeContext").empty();
  $(".includeContext2").empty();

  $('input[id="KanjiInput"]').focus();
  // body...
}


function blockBackspaceAndEnter(e) {
        if (!e) { // IE reports window.event instead of the argument
            e = window.event;
        }
        var keycode;
        if (document.all) {
            // IE
            keycode = e.keyCode;
        } else {
            // Not IE
            keycode = e.which;
        }
        // Enter is only allowed in textareas, and Backspace is only allowed in textarea and input text and password
        if ((keycode == 8
                && e.srcElement.type != "text"
                && e.srcElement.type != "textarea"
                && e.srcElement.type != "password")
                || (keycode == 13 && e.srcElement.type != "textarea")) {
            e.keyCode = 0;
            if (document.all) {
                // IE
                event.returnValue = false;
            } else {
                // Non IE
                Event.stop(e);
            }
        }
    }
    for (var i = 0; i < document.forms.length; i++) {
        document.forms[i].onkeydown = blockBackspaceAndEnter;
    }



//var relativeKanaWordFormat = 