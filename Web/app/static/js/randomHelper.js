function GetValueUsingWeight(valueWithWeight)
{
    var totalSum = 0;
    for(var value in valueWithWeight)
    {
        totalSum += valueWithWeight[value];
    }

    var r = Math.random() * totalSum;
    var partialSum = 0;

    for(var value in valueWithWeight)
    {
        partialSum += valueWithWeight[value];
        if(r <= partialSum)
            return value;
    }

    return Object.keys(valueWithWeight)[0];
 }