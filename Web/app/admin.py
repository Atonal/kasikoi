# -*- coding: utf-8 -*-
# Created by Jinsung Lee
from datetime import datetime, timedelta
from random import choice, randint, randrange
from flask import Blueprint, request, make_response

from database import db_session
from routes import json_response
from common import get_file_path
from models import User, LearningPool, LearningRecord, UserLog,\
    SHARD_MODEL_DICT, get_sharding_class, user_delete, db_user_clear
from service.session import _session_cache
from service.user import get_user
from service.learning import command_learn, wordbook_import, init_data, load_data
from service.heuristics import memory_expectation

admin = Blueprint('admin', __name__, url_prefix='/admin')

ADMIN_SUCCESS = {'cmd': "ADMIN", 'result': "SUCCESS"}

@admin.route('/db_user_clear', methods=['GET'])
def DBUserClear():
    except_str = request.args.get('except_ids', None, type=str)
    except_ids = []
    if except_str:
        except_ids = [int(i) for i in except_str.split(',')]
    db_user_clear(except_ids)
    _session_cache.clear()
    return json_response(ADMIN_SUCCESS)

@admin.route('/word_import', methods=['GET'])
def WordImport():
    wfile = open(get_file_path('data/'+request.args.get('file', type=str)), 'r')
    wordbook_import(request.args.get('uid', type=int), wfile)
    return json_response(ADMIN_SUCCESS)

@admin.route('/user_delete', methods=['GET'])
def UserDelete():
    user_delete(user_index=request.args.get('uid', type=int))
    _session_cache.clear()
    return json_response(ADMIN_SUCCESS)

@admin.route('/user_init', methods=['GET'])
def UserInit():
    init_data(request.args.get('uid', type=int))
    return json_response(ADMIN_SUCCESS)

@admin.route('/user_load', methods=['GET'])
def UserLoad():
    load_data(request.args.get('uid', type=int))
    return json_response(ADMIN_SUCCESS)

@admin.route('/user_copy', methods=['GET'])
def UserCopy():
    src_id = request.args.get('src', type=int)
    dst_id = request.args.get('dst', type=int)

    init_data(dst_id)
    src_user = get_user(src_id)
    dst_user = get_user(dst_id)
    dst_user.last_command = src_user.last_command
    db_session.add(dst_user)

    rdata = {}
    for ShardModel in SHARD_MODEL_DICT.keys():
        SrcShardModel = get_sharding_class(ShardModel, src_id)
        DstShardModel = get_sharding_class(ShardModel, dst_id)
        query = db_session.query(SrcShardModel)
        for row in query:
            db_session.add(row.clone(DstShardModel))
        rdata[DstShardModel.__tablename__] = query.count()
    db_session.commit()

    rdata.update(ADMIN_SUCCESS)
    return json_response(rdata)

@admin.route('/user_change_days')
def UserChangeDays():
    uid = request.args.get('uid', type=int)
    days = request.args.get('days', type=int)

    td_days = timedelta(days=days)
    user = get_user(uid)
    user.last_command += td_days
    db_session.add(user)

    ShardLearningRecord = get_sharding_class(LearningRecord, uid)
    for record in db_session.query(ShardLearningRecord):
        record.encounter_date += td_days
        record.recent_date += td_days
        db_session.add(record)

    ShardUserLog = get_sharding_class(UserLog, uid)
    if days < 0:
        query = db_session.query(ShardUserLog).order_by('date')
    else:
        query = db_session.query(ShardUserLog).order_by('-date')
    for userlog in query:
        userlog.date += td_days
        db_session.add(userlog)

    db_session.commit()
    return json_response(ADMIN_SUCCESS)

@admin.route('/find_user')
def FindUser():
    name = request.args.get('name', type=str)

    query = db_session.query(User).filter(User.user_name.ilike('%'+name+'%'))
    ulist = []
    for u in query:
        ulist.append([u.user_index, u.user_name])

    rdata = {'ulist': ulist}
    rdata.update(ADMIN_SUCCESS)
    return json_response(rdata)

@admin.route('/dummy_learn')
def DummyLearn():
    uid = request.args.get('uid', type=int)
    days = request.args.get('days', type=int)

    init_data(uid, init_pool=False)
    user = get_user(uid)
    dt_check = datetime(2015, 12, 11, 18, 0, 0)
    MAX_WORD_ID = 1514
    PROBLEM_TYPE_LIST = ["KANJI", "KANA", "MEANING"]

    track_list = list()
    wait_list = [i for i in range(1, MAX_WORD_ID+1)]
    last_dt = datetime.now()
    for after_days in range(0, days):
        dt_day = user.created + timedelta(days=after_days)
        dt_study = list()
        if randrange(0, 2) == 0:
            dt_study.append(dt_day + timedelta(seconds=randrange(0, 28700)))
            dt_study.append(dt_day + timedelta(seconds=randrange(28800, 57500)))
            dt_study.append(dt_day + timedelta(seconds=randrange(57600, 86300)))
        else:
            dt_study.append(dt_day + timedelta(seconds=randrange(0, 21500)))
            dt_study.append(dt_day + timedelta(seconds=randrange(21600, 43100)))
            dt_study.append(dt_day + timedelta(seconds=randrange(43200, 64700)))
            dt_study.append(dt_day + timedelta(seconds=randrange(64800, 86300)))

        for dt in dt_study:
            new_word_count = 20 if after_days == 0 else 4
            for i in range(0, new_word_count):
                new_word_id = wait_list.pop(randrange(0, len(wait_list)))
                track_list.append(new_word_id)
                for j in range(0, 3):
                    dt += timedelta(seconds=2)
                    #print dt, "new", new_word_id
                    command_learn(user, dt, new_word_id, choice(PROBLEM_TYPE_LIST), True)

            for i in range(0, randint(20, 30)):
                word_id = choice(track_list)
                dt += timedelta(seconds=2)
                #print dt, "rev", word_id
                command_learn(user, dt, word_id, choice(PROBLEM_TYPE_LIST), True)
            last_dt = dt

    ShardLearningRecord = get_sharding_class(LearningRecord, uid)
    for record in db_session.query(ShardLearningRecord):
        if memory_expectation(record, dt_check) < 0.1:
            last_dt += timedelta(seconds=2)
            command_learn(user, last_dt, record.word_id, choice(PROBLEM_TYPE_LIST), True)
            print last_dt, "zer", record.word_id, memory_expectation(record, dt_check)

    user.last_command = last_dt
    db_session.add(user)
    db_session.commit()

    return json_response(ADMIN_SUCCESS)

DUMMY_TEST_IDS = [[57,58,59],
    [62,63,65], [66,67,68], [69,70,71], [72,73,74], [75,76,77], 
    [78,79,80], [81,82,83], [84,85,86], [87,88,89]]
@admin.route('/dummy_reset')
def DummyReset():
    tid = request.args.get('tid', type=int)
    if tid < 1 or tid > 9:
        return json_response({'cmd': "ADMIN", 'result': "FAILED"})

    for i in range(0, 3):
        src_id = DUMMY_TEST_IDS[0][i]
        dst_id = DUMMY_TEST_IDS[tid][i]

        init_data(dst_id)
        src_user = get_user(src_id)
        dst_user = get_user(dst_id)
        dst_user.last_command = src_user.last_command
        db_session.add(dst_user)

        for ShardModel in SHARD_MODEL_DICT.keys():
            SrcShardModel = get_sharding_class(ShardModel, src_id)
            DstShardModel = get_sharding_class(ShardModel, dst_id)
            query = db_session.query(SrcShardModel)
            for row in query:
                db_session.add(row.clone(DstShardModel))

    db_session.commit()
    return json_response(ADMIN_SUCCESS)
