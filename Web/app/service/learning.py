# -*- coding: utf-8 -*-
# Created by Jinsung Lee
# learning_system module
import json
from datetime import datetime, date, timedelta
from random import randrange
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from common import get_file_path, get_file_mdatetime
from database import db_session
from models import LearningPool, LearningRecord, UserLog, get_sharding_class
from session import find_session
from user import get_user
from heuristics import memory_expectation

TEST_LEARNING = False
TEST_BASIC_DICTIONARY = False

TEMP_KANJI_DICTIONARY = \
    '{ "安": "편안할 안", "心": "마음 심", "全": "완전 전", "一": "한 일", '+\
    '"番": "차례 번", "杯": "잔 배", "月": "달 월" }'

TEMP_USER_DATA = \
    '{ "userName": "dummyUser", '+\
    '"log": [ [ "2015-11-10T00:00:00.000Z", 200, 80, 50, 40 ], [ "2015-11-11T00:00:00.000Z", 207, 82, 55, 62 ], [ "2015-11-12T00:00:00.000Z", 207, 83, 50, 15 ] ], '+\
    '"learningPool": { "0": [ "安心", "あんしん", "안심" ], "1": [ "一日", "いちにち", "1일; 초하루" ], "2": [ "", "ピアノ", "피아노" ] }, '+\
    '"learningRecord": { "0": ["2015-11-10T00:00:00.000Z", "2015-11-10T00:00:00.000Z", 1, 1, 1, 0, 1, 0, 0, "XOO" ] } }'

TEST_LEARNING_POOL_PATH = get_file_path('data/test_learning_pool.csv')
TEST_PROTO_DATA_PATH = get_file_path('data/test_proto_data.txt')
KANJI_DICTIONARY_PATH = get_file_path('data/kanji_dictionary.json')
RECENT_COUNT = 5

def kanji_dictionary_timestamp(): #-> datetime
    if TEST_LEARNING:
        if randrange(0, 4) == 0:
            return datetime.now()
        else:
            return datetime(1990, 8, 13)

    return get_file_mdatetime(KANJI_DICTIONARY_PATH)

def kanji_dictionary(): #-> data dict
    if TEST_LEARNING:
        data = json.loads(TEMP_KANJI_DICTIONARY)
        return data

    f = open(KANJI_DICTIONARY_PATH, 'r')
    data = json.loads(f.read())
    f.close()
    return data

def user_data_timestamp(session_id): #-> datetime
    if TEST_LEARNING:
        if randrange(0, 4) == 0:
            return datetime.now()
        else:
            return datetime(1990, 8, 13)

    user_index = find_session(session_id)
    user = get_user(user_index)
    return user.last_command

def user_data(session_id, sync_dt): #-> data dict
    if TEST_LEARNING:
        data = json.loads(TEMP_USER_DATA)
        return data

    user_index = find_session(session_id)
    user = get_user(user_index)
    data = {'userName': user.user_name}

    learningPoolDict = {}
    ShardLearningPool = get_sharding_class(LearningPool, user_index)
    query = db_session.query(ShardLearningPool).order_by('word_id').all()
    if TEST_BASIC_DICTIONARY and len(query) == 0: #temp code
        f = open(TEST_LEARNING_POOL_PATH, 'r')
        wordbook_import(user_index, f)
        query = db_session.query(ShardLearningPool).order_by('word_id').all()
    for l in query:
        learningPoolDict[l.word_id] = l.to_list()

    learningRecordDict = {}
    ShardLearningRecord = get_sharding_class(LearningRecord, user_index)
    query = db_session.query(ShardLearningRecord).order_by('word_id')
    for l in query:
        learningRecordDict[l.word_id] = l.to_list()

    userlog = get_userlog(user_index, sync_dt, True)
    db_session.commit()

    userLogList = []
    ShardUserLog = get_sharding_class(UserLog, user_index)
    query = db_session.query(ShardUserLog).order_by('date')
    for l in query:
        userLogList.append(l.to_list())

    data['learningPool'] = learningPoolDict
    data['learningRecord'] = learningRecordDict
    data['log'] = userLogList
    return data

def get_record(user_index, word_id, record_dt): #-> LearningRecord
    ShardLearningRecord = get_sharding_class(LearningRecord, user_index)
    query = db_session.query(ShardLearningRecord).filter(ShardLearningRecord.word_id==word_id)
    try:
        record = query.one()
    except NoResultFound:
        record = ShardLearningRecord(word_id, record_dt)
    
    db_session.add(record)
    return record

def get_userlog(user_index, check_dt, is_update=False): #-> UserLog
    ShardUserLog = get_sharding_class(UserLog, user_index)
    query = db_session.query(ShardUserLog)
    try:
        userlog = query.filter(ShardUserLog.date==check_dt.date()).one()
    except NoResultFound:
        userlog = ShardUserLog(check_dt.date())
        is_update = True

        yesterday_date = userlog.date-timedelta(days=1)
        if query.count() > 0 and not query.filter(ShardUserLog.date==yesterday_date).count():
            yesterday_log = ShardUserLog(yesterday_date)
            yesterday_dt = datetime.combine(yesterday_date, datetime.min.time())
            yesterday_dt += timedelta(hours=23, minutes=59)
            update_userlog(user_index, yesterday_log, yesterday_dt)
            db_session.add(yesterday_log)

    if is_update:
        update_userlog(user_index, userlog, check_dt)
    db_session.add(userlog)
    return userlog

def update_userlog(user_index, userlog, update_dt):
    ShardLearningPool = get_sharding_class(LearningPool, user_index)
    ShardLearningRecord = get_sharding_class(LearningRecord, user_index)
    poolQuery = db_session.query(ShardLearningPool).all()
    recordQuery = db_session.query(ShardLearningRecord).all()

    userlog.total = len(poolQuery)
    userlog.learning = len(recordQuery)
    userlog.memorized = 0
    for record in recordQuery:
        expectatation = memory_expectation(record, update_dt)
        userlog.memorized += expectatation
    userlog.memorized = int(userlog.memorized)

def command_learn(user, cmd_dt, word_id, problem_type, is_memorized):
    user_index = user.user_index
    record = get_record(user_index, word_id, cmd_dt)
    userlog = get_userlog(user_index, cmd_dt)

    if record.recent_date < cmd_dt:
        record.recent_date = cmd_dt

    if problem_type == "KANJI":
        if is_memorized:
            record.correct_kanji_count += 1
        else:
            record.wrong_kanji_count += 1
    elif problem_type == "KANA":
        if is_memorized:
            record.correct_kana_count += 1
        else:
            record.wrong_kana_count += 1
    elif problem_type == "MEANING":
        if is_memorized:
            record.correct_meaning_count += 1
        else:
            record.wrong_meaning_count += 1
    else: return

    next_result = 'O' if is_memorized else 'X'
    record.recent_result = record.recent_result if len(record.recent_result) < 5 else record.recent_result[1:5]
    record.recent_result += next_result
    if record.recent_result_straight(next_result):
        if (is_memorized):
            record.memory_level = min(10, record.memory_level+1)
        else:
            record.memory_level = max(1, record.memory_level-1)
    userlog.iteration += 1

    db_session.commit()

def command_addword(user, cmd_dt, word_id, kanji, kana, meaning, no_kanji_test, ex_sentence, ex_meaning):
    ShardLearningPool = get_sharding_class(LearningPool, user.user_index)
    pool = ShardLearningPool(word_id, kanji, kana, meaning, no_kanji_test, ex_sentence, ex_meaning)
    db_session.add(pool)
    db_session.commit()

def command_editword(user, cmd_dt, word_id, kanji, kana, meaning, no_kanji_test, ex_sentence, ex_meaning):
    ShardLearningPool = get_sharding_class(LearningPool, user.user_index)
    query = db_session.query(ShardLearningPool).filter(ShardLearningPool.word_id==word_id)
    pool = query.one()
    pool.edit(kanji, kana, meaning, no_kanji_test, ex_sentence, ex_meaning)
    db_session.add(pool)
    db_session.commit()

def command_deleteword(user, cmd_dt, word_id):
    user_index = user.user_index
    ShardLearningPool = get_sharding_class(LearningPool, user_index)
    ShardLearningRecord = get_sharding_class(LearningRecord, user_index)
    db_session.query(ShardLearningPool).filter(ShardLearningPool.word_id==word_id).delete()
    db_session.query(ShardLearningRecord).filter(ShardLearningRecord.word_id==word_id).delete()
    db_session.commit()

def wordbook_import(user_index, wordfile):
    ShardLearningPool = get_sharding_class(LearningPool, user_index)
    count = 0
    while True:
        line = wordfile.readline()
        if not line: break

        p = line.decode('utf-8').replace('\n', '').split(',')
        p_len = len(p)
        if p_len < 3: continue

        kanji = p[0]
        kana = p[1]
        meaning = p[2]
        ex_sentence = p[3] if p_len >= 4 else ""
        ex_meaning = p[4] if p_len >= 5 else ""
        pool = ShardLearningPool(None, kanji, kana, meaning, None, ex_sentence, ex_meaning)
        db_session.add(pool)
        count += 1

    db_session.commit()
    return count

def wordbook_export(user_index): #-> wordbook string
    ShardLearningPool = get_sharding_class(LearningPool, user_index)
    query = db_session.query(ShardLearningPool).order_by('word_id')
    filetext = ""
    for p in query:
        filetext += p.to_export()

    return filetext

def init_data(user_index, init_pool=True):
    if init_pool:
        ShardLearningPool = get_sharding_class(LearningPool, user_index)
        db_session.query(ShardLearningPool).delete()
    
    ShardLearningRecord = get_sharding_class(LearningRecord, user_index)
    ShardUserLog = get_sharding_class(UserLog, user_index)
    db_session.query(ShardLearningRecord).delete()
    db_session.query(ShardUserLog).delete()
    db_session.commit()

def load_data(user_index, dfile=None):
    init_data(user_index)

    ShardLearningPool = get_sharding_class(LearningPool, user_index)
    ShardLearningRecord = get_sharding_class(LearningRecord, user_index)
    ShardUserLog = get_sharding_class(UserLog, user_index)
    dt_now = datetime.now()

    if not dfile:
        dfile = open(TEST_PROTO_DATA_PATH, 'r')
    datas = dfile.readlines()
    line = 0
    is_word, is_record, is_log = False, False, False
    log_data, log_mem_min, log_mem_max = None, None, None
    pool0, record0, zero_word_id = None, None, 0
    while line < len(datas):
        if is_word:
            if datas[line] == "# End Section\n":
                is_word = False
                if pool0:
                    zero_word_id = word_id+1
                    pool0.word_id = zero_word_id
                    db_session.add(pool0)
                db_session.commit()
            else:
                word_id = int(datas[line].replace('\n',''))
                kanji = datas[line+1].decode('utf-8').replace('\n','')
                kana = datas[line+2].decode('utf-8').replace('\n','')
                meaning = datas[line+3].decode('utf-8').replace('\n','')
                pool = ShardLearningPool(word_id, kanji, kana, meaning, None, None, None)

                if word_id == 0:
                    pool0 = pool
                else:
                    db_session.add(pool)
                line += 4

        elif is_record:
            if datas[line] == "# End Section\n":
                is_record = False
                if record0:
                    record0.word_id = zero_word_id
                    db_session.add(record0)
                db_session.commit()
            else:
                word_id = int(datas[line].replace('\n',''))
                record = ShardLearningRecord(word_id, 
                    datetime.strptime(datas[line+1], "%Y-%m-%d %H:%M\n")) # encounter_date
                record.recent_date = datetime.strptime(datas[line+2], "%Y-%m-%d %H:%M\n")
                record.memory_level = int(datas[line+3].replace('\n',''))

                cd = datas[line+4].replace('\n','').split(' ')
                record.correct_kanji_count = int(cd[0])
                record.correct_kana_count = int(cd[1])
                record.correct_meaning_count = int(cd[2])
                record.wrong_kanji_count = int(cd[3])
                record.wrong_kana_count = int(cd[4])
                record.wrong_meaning_count = int(cd[5])

                rd = datas[line+5].replace(' \n','').split(' ') # 뒷 빈칸 제거
                if len(rd) > RECENT_COUNT:
                    rd = rd[len(rd)-RECENT_COUNT:]
                result = ""
                for s in rd:
                    result += 'O' if s == "True" else 'X'
                record.recent_result = result

                if word_id == 0:
                    record0 = record
                else:
                    db_session.add(record)
                line += 6

        elif is_log:
            if datas[line] == "# End Section\n":
                is_log = False
                if log_data:
                    iteration = int((log_mem_max-log_mem_min)*1.2)
                    userlog = ShardUserLog(log_data[0].date(), 
                        log_data[3], log_data[2], log_data[1], iteration)
                    db_session.add(userlog)
                db_session.commit()
            else:
                ld = datas[line].replace('\n','').split('\t')
                ld[0] = datetime.strptime(ld[0], "%Y-%m-%d %H:%M")
                ld[1], ld[2], ld[3] = int(ld[1]), int(ld[2]), int(ld[3])
                if not log_data:
                    log_mem_min = 0
                    log_mem_max = ld[1]
                elif log_data[0].date() == ld[0].date():
                    log_mem_min = min(log_mem_min, ld[1])
                    log_mem_max = max(log_mem_max, ld[1])
                elif log_data[0].date() < ld[0].date():
                    iteration = int((log_mem_max-log_mem_min)*1.2)
                    userlog = ShardUserLog(log_data[0].date(), 
                        log_data[3], log_data[2], log_data[1], iteration)
                    db_session.add(userlog)
                    log_mem_min = min(log_data[1], ld[1])
                    log_mem_max = max(log_data[1], ld[1])
                log_data = ld
                line += 1

        else:
            if datas[line] == "# Word Book\n":
                is_word = True
            elif datas[line] == "# Word Record\n":
                is_record = True
            elif datas[line] == "# Log\n":
                is_log = True
            line += 1

    user = get_user(user_index)
    user.last_command = dt_now
    db_session.add(user)
    db_session.commit()
