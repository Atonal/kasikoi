# Created by Jinsung Lee
# session_servcie module
from werkzeug.contrib.cache import SimpleCache
from sqlalchemy.orm.exc import NoResultFound

from database import db_session
from models import Session

_session_cache = SimpleCache()

def new_session(user_index): #-> session_id
    session = Session(user_index)
    db_session.add(session)
    db_session.commit()
    _session_cache.set(session.session_id, session.user_index)
    return session.session_id

def find_session(session_id): #-> user_index
    user_index = _session_cache.get(session_id)
    if user_index:
        return user_index

    query = db_session.query(Session).filter(Session.session_id==session_id)
    try:
        session = query.one()
        _session_cache.set(session.session_id, session.user_index)
        return session.user_index
    except NoResultFound, e:
        raise e

def end_session(session_id):
    _session_cache.delete(session_id)
    query = db_session.query(Session).filter(Session.session_id==session_id)
    try:
        session = query.one()
        db_session.delete(session)
        db_session.commit()
    except NoResultFound, e:
        raise e
