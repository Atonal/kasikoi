# Created by Jinsung Lee
# login_service module
from sqlalchemy.orm.exc import NoResultFound

from database import db_session
from models import Login, Session, User
from service.session import new_session, find_session, end_session
from service.user import user_hashed_password, handle_login

def login(user_name, password): #-> login_result(boolean), session_id
    session_id = -1
    errorText = "UNKNOWN_ERROR"
    query = db_session.query(Login).filter(Login.user_name==user_name)
    try:
        login = query.one()
        if login.password == user_hashed_password(login.user_index, password):
            session_id = new_session(login.user_index)
        else:
            errorText = "WRONG_PASSWORD"
    except NoResultFound, e:
        session_id = -1
        errorText = "USER_NOT_FOUND"
    return session_id > 0, session_id, errorText

def autologin(session_id):
    try:
        user_index = find_session(session_id)
    except NoResultFound, e:
        raise e

    try:
        handle_login(user_index, session_id)
    except NoResultFound, e:
        end_session(session_id)
        raise e

def logout(session_id):
    try:
        end_session(session_id)
    except NoResultFound, e:
        raise e
