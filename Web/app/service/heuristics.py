# -*- coding: utf-8 -*-
# Created by Jinsung Lee
import math
from database import db_session
from models import LearningRecord, get_sharding_class

FORGET_STARTS = {
	1: (1.0 / (24.0 * 60.0)) * 1,
	2: (1.0 / (24.0 * 60.0)) * 3,
	3: (1.0 / (24.0 * 60.0)) * 10,
	4: 1.0,
	5: 3.0,
	6: 7.0,
	7: 15.0,
	8: 30.0,
	9: 90.0,
	10: 180.0}
MEMORY_EXPECTATION_AT_FORGET_STARTS = 0.8

def memory_expectation(record=None, expect_datetime=None, memory_level=None, wrong_count=None, days_after=None):
    # LearningRecord, datetime -> float
    if record:
        memory_level = record.memory_level
        wrong_count = record.recent_wrong_count()
        td = expect_datetime - record.recent_date
        days_after = td.days + td.seconds / 86400.0 

    #print memory_level, wrong_count, days_after
    forget_starts = FORGET_STARTS[memory_level] / (wrong_count + 1)
    forget_ends = forget_starts * 3

    if days_after < forget_starts:
        return map_cosine(days_after, 0.0, forget_starts, MEMORY_EXPECTATION_AT_FORGET_STARTS, 1.0)
    elif days_after < forget_ends:
        return map_cosine(days_after, forget_starts, forget_ends, 0.0, MEMORY_EXPECTATION_AT_FORGET_STARTS)
    else:
        return 0

def map_cosine(domain, minDomain, maxDomain, minValue, maxValue):
    ratio = (domain - minDomain) / (maxDomain - minDomain)
    theta = ratio * math.pi / 2.0
    cos = math.cos(theta)
    value = (maxValue - minValue) * cos + minValue
    return round(value, 3)
