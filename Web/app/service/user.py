# Created by Jinsung Lee
# user_service module
import base64, hashlib
from datetime import datetime
from werkzeug.contrib.cache import SimpleCache
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from database import db_session
from models import Login, User, LearningPool, LearningRecord, UserLog
from models import SHARD_MODEL_DICT, create_sharding_class, delete_sharding_class

_user_name_cache = SimpleCache()
_last_session_id_cache = SimpleCache()

def get_user(user_index): #-> model_User
    query = db_session.query(User).filter(User.user_index==user_index)
    try:
        user = query.one()
        return user
    except NoResultFound, e:
        return None

def user_hashed_password(user_index, password):
    hex_password = hashlib.sha256("%s%s%s" % (password, "DEBUG_SALT", user_index)).hexdigest()
    return base64.b64encode(hex_password)

def user_name(user_index): #-> string
    name = _user_name_cache.get(user_index)
    if name:
        return name

    user = get_user(user_index)
    if user is None:
        return "[unknown]"

    _user_name_cache.set(user_index, user.user_name)
    return user.user_name

def handle_login(user_index, session_id):
    user = get_user(user_index)
    if user is None:
        raise NoResultFound()

    user.last_login = datetime.now()
    user.last_session_id = session_id
    db_session.commit()
    _last_session_id_cache.set(user_index, session_id)

def is_current_session(user_index, session_id): #-> boolean
    last_session_id = _last_session_id_cache.get(user_index)
    if last_session_id:
        return last_session_id == session_id

    user = get_user(user_index)
    if user is None:
        raise NoResultFound()
    return user.last_session_id == session_id

def has_user_name(user_name): #-> boolean
    query = db_session.query(Login).filter(Login.user_name==user_name)
    try:
        query.one()
        return True
    except NoResultFound, e:
        return False

class NewUserResult():
    Success, Duplicated = range(2)

def create_new_user(user_name, password):
    user = User(user_name)
    try:
        db_session.add(user)
        db_session.commit()
        db_session.add(Login(user.user_index, user_name, user_hashed_password(user.user_index, password)))
        db_session.commit()

        for model in SHARD_MODEL_DICT.keys():
            create_sharding_class(model, user.user_index)
        _user_name_cache.set(user.user_index, user.user_name)
        return NewUserResult.Success
    except IntegrityError, e:
        db_session.rollback()
        return NewUserResult.Duplicated
