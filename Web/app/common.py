# Created by Jinsung Lee
import os
from datetime import datetime

def date_to_jsonstr(d):
    return d.strftime("%Y-%m-%dT00:00:00.000Z")

def datetime_to_jsonstr(dt):
    base_str = dt.strftime("%Y-%m-%dT%H:%M:%S")
    ms_str = dt.strftime("%f")[:3]
    return "%s.%sZ" % (base_str, ms_str)

def jsonstr_to_datetime(s):
    return datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%fZ")

def get_file_path(filename):
    # under Web\app\
    dirpath = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(dirpath, filename)
    return filepath

def get_file_mdatetime(filepath):
    mtime = os.path.getmtime(filepath)
    return datetime.fromtimestamp(mtime)
