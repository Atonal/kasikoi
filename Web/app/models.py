# -*- coding: utf-8 -*-
# Created by Jinsung Lee
from sqlalchemy import Column, Boolean, SmallInteger, Integer, String, Date, DateTime
from sqlalchemy.sql import func

from database import engine, db_session, Base
from common import date_to_jsonstr as djson, datetime_to_jsonstr as dtjson

class Login(Base):
    __tablename__ = 'logins'
    user_index = Column(Integer, primary_key=True)
    user_name = Column(String(64), unique=True)
    password = Column(String(128))

    def __init__(self, user_index, user_name, password):
        self.user_index = user_index
        self.user_name = user_name
        self.password = password

    def __repr__(self):
        return "Login(%d, %s)" % (self.user_index, self.user_name)

class Session(Base):
    __tablename__ = 'sessions'
    session_id = Column(Integer, primary_key=True)
    user_index = Column(Integer)

    def __init__(self, user_index):
        self.user_index = user_index

    def __repr__(self):
        return "Session(%d, %d)" % (self.session_id, self.user_index)

class User(Base):
    __tablename__ = 'users'
    user_index = Column(Integer, primary_key=True)
    user_name = Column(String(64), unique=True)
    last_session_id = Column(Integer, default=0)
    created = Column(DateTime, default=func.now())
    last_login = Column(DateTime, default=func.now())
    last_command = Column(DateTime, default=func.now())
    forgetting_curve = Column(String(1024))

    def __init__(self, user_name):
        self.user_name = user_name

    def __repr__(self):
        return "User(%d, %s)" % (self.user_index, self.user_name)

class LearningPool(Base):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    word_id = Column(Integer, primary_key=True)
    kanji_notation = Column(String(255), nullable=True)
    kana_notation = Column(String(255))
    meaning = Column(String(255), nullable=True)
    no_kanji_test = Column(Boolean, default=False)
    ex_sentence = Column(String(255), nullable=True)
    ex_meaning = Column(String(255), nullable=True)

    def __init__(self, word_id, kanji_notation, kana_notation, meaning, no_kanji_test, ex_sentence, ex_meaning):
        if word_id: self.word_id = word_id
        self.edit(kanji_notation, kana_notation, meaning, no_kanji_test, ex_sentence, ex_meaning)

    def __repr__(self):
        return "%s(%d)" % (self.__class__.__name__, self.word_id)

    def clone(self, ShardPool):
        return ShardPool(self.word_id,
            self.kanji_notation, self.kana_notation, self.meaning,
            self.no_kanji_test, self.ex_sentence, self.ex_meaning)

    def to_list(self):
        return [self.kanji_notation, self.kana_notation, self.meaning,
            self.no_kanji_test, self.ex_sentence, self.ex_meaning]

    def to_export(self):
        return "%s,%s,%s,%s,%s\n" % (self.kanji_notation, self.kana_notation, self.meaning, 
            self.ex_sentence, self.ex_meaning)

    def edit(self, kanji_notation, kana_notation, meaning, no_kanji_test, ex_sentence, ex_meaning):
        if kanji_notation: self.kanji_notation = kanji_notation
        if kana_notation: self.kana_notation = kana_notation
        if meaning: self.meaning = meaning
        if no_kanji_test is not None: self.no_kanji_test = no_kanji_test
        if ex_sentence: self.ex_sentence = ex_sentence 
        if ex_meaning: self.ex_meaning = ex_meaning

class LearningRecord(Base):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    word_id = Column(Integer, primary_key=True)
    encounter_date = Column(DateTime, default=func.now())
    recent_date = Column(DateTime, default=func.now())
    memory_level = Column(Integer, default=1)
    correct_kanji_count = Column(Integer, default=0)
    correct_kana_count = Column(Integer, default=0)
    correct_meaning_count = Column(Integer, default=0)
    wrong_kanji_count = Column(Integer, default=0)
    wrong_kana_count = Column(Integer, default=0)
    wrong_meaning_count = Column(Integer, default=0)
    recent_result = Column(String(16))

    def __init__(self, word_id, encounter_date, recent_date=None, memory_level=1,
        ckanji=0, ckana=0, cmeaning=0, wkanji=0, wkana=0, wmeaning=0, recent_result=""):
        self.word_id = word_id
        self.encounter_date = encounter_date
        self.recent_date = recent_date if recent_date else encounter_date
        self.memory_level = memory_level
        self.correct_kanji_count = ckanji
        self.correct_kana_count = ckana
        self.correct_meaning_count = cmeaning
        self.wrong_kanji_count = wkanji
        self.wrong_kana_count = wkana
        self.wrong_meaning_count = wmeaning
        self.recent_result = recent_result

    def __repr__(self):
        return "%s(%d)" % (self.__class__.__name__, self.word_id)

    def clone(self, ShardRecord):
        return ShardRecord(self.word_id,
            self.encounter_date, self.recent_date, self.memory_level,
            self.correct_kanji_count, self.correct_kana_count, self.correct_meaning_count,
            self.wrong_kanji_count, self.wrong_kana_count, self.wrong_meaning_count,
            self.recent_result)
    
    def to_list(self):
        return [dtjson(self.encounter_date), dtjson(self.recent_date), 
            self.memory_level, self.correct_kanji_count, self.correct_kana_count, self.correct_meaning_count,
            self.wrong_kanji_count, self.wrong_kana_count, self.wrong_meaning_count, self.recent_result]

    def recent_wrong_count(self):
        return self.recent_result.count('X')

    def recent_result_straight(self, result):
        i = len(self.recent_result)-1
        count = 3
        while i > 0 and count > 0:
            if self.recent_result[i] != result: return False
            i -= 1
            count -= 1
        return True

class UserLog(Base):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    date = Column(Date, primary_key=True, default=func.now())
    total = Column(Integer, default=0)
    learning = Column(Integer, default=0)
    memorized = Column(Integer, default=0)
    iteration = Column(Integer, default=0)

    def __init__(self, log_date, total=0, learning=0, memorized=0, iteration=0):
        self.date = log_date
        self.total = total
        self.learning = learning
        self.memorized = memorized
        self.iteration = iteration

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, self.date.isoformat())

    def clone(self, ShardUserLog):
        return ShardUserLog(self.date,
            self.total, self.learning, self.memorized, self.iteration)

    def to_list(self):
        return [djson(self.date), self.total, self.learning, self.memorized, self.iteration]

BASIC_MODEL_LIST = [Session, Login, User]
SHARD_MODEL_DICT = {LearningPool: 'learning_pool_', LearningRecord: 'learning_record_', UserLog: 'user_log_'}

def get_sharding_class(BaseClass, user_index):
    class_name = '%s%d' % (BaseClass.__name__, user_index)
    table_name = '%s%d' % (SHARD_MODEL_DICT[BaseClass], user_index)
    cls = type(class_name, (BaseClass,), {'__tablename__': table_name})
    return cls

def create_sharding_class(BaseClass, user_index):
    cls = get_sharding_class(BaseClass, user_index)
    cls.__table__.create(bind=engine, checkfirst=True)

def delete_sharding_class(BaseClass, user_index):
    cls = get_sharding_class(BaseClass, user_index)
    cls.__table__.drop(bind=engine, checkfirst=True)

def db_init():
    for BasicModel in BASIC_MODEL_LIST:
        BasicModel.__table__.create(bind=engine, checkfirst=True)

def db_drop():
    for BasicModel in BASIC_MODEL_LIST:
        BasicModel.__table__.drop(bind=engine, checkfirst=True)

def user_delete(user=None, user_index=None, commit=True):
    if user:
        user_index = user.user_index
    if user_index:
        user = db_session.query(User).filter(User.user_index==user_index).one()

    for ShardModel in SHARD_MODEL_DICT.keys():
        delete_sharding_class(ShardModel, user_index)

    if commit:
        for BasicModel in BASIC_MODEL_LIST:
            db_session.query(BasicModel).filter(BasicModel.user_index==user_index).delete()
        db_session.commit()

def db_user_clear(except_ids=[]):
    query = db_session.query(User).filter(User.user_index.notin_(except_ids))
    for user in query:
        user_delete(user=user, commit=False)

    for BasicModel in BASIC_MODEL_LIST:
        db_session.query(BasicModel).filter(BasicModel.user_index.notin_(except_ids)).delete(synchronize_session=False)
    db_session.commit()

