﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSAjaxScratch
{
    class Program
    {
        static void Main(string[] args)
        {
            var response = AjaxService.SendRequest("{ \"test\": true }", "ajax/testEcho");
            Console.WriteLine(response);

            Console.ReadKey();
        }
    }
}
