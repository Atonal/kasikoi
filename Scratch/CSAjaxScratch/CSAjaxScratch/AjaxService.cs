﻿using System.IO;
using System.Net;
using System.Text;

namespace CSAjaxScratch
{
    public static class AjaxService
    {
        static readonly string ServerUrl = "http://143.248.233.58:5000/";

        public static string SendRequest(string jsonString, string requestUrl)
        {
            requestUrl.TrimStart('\\', '/');

            var request = WebRequest.CreateHttp(ServerUrl + requestUrl);
            var byteArray = Encoding.UTF8.GetBytes(jsonString);
            request.ContentType = "application/json";
            request.Method = "POST";
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(byteArray, 0, byteArray.Length);
            }

            var response = request.GetResponse();
            using (var responseStream = new StreamReader(response.GetResponseStream()))
            {
                var result = responseStream.ReadToEnd();
                return result;
            }
        }
    }
}
