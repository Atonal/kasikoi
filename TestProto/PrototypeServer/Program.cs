﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace JTestServer
{
    class Program
    {
        static Socket listener;

        static void Main(string[] args)
        {
            Console.WriteLine("CS408 Test Statistics Server");

            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 2525);
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(ipEndPoint);
            listener.Listen(100);
            listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
            Console.WriteLine("Wating for user data");

            while (true)
                Thread.Sleep(1000);
        }

        static void AcceptCallback(IAsyncResult ar)
        {
            var listener = (Socket)ar.AsyncState;
            var handler = listener.EndAccept(ar);

            try
            {
                var data = ReadRequest(handler);
                int newLineIndex = data.IndexOf('\n');
                string userName = data.Substring(0, newLineIndex);
                try
                {
                    if (!Directory.Exists("Data\\"))
                        Directory.CreateDirectory("Data\\");
                    File.WriteAllText("Data\\" + userName + ".txt", data.Replace("\n", "\r\n"));
                    handler.Send(Encoding.UTF8.GetBytes("OK"));
                    Console.WriteLine("request from " + userName + " accepted");
                }
                catch (Exception e)
                {
                    handler.Send(Encoding.UTF8.GetBytes("FAILED"));
                    Console.WriteLine("request from " + userName + " failed");
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
            Console.WriteLine("Wating for user data");
        }

        static byte[] buffer = new byte[5 * 1024 * 1024];
        static string ReadRequest(Socket requestedSocket)
        {
            var length = 0;
            var contentLength = 0;
            while (length < buffer.Length)
            {
                var read = requestedSocket.Receive(buffer, length, 1, SocketFlags.None);
                length += read;

                if (length >= 2)
                {
                    if (buffer[length - 2] == '\r' && buffer[length - 1] == '\n')
                    {
                        var headerLine = Encoding.UTF8.GetString(buffer, 0, length);
                        length = 0;
                        headerLine = headerLine.ToLower();
                        if (headerLine.StartsWith("content-length: "))
                        {
                            var colonPos = headerLine.IndexOf(':');
                            var lengthString = headerLine.Substring(colonPos + 1);
                            contentLength = int.Parse(lengthString);
                        }
                        if (headerLine == "\r\n")
                            break;
                    }
                }
            }

            if (contentLength > buffer.Length)
                return "";

            length = 0;
            while (length < contentLength)
            {
                var remains = buffer.Length - length;
                var read = requestedSocket.Receive(buffer, length, 1, SocketFlags.None);
                length += read;
            }

            return Encoding.UTF8.GetString(buffer, 0, length);
        }
    }
}
