﻿using System;
using System.Collections.Generic;
using Android.Views;

namespace CS408.JTest
{
    public class SimpleTestView : TestView
    {
        List<WordState> wordList;
        int index = 0;

        public SimpleTestView(MainActivity activity)
            : base(activity, LearningMethod.Simple)
        {
            checkDifficult.Visibility = ViewStates.Gone;
            checkGood.Visibility = ViewStates.Gone;
            checkEasy.Visibility = ViewStates.Gone;

            var state = activity.GetMethodState(LearningMethod.Simple);
            wordList = new List<WordState>();
            foreach (var w in state.WordList)
                wordList.Add(w);

            Random rand = new Random();
            for (int i = 0; i < wordList.Count; i++)
            {
                var from = i;
                var to = rand.Next(wordList.Count);
                var temp = wordList[from];
                wordList[from] = wordList[to];
                wordList[to] = temp;
            }
            Console.WriteLine("SimpleTestView count = " + wordList.Count);

            StartTest(wordList[index].Word);
        }

        public override void OnStop()
        {
            RecordTime();
        }

        protected override void NextWord()
        {
            var w = wordList[index];
            bool isMemorized = !checkForgot.Checked;

            state.RecordCurve(LearningMethod.Simple, w, isMemorized);

            Console.WriteLine("SimpleTest before");
            LogWord(w);

            if (isMemorized)
                w.MarkCorrect();
            else
                w.MarkWrong();

            Console.WriteLine("SimpleTest after");
            LogWord(w);

            if (IsTimeToNextTest)
            {
                RecordTime();
                activity.NextTestType();   
            }
            else
            {
                index = (index + 1) % wordList.Count;
                StartTest(wordList[index].Word);
            }
        }
    }
}

