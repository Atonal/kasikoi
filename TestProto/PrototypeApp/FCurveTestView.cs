﻿using System;
using System.Collections.Generic;
using Android.Views;

namespace CS408.JTest
{
    public class FCurveTestView : TestView
    {
        List<WordState> memorySet;
        List<WordState> otherSet;
        Random rand = new Random();

        int index = 0;

        public FCurveTestView(MainActivity activity)
            : base(activity, LearningMethod.FCurve)
        {
            checkDifficult.Visibility = ViewStates.Gone;
            checkGood.Visibility = ViewStates.Gone;
            checkEasy.Visibility = ViewStates.Gone;

            var state = activity.GetMethodState(LearningMethod.FCurve);
            memorySet = new List<WordState>();
            otherSet = new List<WordState>();
            foreach (var w in state.WordList)
            {
                otherSet.Add(w);
            }

            FillMemorySet();
            StartTest(memorySet[index].Word);
        }

        void FillMemorySet()
        {
            int weight = WeightSum();
            while (otherSet.Count > 0 && memorySet.Count < 10)
            {
                memorySet.Add(SelectWord(ref weight));
            }

            for (int i = 0; i < memorySet.Count; i++)
            {
                var from = i;
                var to = rand.Next(memorySet.Count);
                var temp = memorySet[from];
                memorySet[from] = memorySet[to];
                memorySet[to] = temp;
            }
        }

        int WeightSum()
        {
            int sum = 0;
            foreach (var w in otherSet)
            {
                w.UpdateWeight();
                sum += w.Weight;
            }
            return sum;
        }

        WordState SelectWord(ref int weightSum)
        {
            int targetSum = rand.Next(weightSum);
            int partialSum = 0;
            for (int i = 0; i < otherSet.Count; i++)
            {
                var w = otherSet[i];
                partialSum += w.Weight;
                if (partialSum > targetSum)
                {
                    weightSum -= w.Weight;
                    otherSet.RemoveAt(i);
                    return w;
                }
            }

            return otherSet[0];
        }


        protected override void NextWord()
        {
            var w = memorySet[index];
            bool isMemorized = !checkForgot.Checked;

            state.RecordCurve(LearningMethod.FCurve, w, isMemorized);

            Console.WriteLine("FCurveTest before");
            LogWord(w);

            if (isMemorized)
            {
                w.MarkCorrect();
                memorySet.RemoveAt(index);
                otherSet.Add(w);
                index--;
            }
            else
                w.MarkWrong();
            w.UpdateState();

            Console.WriteLine("FCurveTest after");
            LogWord(w);

            if (IsTimeToNextTest)
            {
                RecordTime();
                activity.NextTestType();   
            }
            else
            {
                index = index + 1;
                if (index >= memorySet.Count)
                {
                    FillMemorySet();
                    index = 0;
                }
                StartTest(memorySet[index].Word);
            }
        }

        public override void OnStop()
        {
            RecordTime();
        }
    }
}

