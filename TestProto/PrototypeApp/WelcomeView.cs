﻿using System;
using Android.Widget;
using Android.App;
using System.Net;
using System.Text;

namespace CS408.JTest
{
    public class WelcomeView
    {
        Button start;
        Button reset;
        Button send;
        MainActivity activity;

        public WelcomeView(MainActivity activity)
        {
            this.activity = activity;
            activity.SetContentView (Resource.Layout.Welcome);
            start = activity.FindViewById<Button> (Resource.Id.start);
            reset = activity.FindViewById<Button> (Resource.Id.reset);
            send = activity.FindViewById<Button> (Resource.Id.send);
            activity.LoadLearningState();
            UpdateButtonEnable();

            start.Click += delegate {
                if (activity.LearningState == null || activity.LearningState.Stage == LearningStage.NewComer)
                {
                    var dialog = new AlertDialog.Builder(activity);
                    dialog.SetTitle("사용자 이름 입력");
                    dialog.SetMessage(" ");
                    EditText input = new EditText(activity);
                    dialog.SetView(input);
                    dialog.SetPositiveButton("완료", (sender, e) =>
                        {
                            if (string.IsNullOrEmpty(input.Text))
                            {
                                activity.ShowAlertPopup("사용자 이름은 빈 문자열이면 안 됩니다.");
                            }
                            else if (input.Text.Length > 5)
                            {
                                activity.ShowAlertPopup("사용자 이름은 최대 5자만 허용됩니다.");
                            }
                            else
                            {
                                activity.LearningState.StartLearning(input.Text);
                                var defaultWords = activity.GetString(Resource.String.default_wordbook);
                                var wordBook = WordBook.FromDefault(defaultWords);
                                wordBook.Save();
                                activity.MoveToTest(wordBook);
                            }
                        });
                    dialog.Show();
                }
                else
                {
                    activity.MoveToTest(null);
                }
            };
            reset.Click += delegate {
                var dialog = new AlertDialog.Builder(activity);
                dialog.SetTitle("경고!");
                dialog.SetMessage("정말로 학습 데이터를 초기화 하겠습니까?");
                dialog.SetPositiveButton("예", (sender, e) =>
                    {
                        activity.ResetLearningState();
                        UpdateButtonEnable();
                    });
                dialog.SetNegativeButton("아니오", delegate {});
                dialog.Show();
            };
            send.Click += delegate {
                try {
                    var dataString = activity.LearningState.ToString();
                    var data = Encoding.UTF8.GetBytes(dataString);
                    var request = WebRequest.Create("http://143.248.233.58:2525");
                    request.Method = "POST";
                    request.ContentLength = data.Length;
                    request.ContentType = "application/x-www-form-urlencoded";
                    var stream = request.GetRequestStream();
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    activity.ShowAlertPopup("전송되었습니다.");
                    send.Enabled = false;
                }
                catch (Exception e)
                {
                    activity.ErrorPopup("통계 정보 전송 중에 문제가 발생했습니다.", e);
                }
            };
        }

        void UpdateButtonEnable()
        {
            bool newComerOrNotLoaded = (activity.LearningState == null)
                || (activity.LearningState.Stage == LearningStage.NewComer);
            
            reset.Enabled = !newComerOrNotLoaded || (activity.LearningState == null);
            send.Enabled = !newComerOrNotLoaded;
        }
    }
}

