﻿using System;
using System.Collections.Generic;
using Android.Views;

namespace CS408.JTest
{
    public class AnkiTestView : TestView
    {
        List<WordState> wordList;
        int index = 0;

        public AnkiTestView(MainActivity activity)
            : base(activity, LearningMethod.Anki)
        {
            checkMemorized.Visibility = ViewStates.Gone;

            var state = activity.GetMethodState(LearningMethod.Anki);
            wordList = new List<WordState>();
            foreach (var w in state.WordList)
            {
                if (w.ReservedDate <= DateTime.Today)
                    wordList.Add(w);
            }
            if (wordList.Count == 0)
                wordList = new List<WordState>(state.WordList);

            Random rand = new Random();
            for (int i = 0; i < wordList.Count; i++)
            {
                var from = i;
                var to = rand.Next(wordList.Count);
                var temp = wordList[from];
                wordList[from] = wordList[to];
                wordList[to] = temp;
            }
            Console.WriteLine("AnkiTestView count = " + wordList.Count);

            StartTest(wordList[index].Word);
        }

        public override void OnStop()
        {
            RecordTime();
        }

        protected override void NextWord()
        {
            var w = wordList[index];
            bool isMemorized = !checkForgot.Checked;

            state.RecordCurve(LearningMethod.Anki, w, isMemorized);

            Console.WriteLine("AnkiTest before");
            LogWord(w);

            if (isMemorized)
            {
                w.MarkCorrect();
                wordList.Remove(w);
                index--;
                if (checkEasy.Checked)
                    w.MarkEasy();
                else if (checkGood.Checked)
                    w.MarkGood();
                else if (checkDifficult.Checked)
                    w.MarkDifficult();
            }
            else
            {
                w.MarkWrong();
            }

            Console.WriteLine("AnkiTest after");
            LogWord(w);

            if (IsTimeToNextTest || wordList.Count == 0)
            {
                RecordTime();
                activity.NextTestType();   
            }
            else
            {
                index = (index + 1) % wordList.Count;
                StartTest(wordList[index].Word);
            }
        }
    }
}

