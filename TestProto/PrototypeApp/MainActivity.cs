﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Content.PM;

namespace CS408.JTest
{
    [Activity (Label = "KASIKOI Test", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{
        public LearningState LearningState { private set; get; }
        public WordBook WordBook { private set; get; }
        Dictionary<LearningMethod, MethodState> methodStates = new Dictionary<LearningMethod, MethodState>();
        TestView runningTest;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            new WelcomeView(this);
		}

        protected override void OnStop()
        {
            if (runningTest != null)
                runningTest.OnStop();

            if (LearningState != null)
            {
                foreach (var state in methodStates.Values)
                {
                    var snapShot = LearningState.GetStateSnapShot(state.Method);
                    snapShot.UtilizeTimeInSecond = LearningState.GetUtilizeTime(state.Method);
                    state.RecordSnapShot(snapShot);
                    state.Save();
                }
                LearningState.Save();
            }

            if (WordBook != null)
                WordBook.Save();
            
            base.OnStop();
        }

        protected override void OnResume()
        {
            base.OnResume();

            runningTest = null;
            new WelcomeView(this);
        }

        public void LoadLearningState()
        {
            try
            {
                LearningState = LearningState.Load();
                methodStates.Clear();
                foreach (var m in LearningMethods.AllMethods)
                    methodStates.Add(m, MethodState.Load(m));
            }
            catch (Exception exception)
            {
                ErrorPopup("학습 데이터를 불러오는 도중 오류가 발생했습니다.", exception);
                LearningState = null;
            }
        }

        public void ResetLearningState()
        {
            try
            {
                LearningState.Reset();
                LearningState = LearningState.Load();

                MethodState.ResetAll();
                foreach (var m in LearningMethods.AllMethods)
                    methodStates[m] = MethodState.Load(m);
                
                ShowAlertPopup("학습 데이터가 초기화 되었습니다.");
            }
            catch (Exception exception)
            {
                ErrorPopup("학습 데이터 초기화 도중 오류가 발생했습니다.", exception);
            }
        }

        public void ErrorPopup(string contextDescription, Exception exception)
        {
            var dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("오류 발생!");
            dialog.SetMessage(contextDescription + "\n자세한 내용을 보시겠습니까?");
            dialog.SetPositiveButton("예", (sender, e) =>
                {
                    ShowAlertPopup(exception.ToString());
                });
            dialog.SetNegativeButton("아니오", delegate {});
            dialog.Show();
        }

		public void ShowAlertPopup(string content)
		{
			var dialog = new AlertDialog.Builder(this);
			dialog.SetTitle("알림");
			dialog.SetMessage(content);
			dialog.SetPositiveButton("확인", (sender, e) =>
				{
				});
			dialog.Show();
		}

        public MethodState GetMethodState(LearningMethod method)
        {
            return methodStates[method];
        }

        public void MoveToTest(WordBook wordBook)
        {
            if (wordBook == null)
                WordBook = WordBook.FromSaveData();
            else
                WordBook = wordBook;
            
            if (LearningState.Stage == LearningStage.FirstTest)
                new FirstTestView(this);
            else
                NextTestType();
        }

        public void NextTestType()
        {
            foreach (var m in LearningMethods.AllMethods)
            {
                int unknownCount = 0;
                foreach (var w in methodStates[m].WordList)
                {
                    if (!w.RecentlyChecked)
                        unknownCount++;
                }
                if (unknownCount < 10 && WordBook.Count > 0)
                {
                    LearningState.AddMoreWord();
                    new FirstTestView(this);
                    return;
                }
            }

            var method = LearningState.MethodWithMinimumTime;
            if (method == LearningMethod.Simple)
            {
                runningTest = new SimpleTestView(this);
            }
            else if (method == LearningMethod.Anki)
            {
                runningTest = new AnkiTestView(this);
            }
            else if (method == LearningMethod.FCurve)
            {
                runningTest = new FCurveTestView(this);
            }
        }
	}
}


