﻿using System;
using Android.Widget;
using Android.Views;
using System.Threading;

namespace CS408.JTest
{
    public abstract class TestView
    {
        protected MainActivity activity;

        protected ProgressBar timeBar;
        protected TextView textKanji;
        protected TextView textKana;
        protected TextView textMeaning;
        protected CheckBox checkMemorized;
        protected CheckBox checkForgot;
        protected CheckBox checkDifficult;
        protected CheckBox checkGood;
        protected CheckBox checkEasy;
        protected CheckBox[] checks;
        protected Button buttonNext;

        protected LearningState state;

        Timer timer;
        bool isProblemState = false;
        protected int questionTimeout = 5000;
        protected int answerMinimumTime = 1000;
        const int timerInterval = 100;

        protected Word currentWord { get; private set; }
        bool nextWordReserved = false;

        public readonly LearningMethod Method;
        DateTime testStartTime;        
        int sessionTimeInSecond = 0;

        public TestView(MainActivity activity, LearningMethod method)
        {
            this.activity = activity;
            this.state = activity.LearningState;
            this.Method = method;
            this.testStartTime = DateTime.Now;
            activity.SetContentView(Resource.Layout.Test);
            timeBar = activity.FindViewById<ProgressBar>(Resource.Id.timeBar);
            textKanji = activity.FindViewById<TextView>(Resource.Id.kanji);
            textKana = activity.FindViewById<TextView>(Resource.Id.kana);
            textMeaning = activity.FindViewById<TextView>(Resource.Id.meaning);
            checkMemorized = activity.FindViewById<CheckBox>(Resource.Id.memroized);
            checkForgot = activity.FindViewById<CheckBox>(Resource.Id.forgot);
            checkDifficult = activity.FindViewById<CheckBox>(Resource.Id.difficult);
            checkGood = activity.FindViewById<CheckBox>(Resource.Id.good);
            checkEasy = activity.FindViewById<CheckBox>(Resource.Id.easy);
            buttonNext = activity.FindViewById<Button>(Resource.Id.next);
            buttonNext.Click += (object sender, EventArgs e) => { 
                if (timeBar.Progress > 0)
                    nextWordReserved = true; 
                else
                    NextWord();
            };

            checks = new CheckBox[]
            {
                checkMemorized,
                checkForgot,
                checkDifficult,
                checkGood,
                checkEasy,
            };
            foreach (var c in checks)
            {
                c.Click += CheckClicked;
            }

            timer = new Timer(OnUpdate, null, timerInterval, timerInterval);
        }

        protected void StartTest(Word word)
        {
            var dSecond = (int)(DateTime.Now - testStartTime).TotalSeconds;
            dSecond = Math.Min(Math.Max(dSecond, 1), 30);
            sessionTimeInSecond += dSecond;
            testStartTime = DateTime.Now;

            textKanji.Text = word.FirstNotation;
            textKana.Text = word.SecondNotation;
            textMeaning.Text = word.Meaning;
            textMeaning.Visibility = ViewStates.Invisible;
            textKana.Visibility = ViewStates.Invisible;

            foreach (var c in checks)
                c.Checked = false;

            isProblemState = true;
            buttonNext.Enabled = false;
            timeBar.Max = questionTimeout;
            timeBar.Progress = questionTimeout;

            currentWord = word;
        }
     
        void CheckClicked(object sender, EventArgs args)
        {
            foreach (var c in checks)
                c.Checked = false;
            (sender as CheckBox).Checked = true;

            if (isProblemState)
                ShowAnswer();

            nextWordReserved = false;
        }

        public abstract void OnStop();

        void OnUpdate(Object state)
        {
            activity.RunOnUiThread(() =>
                {
                    if (isProblemState)
                    {
                        if (timeBar.Progress > timerInterval)
                        {
                            timeBar.Progress -= timerInterval;
                        }
                        else
                        {
                            timeBar.Progress = 0;
                            checkForgot.CallOnClick();
                        }
                    }
                    else
                    {
                        if (timeBar.Progress > timerInterval)
                        {
                            timeBar.Progress -= timerInterval;
                        }
                        else
                        {
                            timeBar.Progress = 0;
                            if (nextWordReserved)
                            {
                                buttonNext.CallOnClick();
                                nextWordReserved = false;
                            }
                        }
                    }
                });
        }

        protected abstract void NextWord();

        void ShowAnswer()
        {
            isProblemState = false;
            buttonNext.Enabled = true;
            timeBar.Max = answerMinimumTime;
            timeBar.Progress = answerMinimumTime;

            textMeaning.Visibility = ViewStates.Visible;
            textKana.Visibility = ViewStates.Visible;
        }

        protected bool IsTimeToNextTest
        {
            get
            {
                double maxTestMinutes = 3.0;
                return sessionTimeInSecond >= maxTestMinutes * 60;
            }
        }

        protected void RecordTime()
        {
            state.RecordTime(Method, sessionTimeInSecond);
        }

        protected void LogWord(WordState w)
        {
            Console.WriteLine(w.Word.FirstNotation);
            Console.WriteLine("checkCount = " + w.CheckCount.ToString());
            Console.WriteLine("level = " + w.Level + "  weight = " + w.Weight);
            Console.WriteLine("recent check = " + w.RecentlyChecked + "  past = " + w.PastChecked);
            Console.WriteLine("recent date = " + w.RecentDate);
            Console.WriteLine("reserve date = " + w.ReservedDate);
        }
    }
}

