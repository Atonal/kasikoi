﻿using System;
using Android.Widget;
using Android.Views;

namespace CS408.JTest
{
    public class FirstTestView : TestView
    {
        WordBook wordBook;

        public FirstTestView(MainActivity activity)
            : base(activity, LearningMethod.Simple)
        {
            checkDifficult.Visibility = ViewStates.Gone;
            checkGood.Visibility = ViewStates.Gone;
            checkEasy.Visibility = ViewStates.Gone;
            wordBook = activity.WordBook;
            answerMinimumTime = 100;

            if (wordBook.Count > 0)
            {
                StartTest(wordBook.GetWord());
            }
            else
            {
                activity.LearningState.EndFirstTest();
                activity.NextTestType();
            }

            var ankiState = activity.GetMethodState(LearningMethod.Anki);
            var count = ankiState.WordList.Count;
            var blockSize = TestConfiguration.LevelTestSetCount;
            var countBlock = (int)(count / blockSize);
            var countFrom = (countBlock * blockSize);
            var ratio = (count - countFrom) / (double)blockSize * 100.0;
            activity.ShowAlertPopup("레벨 테스트를 시작합니다.  (" + ratio.ToString("F1") + "% 진행중)");
        }

        protected override void NextWord()
        {
            var memorized = !checkForgot.Checked;
            var end = false;
            if (!memorized)
            {
                var method = state.MethodForTheNextNewWord(memorized);
                var methodState = activity.GetMethodState(method);
                methodState.AddWord(currentWord);

                if (method == LearningMethod.Anki)
                {
                    var count = methodState.WordList.Count;
                    if (count % TestConfiguration.LevelTestSetCount == 0)
                        end = true;
                }
            }

            Word word;
            if ((word = wordBook.NextWord()) != null 
                && !end)
            {
                StartTest(word);
            }
            else
            {
                activity.LearningState.EndFirstTest();
                activity.ShowAlertPopup("레벨 테스트가 끝났습니다.");
                activity.NextTestType();
            }
        }

        public override void OnStop()
        {
            
        }
    }
}

