﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;

namespace CS408.JTest
{
    public class MethodState
    {
        private static string SavePath(LearningMethod method)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "state" + method.ToString() + ".txt");
            return filename;
        }

        public static void ResetAll()
        {
            foreach (var m in LearningMethods.AllMethods)
            {
                File.Delete(SavePath(m));
            }
        }

        private MethodState(LearningMethod method)
        {
            this.Method = method;
            this.stateList = new List<WordState>();
        }

        public static MethodState Load(LearningMethod method)
        {
            var path = SavePath(method);
            if (File.Exists(path))
            {
                return new MethodState(method, File.ReadAllLines(path));
            }
            else
            {
                return new MethodState(method);
            }
        }

        private MethodState(LearningMethod method, string[] lines)
            : this(method)
        {
            int l = 0;
            while (l < lines.Length)
            {
                try
                {
                    stateList.Add(new WordState(lines, ref l));
                }
                catch
                {
                    break;
                }
            }
            Console.WriteLine("loaded {0} method: {1} words", Method.ToString(), stateList.Count);
        }

        public void Save()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in stateList)
                s.AppendTo(sb);

            File.WriteAllText(SavePath(Method), sb.ToString());

            Console.WriteLine("saved {0} method: {1} words", Method.ToString(), stateList.Count);
        }

        public void AddWord(Word word)
        {
            stateList.Add(new WordState(word));
        }

        public void RecordSnapShot(StateSnapShot snapShot)
        {
            snapShot.EncounterCount = 0;
            snapShot.MemorizedCount = 0;
            snapShot.TotalCount = stateList.Count;
            for (int mark = 0; mark < ForgettingCurveStatistics.MaxMemorizedMark; mark++)
                for (int day = 0; day < ForgettingCurveStatistics.MaxDay; day++)
                    snapShot.CurveStateCount[mark, day] = 0;

            foreach (var w in stateList)
            {
                if (w.RecentDate != DateTime.MinValue)
                    snapShot.EncounterCount++;
                if (w.RecentlyChecked)
                {
                    snapShot.MemorizedCount++;
                    int mark = Math.Min(Math.Max(w.CheckCount, 0), ForgettingCurveStatistics.MaxMemorizedMark - 1);
                    var diff = DateTime.Today - w.RecentDate;
                    int day = (int)diff.TotalDays;
                    day = Math.Min(Math.Max(day, 0), ForgettingCurveStatistics.MaxDay - 1);
                    snapShot.CurveStateCount[mark, day]++;
                }
            }
        }

        public readonly LearningMethod Method;
        public IReadOnlyList<WordState> WordList
        {
            get
            {
                return stateList;
            }
        }

        List<WordState> stateList;
    }
}

