﻿using System;
using System.Text;
using System.Diagnostics;

namespace CS408.JTest
{
    public class ForgettingCurveStatistics
    {
        public const int MaxDay = 30;
        public const int MaxMemorizedMark = 7;
        int[,] memorizedMarkCount;
        int[,] forgetMarkCount;

        public ForgettingCurveStatistics()
        {
            memorizedMarkCount = Allocate();
            forgetMarkCount = Allocate();
        }

        public ForgettingCurveStatistics(string[] lines, ref int l)
            : this()
        {
            l++;
            l++;
            Deserialize(memorizedMarkCount, lines, ref l);
            l++;
            Deserialize(forgetMarkCount, lines, ref l);
        }

        public void Record(int mark, int day, bool memorized)
        {
            mark = Math.Min(Math.Max(mark, 0), MaxMemorizedMark - 1);
            day = Math.Min(Math.Max(day, 0), MaxDay - 1);
            if (memorized)
                memorizedMarkCount[mark, day]++;
            else
                forgetMarkCount[mark, day]++;
        }

        public void WriteTo(StringBuilder sb)
        {
            sb.AppendLine("forgetting curve. row: mark / column: day");
            sb.AppendLine("memorizedMarkCount");
            Serialize(memorizedMarkCount, sb);
            sb.AppendLine("forgetMarkCount");
            Serialize(forgetMarkCount, sb);
        }

        public static int[,] Allocate()
        {
            return new int[MaxMemorizedMark, MaxDay];
        }

        public static void Serialize(int[,] array, StringBuilder sb)
        {
            for (int mark = 0; mark < MaxMemorizedMark; mark++)
            {
                for (int day = 0; day < MaxDay; day++)
                {
                    sb.Append(array[mark, day]);
                    sb.Append(' ');
                }
                sb.AppendLine();
            }
        }

        public static void Deserialize(int[,] array, string[] lines, ref int l)
        {
            for (int mark = 0; mark < MaxMemorizedMark; mark++)
            {
                var token = lines[l++].Split(' ');
                for (int day = 0; day < MaxDay; day++)
                {
                    array[mark, day] = int.Parse(token[day]);
                }
            }
        }
    }
}

