﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CS408.JTest
{
	public class WordBook
	{
		private static string SavePath
		{
			get
			{
				string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				string filename = Path.Combine(path, "wordpool.txt");
				return filename;
			}
		}

		private WordBook(string[] lines)
		{
			// parse lines into word list
			var count = (lines.Length / 3);
			words = new List<Word>();
			for (int i = 0; i < count; i++)
			{
				var l = i * 3;
				words.Add(new Word(lines, ref l));
			}

			// list shuffle
			Random rand = new Random();
			for (int i = 0; i < count; i++)
			{
				var from = i;
				var to = rand.Next(words.Count);
				var temp = words[from];
				words[from] = words[to];
				words[to] = temp;
			}

            Console.WriteLine("wordbook loaded {0} words", words.Count);
		}

		public static WordBook FromSaveData()
		{
			var lines = File.ReadAllLines(SavePath);
			return new WordBook(lines);
		}

		public void Save()
		{
			var sb = new StringBuilder();
			foreach (var w in words)
				w.AppendTo(sb);
			File.WriteAllText(SavePath, sb.ToString());

            Console.WriteLine("wordbook saved {0} words", words.Count);
		}

		public static WordBook FromDefault(string defaultString)
		{
			var lines = defaultString.Split('\n');
			return new WordBook(lines);
		}

		public Word GetWord()
		{
			if (words.Count > 0)
			{
				var last = words.Count - 1;
				return words[last];
			}
			return null;
		}

		public Word NextWord()
		{
			if (words.Count > 0)
			{
				var last = words.Count - 1;
				words.RemoveAt(last);
			}
			return GetWord();
		}

		public int Count
		{
			get
			{
				return words.Count;
			}
		}

		private List<Word> words;
	}
}

