﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace CS408.JTest
{
    public class LearningState
    {
        private static string SavePath
        {
            get
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                string filename = Path.Combine(path, "state.txt");
                return filename;
            }
        }

        public static LearningState Load()
        {
            if (File.Exists(SavePath))
            {
                string[] lines = File.ReadAllLines(SavePath);
                return new LearningState(lines);
            }
            return new LearningState();
        }

        private LearningState()
        {
            Stage = LearningStage.NewComer;
            UserName = "";
            utilizeTimeInSeconds = new Dictionary<LearningMethod, int>();
            foreach (var m in LearningMethods.AllMethods)
                utilizeTimeInSeconds.Add(m, 0);

            forgettingCurve = new ForgettingCurveStatistics();
            curves = new Dictionary<LearningMethod, ForgettingCurveStatistics>();
            foreach (var m in LearningMethods.AllMethods)
                curves.Add(m, new ForgettingCurveStatistics());

            initialState = new Dictionary<LearningMethod, InitialLearningMethodState>();
            foreach (var m in LearningMethods.AllMethods)
                initialState.Add(m, new InitialLearningMethodState());

            snapShotList = new List<StateSnapShot>();
            currentSnapShot = new Dictionary<LearningMethod, StateSnapShot>();
            foreach (var m in LearningMethods.AllMethods)
            {
                var snapShot = new StateSnapShot(m);
                snapShotList.Add(snapShot);
                currentSnapShot.Add(m, snapShot);
            }
        }

        private LearningState(string[] lines)
        {
            Console.WriteLine("LearningState.ctor(lines) called");

            int l = 0;
            UserName = lines[l++];
            Stage = (LearningStage)Enum.Parse(typeof(LearningStage), lines[l++]);

            l++;
            utilizeTimeInSeconds = new Dictionary<LearningMethod, int>();
            for (int i = 0; i < LearningMethods.Count; i++)
            {
                var method = (LearningMethod)Enum.Parse(typeof(LearningMethod), lines[l++]);
                utilizeTimeInSeconds.Add(method, int.Parse(lines[l++]));
            }

            forgettingCurve = new ForgettingCurveStatistics(lines, ref l);
            curves = new Dictionary<LearningMethod, ForgettingCurveStatistics>();
            for (int i = 0; i < LearningMethods.Count; i++)
            {
                var method = (LearningMethod)Enum.Parse(typeof(LearningMethod), lines[l++]);
                var curve = new ForgettingCurveStatistics(lines, ref l);
                curves.Add(method, curve);
            }

            initialState = new Dictionary<LearningMethod, InitialLearningMethodState>();
            l++;
            for (int i = 0; i < LearningMethods.Count; i++)
            {
                var state = new InitialLearningMethodState();
                var method = (LearningMethod)Enum.Parse(typeof(LearningMethod), lines[l++]);
                state.KnownWordCount = int.Parse(lines[l++]);
                state.UnknowWordCount = int.Parse(lines[l++]);
                initialState.Add(method, state);
            }

            snapShotList = new List<StateSnapShot>();
            currentSnapShot = new Dictionary<LearningMethod, StateSnapShot>();
            while (l < lines.Length)
            {
                LearningMethod method;
                if (Enum.TryParse(lines[l], out method))
                {
                    var snapShot = new StateSnapShot(lines, ref l);
                    snapShotList.Add(snapShot);
                    if (snapShot.SnapShotDate == DateTime.Today)
                    {
                        if (!currentSnapShot.ContainsKey(snapShot.Method))
                        {
                            currentSnapShot.Add(snapShot.Method, snapShot);
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            foreach (var m in LearningMethods.AllMethods)
            {
                if (!currentSnapShot.ContainsKey(m))
                {
                    var snapShot = new StateSnapShot(m);
                    currentSnapShot.Add(m, snapShot);
                    snapShotList.Add(snapShot);
                }
            }

            Console.WriteLine("LearningState.ctor(lines) succeed");
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(UserName);
            sb.AppendLine(Stage.ToString());

            sb.AppendLine("Utilize Time Per Method");
            foreach (var m in LearningMethods.AllMethods)
            {
                sb.AppendLine(m.ToString());
                sb.AppendLine(utilizeTimeInSeconds[m].ToString());
            }

            forgettingCurve.WriteTo(sb);
            foreach (var m in LearningMethods.AllMethods)
            {
                sb.AppendLine(m.ToString());
                curves[m].WriteTo(sb);
            }

            sb.AppendLine("Initial State");
            foreach (var m in LearningMethods.AllMethods)
            {
                sb.AppendLine(m.ToString());
                sb.AppendLine(initialState[m].KnownWordCount.ToString());
                sb.AppendLine(initialState[m].UnknowWordCount.ToString());
            }

            foreach (var s in snapShotList)
                s.WriteTo(sb);

            return sb.ToString();
        }

        public void Save()
        {
            Console.WriteLine("LearningState.Save() called");
            File.WriteAllText(SavePath, ToString());
            Console.WriteLine("LearningState.Save() succeed");
        }

        public static void Reset()
        {
            File.Delete(SavePath);
        }

        public void StartLearning(string name)
        {
            Debug.Assert(Stage == LearningStage.NewComer);
            UserName = name;
            Stage = LearningStage.FirstTest;
        }

        public void AddMoreWord()
        {
            Debug.Assert(Stage == LearningStage.Learning);
            Stage = LearningStage.FirstTest;
        }

        public void EndFirstTest()
        {
            Debug.Assert(Stage == LearningStage.FirstTest);
            Stage = LearningStage.Learning;
        }

        public LearningStage Stage { get; private set; }
        public string UserName { get; private set; }

        Dictionary<LearningMethod, int> utilizeTimeInSeconds;
        public LearningMethod MethodWithMinimumTime
        {
            get
            {
                int minTime = int.MaxValue;
                LearningMethod minTimeMethod = LearningMethod.Anki;
                foreach (var m in LearningMethods.AllMethods)
                {
                    var t = utilizeTimeInSeconds[m];
                    if (t < minTime)
                    {
                        minTime = t;
                        minTimeMethod = m;
                    }
                }
                return minTimeMethod;
            }
        }

        public void RecordTime(LearningMethod method, int timeInSecond)
        {
            utilizeTimeInSeconds[method] = utilizeTimeInSeconds[method] + timeInSecond;
        }

        public int GetUtilizeTime(LearningMethod method)
        {
            return utilizeTimeInSeconds[method];
        }

        ForgettingCurveStatistics forgettingCurve;
        Dictionary<LearningMethod, ForgettingCurveStatistics> curves;

        public void RecordCurve(LearningMethod method, WordState word, bool memorized)
        {
            var markCount = word.CheckCount;
            var diff = DateTime.Today - word.RecentDate;
            var days = (int)diff.TotalDays;
            if (word.RecentDate == DateTime.MinValue)
                days = 0;
            
            forgettingCurve.Record(markCount, days, memorized);
            curves[method].Record(markCount, days, memorized);
        }

        public class InitialLearningMethodState
        {
            public int KnownWordCount;
            public int UnknowWordCount;
        }
        Dictionary<LearningMethod, InitialLearningMethodState> initialState;

        Random rand = new Random();
        public LearningMethod MethodForTheNextNewWord(bool isKnownWord)
        {
            LearningMethod methodWithMinCount = LearningMethods.RandomInAll;
            int minCount = isKnownWord ? initialState[methodWithMinCount].KnownWordCount : initialState[methodWithMinCount].UnknowWordCount;

            foreach (var m in LearningMethods.AllMethods)
            {
                int c = isKnownWord ? initialState[m].KnownWordCount : initialState[m].UnknowWordCount;
                if (c < minCount)
                {
                    minCount = c;
                    methodWithMinCount = m;
                }
            }

            if (isKnownWord)
            {
                minCount++;
                initialState[methodWithMinCount].KnownWordCount = minCount;
            }
            else
            {
                minCount++;
                initialState[methodWithMinCount].UnknowWordCount = minCount;
            }

            Console.WriteLine("method\tknown\tunknown");
            foreach (var m in LearningMethods.AllMethods)
                Console.WriteLine("{0}\t{1}\t{2}", m, initialState[m].KnownWordCount, initialState[m].UnknowWordCount);

            return methodWithMinCount;
        }

        List<StateSnapShot> snapShotList;
        Dictionary<LearningMethod, StateSnapShot> currentSnapShot;

        public StateSnapShot GetStateSnapShot(LearningMethod method)
        {
            return currentSnapShot[method];
        }
    }

    public enum LearningStage
    {
        NewComer,
        FirstTest,
        Learning
    }
}


