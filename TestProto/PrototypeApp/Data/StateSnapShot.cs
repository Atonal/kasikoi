﻿using System;
using System.Text;

namespace CS408.JTest
{
    public class StateSnapShot
    {
        public StateSnapShot(LearningMethod method)
        {
            this.Method = method;
            this.SnapShotDate = DateTime.Today;
            this.TotalCount = 0;
            this.EncounterCount = 0;
            this.MemorizedCount = 0;
            this.CurveStateCount = ForgettingCurveStatistics.Allocate();
            this.UtilizeTimeInSecond = 0;
        }

        public StateSnapShot(string[] lines, ref int l)
        {
            Method = (LearningMethod)Enum.Parse(typeof(LearningMethod), lines[l++]);
            SnapShotDate = DateTime.Parse(lines[l++]);
            TotalCount = int.Parse(lines[l++]);
            EncounterCount = int.Parse(lines[l++]);
            MemorizedCount = int.Parse(lines[l++]);
            CurveStateCount = ForgettingCurveStatistics.Allocate();
            ForgettingCurveStatistics.Deserialize(CurveStateCount, lines, ref l);
            UtilizeTimeInSecond = int.Parse(lines[l++]);
        }

        public void WriteTo(StringBuilder sb)
        {
            sb.AppendLine(Method.ToString());
            sb.AppendLine(SnapShotDate.ToShortDateString());
            sb.AppendLine(TotalCount.ToString());
            sb.AppendLine(EncounterCount.ToString());
            sb.AppendLine(MemorizedCount.ToString());
            ForgettingCurveStatistics.Serialize(CurveStateCount, sb);
            sb.AppendLine(UtilizeTimeInSecond.ToString());
        }

        public LearningMethod Method;
        public DateTime SnapShotDate;
        public int TotalCount;
        public int EncounterCount;
        public int MemorizedCount;
        public int[,] CurveStateCount;
        public int UtilizeTimeInSecond;
    }
}

