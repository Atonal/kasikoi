﻿using System;
using System.Text;

namespace CS408.JTest
{
    public class WordState
    {
        public WordState(Word word)
        {
            this.Word = word;
            this.CheckCount = 0;
            this.RecentDate = DateTime.MinValue;
            this.ReservedDate = DateTime.MinValue;
            this.CheckCount = 0;
            this.RecentlyChecked = false;
            this.PastChecked = false;
            this.Level = 1;
        }

        public WordState(string[] lines, ref int l)
        {
            this.Word = new Word(lines, ref l);
            this.RecentDate = DateTime.Parse(lines[l++]);
            this.ReservedDate = DateTime.Parse(lines[l++]);
            this.CheckCount = int.Parse(lines[l++]);
            this.RecentlyChecked = bool.Parse(lines[l++]);
            this.PastChecked = bool.Parse(lines[l++]);
            this.Level = int.Parse(lines[l++]);
        }

        public void AppendTo(StringBuilder sb)
        {
            Word.AppendTo(sb);
            sb.AppendLine(RecentDate.ToShortDateString());
            sb.AppendLine(ReservedDate.ToShortDateString());
            sb.AppendLine(CheckCount.ToString());
            sb.AppendLine(RecentlyChecked.ToString());
            sb.AppendLine(PastChecked.ToString());
            sb.AppendLine(Level.ToString());
        }

        public void MarkCorrect()
        {
            RecentDate = DateTime.Today;
            CheckCount++;
            PastChecked = RecentlyChecked;
            RecentlyChecked = true;
        }

        public void MarkWrong()
        {
            RecentDate = DateTime.Today;
            ReservedDate = DateTime.Today;
            PastChecked = RecentlyChecked;
            RecentlyChecked = false;
        }

        public void MarkEasy()
        {
            RecentDate = DateTime.Today;
            ReservedDate = DateTime.Today + TimeSpan.FromDays(7);
        }

        public void MarkGood()
        {
            RecentDate = DateTime.Today;
            ReservedDate = DateTime.Today + TimeSpan.FromDays(3);
        }

        public void MarkDifficult()
        {
            RecentDate = DateTime.Today;
            ReservedDate = DateTime.Today + TimeSpan.FromDays(1);
        }

        public void UpdateWeight()
        {
            Weight = 1;
            if (Level <= 1)
            {
                if (RecentlyChecked && !PastChecked)
                    Weight = 100;
                else
                    Weight = 50;
                return;
            }

            if (DateTime.Today < ReservedDate)
                return;

            Weight = 50;
            int dayAfter = (int)(DateTime.Today - ReservedDate).TotalDays;
            if (RecentlyChecked && !PastChecked)
                Weight = 100;
            Weight += 5 * dayAfter * dayAfter;
        }

        public void UpdateState()
        {
            if (RecentlyChecked && PastChecked)
                Level = Math.Min(Level + 1, MaxLevel);

            if (!RecentlyChecked && !PastChecked)
                Level = Math.Max(Level - 1, MinLevel);

            if (RecentlyChecked)
            {
                RecentDate = DateTime.Today;
                if (!PastChecked)
                {
                    ReservedDate = RecentDate + TimeSpan.FromDays(
                        Math.Max(Cooldown(Level) / 2, 1)
                    );
                }
                else
                {
                    ReservedDate = RecentDate + TimeSpan.FromDays(
                        Cooldown(Level)
                    );
                }
            }
        }

        public static int Cooldown(int level)
        {
            level = Math.Min(Math.Max(level, MinLevel), MaxLevel);
            switch (level)
            {
                case 1:
                    return 0;
                case 2:
                    return 1;
                case 3:
                    return 3;
                case 4:
                    return 3;
                case 5:
                    return 7;
                case 6:
                    return 7;
                case 7:
                    return 14;
                case 8:
                    return 30;
                case 9:
                    return 60;
                case 10:
                    return 120;
            };
            return 0;
        }

        const int MinLevel = 1;
        const int MaxLevel = 10;

        public readonly Word Word;
        public DateTime RecentDate { get; private set; }
        public DateTime ReservedDate { get; private set; }
        public int CheckCount { get; private set; }

        // 최근 테스트 결과가 pass이면 true
        public bool RecentlyChecked { get; private set; }
        // 최근 테스트 직전의 결과가 pass이면 true
        public bool PastChecked { get; private set; }

        public int Level { get; private set; }
        public int Weight { get; private set; }
    }
}

