﻿using System;
using System.Text;

namespace CS408.JTest
{
    public class Word
    {
        public Word(string[] array, ref int line)
        {
            FirstNotation = array[line++];
            SecondNotation = array[line++];
            if (string.IsNullOrWhiteSpace(FirstNotation))
            {
                FirstNotation = SecondNotation;
                SecondNotation = "";
            }
            Meaning = array[line++];
        }

        public void AppendTo(StringBuilder sb)
        {
            sb.AppendLine(FirstNotation);
            sb.AppendLine(SecondNotation);
            sb.AppendLine(Meaning);
        }

        public bool HasSecondNotation
        {
            get
            {
                return string.IsNullOrWhiteSpace(SecondNotation);
            }
        }

        public readonly string FirstNotation;
        public readonly string SecondNotation;
        public readonly string Meaning;
    }
}

