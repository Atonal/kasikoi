﻿using System;
using System.Collections.Generic;

namespace CS408.JTest
{
    public enum LearningMethod
    {
        Simple,
        Anki,
        FCurve
    }

    public static class LearningMethods
    {
        static LearningMethods()
        {
            allMethods = new LearningMethod[]
            {
                LearningMethod.Simple,
                LearningMethod.Anki,
                LearningMethod.FCurve
            };
        }

        static LearningMethod[] allMethods;
        public static IReadOnlyList<LearningMethod> AllMethods
        {
            get
            {
                return allMethods;
            }
        }

        public static int Count
        {
            get
            {
                return allMethods.Length;
            }
        }

        static Random rand = new Random();
        public static LearningMethod RandomInAll
        {
            get
            {
                return allMethods[rand.Next(allMethods.Length)];
            }
        }
    }
}

