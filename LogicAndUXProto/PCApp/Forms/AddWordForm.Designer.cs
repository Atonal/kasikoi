﻿namespace Kasikoi.Forms
{
    partial class AddWordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanjiNotation = new System.Windows.Forms.Label();
            this.lblKanaNotation = new System.Windows.Forms.Label();
            this.lblMeaning = new System.Windows.Forms.Label();
            this.txtKanji = new System.Windows.Forms.TextBox();
            this.txtKana = new System.Windows.Forms.TextBox();
            this.txtMeaning = new System.Windows.Forms.TextBox();
            this.lblKanjiWarning = new System.Windows.Forms.Label();
            this.lblKanaWarning = new System.Windows.Forms.Label();
            this.lblRelated = new System.Windows.Forms.Label();
            this.txtRelated = new System.Windows.Forms.TextBox();
            this.btnKanjiAction = new System.Windows.Forms.Button();
            this.btnKanaAction = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnRelatedAdd = new System.Windows.Forms.Button();
            this.lblRelatedList = new System.Windows.Forms.Label();
            this.lblRelatedWarning = new System.Windows.Forms.Label();
            this.btnRelatedAction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblKanjiNotation
            // 
            this.lblKanjiNotation.AutoSize = true;
            this.lblKanjiNotation.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblKanjiNotation.Location = new System.Drawing.Point(12, 9);
            this.lblKanjiNotation.Name = "lblKanjiNotation";
            this.lblKanjiNotation.Size = new System.Drawing.Size(80, 18);
            this.lblKanjiNotation.TabIndex = 1;
            this.lblKanjiNotation.Text = "한자 표기:";
            // 
            // lblKanaNotation
            // 
            this.lblKanaNotation.AutoSize = true;
            this.lblKanaNotation.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblKanaNotation.Location = new System.Drawing.Point(12, 65);
            this.lblKanaNotation.Name = "lblKanaNotation";
            this.lblKanaNotation.Size = new System.Drawing.Size(85, 18);
            this.lblKanaNotation.TabIndex = 2;
            this.lblKanaNotation.Text = "가나 표기: ";
            // 
            // lblMeaning
            // 
            this.lblMeaning.AutoSize = true;
            this.lblMeaning.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMeaning.Location = new System.Drawing.Point(12, 121);
            this.lblMeaning.Name = "lblMeaning";
            this.lblMeaning.Size = new System.Drawing.Size(35, 18);
            this.lblMeaning.TabIndex = 3;
            this.lblMeaning.Text = "뜻: ";
            // 
            // txtKanji
            // 
            this.txtKanji.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtKanji.Location = new System.Drawing.Point(98, 6);
            this.txtKanji.Name = "txtKanji";
            this.txtKanji.Size = new System.Drawing.Size(885, 26);
            this.txtKanji.TabIndex = 0;
            this.txtKanji.Text = "テスト";
            // 
            // txtKana
            // 
            this.txtKana.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtKana.Location = new System.Drawing.Point(98, 62);
            this.txtKana.Name = "txtKana";
            this.txtKana.Size = new System.Drawing.Size(885, 26);
            this.txtKana.TabIndex = 1;
            this.txtKana.Text = "テスト";
            // 
            // txtMeaning
            // 
            this.txtMeaning.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMeaning.Location = new System.Drawing.Point(98, 118);
            this.txtMeaning.Name = "txtMeaning";
            this.txtMeaning.Size = new System.Drawing.Size(885, 26);
            this.txtMeaning.TabIndex = 2;
            this.txtMeaning.Text = "テスト";
            // 
            // lblKanjiWarning
            // 
            this.lblKanjiWarning.AutoSize = true;
            this.lblKanjiWarning.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblKanjiWarning.ForeColor = System.Drawing.Color.Red;
            this.lblKanjiWarning.Location = new System.Drawing.Point(95, 35);
            this.lblKanjiWarning.Name = "lblKanjiWarning";
            this.lblKanjiWarning.Size = new System.Drawing.Size(38, 18);
            this.lblKanjiWarning.TabIndex = 7;
            this.lblKanjiWarning.Text = "경고";
            // 
            // lblKanaWarning
            // 
            this.lblKanaWarning.AutoSize = true;
            this.lblKanaWarning.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblKanaWarning.ForeColor = System.Drawing.Color.Red;
            this.lblKanaWarning.Location = new System.Drawing.Point(95, 91);
            this.lblKanaWarning.Name = "lblKanaWarning";
            this.lblKanaWarning.Size = new System.Drawing.Size(38, 18);
            this.lblKanaWarning.TabIndex = 8;
            this.lblKanaWarning.Text = "경고";
            // 
            // lblRelated
            // 
            this.lblRelated.AutoSize = true;
            this.lblRelated.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelated.Location = new System.Drawing.Point(12, 153);
            this.lblRelated.Name = "lblRelated";
            this.lblRelated.Size = new System.Drawing.Size(80, 18);
            this.lblRelated.TabIndex = 9;
            this.lblRelated.Text = "연관 단어:";
            // 
            // txtRelated
            // 
            this.txtRelated.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRelated.Location = new System.Drawing.Point(98, 150);
            this.txtRelated.Name = "txtRelated";
            this.txtRelated.Size = new System.Drawing.Size(130, 26);
            this.txtRelated.TabIndex = 3;
            this.txtRelated.Text = "テスト";
            // 
            // btnKanjiAction
            // 
            this.btnKanjiAction.Location = new System.Drawing.Point(923, 35);
            this.btnKanjiAction.Name = "btnKanjiAction";
            this.btnKanjiAction.Size = new System.Drawing.Size(60, 21);
            this.btnKanjiAction.TabIndex = 11;
            this.btnKanjiAction.Text = "예(&H)";
            this.btnKanjiAction.UseVisualStyleBackColor = true;
            this.btnKanjiAction.Click += new System.EventHandler(this.btnKanjiAction_Click);
            // 
            // btnKanaAction
            // 
            this.btnKanaAction.Location = new System.Drawing.Point(923, 92);
            this.btnKanaAction.Name = "btnKanaAction";
            this.btnKanaAction.Size = new System.Drawing.Size(60, 21);
            this.btnKanaAction.TabIndex = 12;
            this.btnKanaAction.Text = "예(&K)";
            this.btnKanaAction.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCancel.Location = new System.Drawing.Point(880, 211);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(103, 37);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "취소(&Q)";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAccept.Location = new System.Drawing.Point(771, 211);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(103, 37);
            this.btnAccept.TabIndex = 14;
            this.btnAccept.Text = "확인(&N)";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnRelatedAdd
            // 
            this.btnRelatedAdd.Location = new System.Drawing.Point(234, 150);
            this.btnRelatedAdd.Name = "btnRelatedAdd";
            this.btnRelatedAdd.Size = new System.Drawing.Size(60, 26);
            this.btnRelatedAdd.TabIndex = 15;
            this.btnRelatedAdd.Text = "추가(&R)";
            this.btnRelatedAdd.UseVisualStyleBackColor = true;
            this.btnRelatedAdd.Click += new System.EventHandler(this.btnRelatedAdd_Click);
            // 
            // lblRelatedList
            // 
            this.lblRelatedList.AutoSize = true;
            this.lblRelatedList.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelatedList.Location = new System.Drawing.Point(300, 153);
            this.lblRelatedList.Name = "lblRelatedList";
            this.lblRelatedList.Size = new System.Drawing.Size(102, 18);
            this.lblRelatedList.TabIndex = 16;
            this.lblRelatedList.Text = "lblRelatedList";
            // 
            // lblRelatedWarning
            // 
            this.lblRelatedWarning.AutoSize = true;
            this.lblRelatedWarning.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelatedWarning.ForeColor = System.Drawing.Color.Red;
            this.lblRelatedWarning.Location = new System.Drawing.Point(95, 183);
            this.lblRelatedWarning.Name = "lblRelatedWarning";
            this.lblRelatedWarning.Size = new System.Drawing.Size(38, 18);
            this.lblRelatedWarning.TabIndex = 17;
            this.lblRelatedWarning.Text = "경고";
            // 
            // btnRelatedAction
            // 
            this.btnRelatedAction.Location = new System.Drawing.Point(923, 184);
            this.btnRelatedAction.Name = "btnRelatedAction";
            this.btnRelatedAction.Size = new System.Drawing.Size(60, 21);
            this.btnRelatedAction.TabIndex = 18;
            this.btnRelatedAction.Text = "예(&T)";
            this.btnRelatedAction.UseVisualStyleBackColor = true;
            // 
            // AddWordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 259);
            this.Controls.Add(this.btnRelatedAction);
            this.Controls.Add(this.lblRelatedWarning);
            this.Controls.Add(this.lblRelatedList);
            this.Controls.Add(this.btnRelatedAdd);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnKanaAction);
            this.Controls.Add(this.btnKanjiAction);
            this.Controls.Add(this.txtRelated);
            this.Controls.Add(this.lblRelated);
            this.Controls.Add(this.lblKanaWarning);
            this.Controls.Add(this.lblKanjiWarning);
            this.Controls.Add(this.txtMeaning);
            this.Controls.Add(this.txtKana);
            this.Controls.Add(this.txtKanji);
            this.Controls.Add(this.lblMeaning);
            this.Controls.Add(this.lblKanaNotation);
            this.Controls.Add(this.lblKanjiNotation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddWordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "단어 추가";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanjiNotation;
        private System.Windows.Forms.Label lblKanaNotation;
        private System.Windows.Forms.Label lblMeaning;
        private System.Windows.Forms.TextBox txtKanji;
        private System.Windows.Forms.TextBox txtKana;
        private System.Windows.Forms.TextBox txtMeaning;
        private System.Windows.Forms.Label lblKanjiWarning;
        private System.Windows.Forms.Label lblKanaWarning;
        private System.Windows.Forms.Label lblRelated;
        private System.Windows.Forms.TextBox txtRelated;
        private System.Windows.Forms.Button btnKanjiAction;
        private System.Windows.Forms.Button btnKanaAction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnRelatedAdd;
        private System.Windows.Forms.Label lblRelatedList;
        private System.Windows.Forms.Label lblRelatedWarning;
        private System.Windows.Forms.Button btnRelatedAction;
    }
}