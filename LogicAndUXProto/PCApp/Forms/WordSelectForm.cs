﻿using Kasikoi.Data;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Kasikoi.Forms
{
    public partial class WordSelectForm : Form
    {
        List<Word> wordList = new List<Word>();

        public WordSelectForm(IReadOnlyCollection<Word> words)
        {
            InitializeComponent();

            foreach (var word in words)
            {
                wordList.Add(word);
                listBoxWord.Items.Add(word.ToString(3));
            }
            listBoxWord.SelectedIndex = 0;
            listBoxWord.SelectedIndexChanged += listBoxWord_SelectedIndexChanged;

            KeyDown += WordSelectForm_KeyDown;
        }

        void listBoxWord_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedWord = wordList[listBoxWord.SelectedIndex];
        }

        void WordSelectForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.D0 <= e.KeyCode && e.KeyCode <= Keys.D9)
            {
                if (Keys.D0 == e.KeyCode)
                {
                    listBoxWord.SelectedIndex = Math.Min(9, listBoxWord.Items.Count - 1);
                }
                else
                {
                    var index = e.KeyCode - Keys.D1;
                    listBoxWord.SelectedIndex = Math.Min(index, listBoxWord.Items.Count - 1);
                }
            }
            if (e.KeyCode == Keys.Enter)
                btnAccept.PerformClick();
        }

        public Word SelectedWord { get; private set; }
        public event Action Accepted = delegate { };

        private void btnAccept_Click(object sender, EventArgs e)
        {
            Close();
            Accepted();
        }
    }
}
