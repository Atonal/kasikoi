﻿using Kasikoi.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Kasikoi.Forms
{
    public partial class AddWordForm : Form
    {
        public AddWordForm()
        {
            InitializeComponent();

            KeyUp += AddWordForm_KeyUp;
            txtKanji.LostFocus += txtKanji_LostFocus;
            NewWord();
        }

        public void NewWord()
        {
            lblKanjiWarning.Visible = false;
            lblKanaWarning.Visible = false;
            btnKanjiAction.Visible = false;
            btnKanaAction.Visible = false;
            lblRelatedWarning.Visible = false;
            btnRelatedAction.Visible = false;

            txtKanji.Text = "";
            txtKana.Text = "";
            txtMeaning.Text = "";
            txtRelated.Text = "";

            relatedWords.Clear();
            UpdateRelatedWordView();

            txtKanji.Focus();
        }

        public Word AddWord()
        {
            var kanji = StringHelper.SplitWithPunctuation(txtKanji.Text);
            var kana = StringHelper.SplitWithPunctuation(txtKana.Text);
            var meaning = StringHelper.SplitWithPunctuation(txtMeaning.Text);

            var data = LearningSystem.Instance;
            var word = new Word(0, kanji, kana, meaning);
            word = data.AddWord(word);

            foreach (var relatedWord in relatedWords)
                data.AddWordRelation(word, relatedWord);

            return word;
        }

        public event Action Confirmed = delegate { };
        public event Action Cancelled = delegate { };

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Cancelled();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKana.Text))
            {
                MessageBox.Show("가나 표기는 반드시 입력해야 합니다.");
                txtKana.Focus();
            }
            else if (string.IsNullOrWhiteSpace(txtKanji.Text)
                && string.IsNullOrWhiteSpace(txtMeaning.Text))
            {
                MessageBox.Show("한자 표기 또는 뜻 둘 중 하나는 반드시 입력해야 합니다.");
                txtKanji.Focus();
            }
            else
            {
                Confirmed();
            }
        }

        void AddWordForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Handled) return;

            if (e.Control)
            {
                if (e.KeyCode == Keys.N)
                {
                    e.Handled = true;
                    btnAccept.PerformClick();
                }
                else if (e.KeyCode == Keys.Q)
                {
                    e.Handled = true;
                    btnCancel.PerformClick();
                }
                else if (e.KeyCode == Keys.R)
                {
                    e.Handled = true;
                    btnRelatedAdd.PerformClick();
                }
                else if (e.KeyCode == Keys.T)
                {
                    e.Handled = true;
                    btnRelatedAction.PerformClick();
                }
                else if (e.KeyCode == Keys.K)
                {
                    e.Handled = true;
                    btnKanaAction.PerformClick();
                }
                else if (e.KeyCode == Keys.H)
                {
                    e.Handled = true;
                    btnKanjiAction.PerformClick();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    e.Handled = true;
                    btnAccept.PerformClick();
                }
            }
        }

        List<Word> sameKanjiNotationList = new List<Word>();
        SortedSet<char> unknownKanjiList = new SortedSet<char>();
        void UpdateKanjiWarning()
        {
            var kanjiNotations = StringHelper.SplitWithPunctuation(txtKanji.Text);
            sameKanjiNotationList.Clear();
            unknownKanjiList.Clear();
            if (kanjiNotations.Length > 0)
            {
                var data = LearningSystem.Instance;
                foreach (var kanjiNotation in kanjiNotations)
                {
                    var sameKanjiList = data.GetWordWithKanjiNotation(kanjiNotation);
                    sameKanjiNotationList.AddRange(sameKanjiList);

                    foreach (var character in kanjiNotation)
                    {
                        if (StringHelper.IsKanji(character))
                        {
                            if (!data.ContainsKanji(character))
                            {
                                unknownKanjiList.Add(character);
                            }
                        }
                    }
                }
            }

            if (unknownKanjiList.Count > 0)
            {
                lblKanjiWarning.Visible = true;
                lblKanjiWarning.Text = string.Format(
                    "등록되지 않은 한자 {0}가 있습니다. 등록하시겠습니까?",
                    unknownKanjiList.Min
                );
                btnKanjiAction.Visible = true;
            }
            else if (sameKanjiNotationList.Count > 0)
            {
                lblKanjiWarning.Visible = true;
                lblKanjiWarning.Text = "이미 같은 표기의 단어가 있습니다. ("
                    + sameKanjiNotationList[0].ToString();
                if (sameKanjiNotationList.Count > 1)
                    lblKanjiWarning.Text += string.Format(" 외 {0}개", sameKanjiNotationList.Count - 1);
                lblKanjiWarning.Text += ") 단어 목록을 확인 할까요?";
                btnKanjiAction.Visible = true;
            }
            else
            {
                lblKanjiWarning.Visible = false;
                btnKanjiAction.Visible = false;
            }
        }

        void txtKanji_LostFocus(object sender, EventArgs e)
        {
            UpdateKanjiWarning();
        }

        private void btnKanjiAction_Click(object sender, EventArgs e)
        {
            if (unknownKanjiList.Count > 0)
            {
                var kanji = unknownKanjiList.Min;
                var addKanjiForm = new AddKanjiForm(unknownKanjiList.Min);
                addKanjiForm.FormClosed += delegate
                {
                    unknownKanjiList.Remove(kanji);
                    UpdateKanjiWarning();
                };
                addKanjiForm.ShowDialog();
            }
            else if (sameKanjiNotationList.Count > 0)
            {
                var selectForm = new WordSelectForm(sameKanjiNotationList);
                selectForm.ShowDialog();
            }
        }

        List<Word> relatedWords = new List<Word>();
        void UpdateRelatedWordView()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var r in relatedWords)
            {
                r.ToStringBuilder(sb);
                sb.Append("  ");
            }
            lblRelatedList.Text = sb.ToString();
        }

        private void btnRelatedAdd_Click(object sender, EventArgs e)
        {
            MessageBox.Show("연관 단어 추가 미구현 기능입니다.");
        }
    }
}
