﻿namespace Kasikoi.Forms
{
    partial class LearningForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMainNotation = new System.Windows.Forms.Label();
            this.lblSubNotation = new System.Windows.Forms.Label();
            this.lblMeaning = new System.Windows.Forms.Label();
            this.lblRelatedWords = new System.Windows.Forms.Label();
            this.lblWordInfo = new System.Windows.Forms.Label();
            this.chkMemorized = new System.Windows.Forms.CheckBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.chkForgot = new System.Windows.Forms.CheckBox();
            this.barTime = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // lblMainNotation
            // 
            this.lblMainNotation.AutoSize = true;
            this.lblMainNotation.Font = new System.Drawing.Font("바탕", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMainNotation.Location = new System.Drawing.Point(8, 9);
            this.lblMainNotation.Name = "lblMainNotation";
            this.lblMainNotation.Size = new System.Drawing.Size(105, 43);
            this.lblMainNotation.TabIndex = 2;
            this.lblMainNotation.Text = "漢字";
            // 
            // lblSubNotation
            // 
            this.lblSubNotation.AutoSize = true;
            this.lblSubNotation.Font = new System.Drawing.Font("바탕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSubNotation.Location = new System.Drawing.Point(12, 59);
            this.lblSubNotation.Name = "lblSubNotation";
            this.lblSubNotation.Size = new System.Drawing.Size(58, 24);
            this.lblSubNotation.TabIndex = 3;
            this.lblSubNotation.Text = "カナ";
            // 
            // lblMeaning
            // 
            this.lblMeaning.AutoSize = true;
            this.lblMeaning.Font = new System.Drawing.Font("바탕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMeaning.Location = new System.Drawing.Point(12, 87);
            this.lblMeaning.Name = "lblMeaning";
            this.lblMeaning.Size = new System.Drawing.Size(34, 24);
            this.lblMeaning.TabIndex = 4;
            this.lblMeaning.Text = "뜻";
            // 
            // lblRelatedWords
            // 
            this.lblRelatedWords.AutoSize = true;
            this.lblRelatedWords.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelatedWords.Location = new System.Drawing.Point(13, 167);
            this.lblRelatedWords.Name = "lblRelatedWords";
            this.lblRelatedWords.Size = new System.Drawing.Size(77, 16);
            this.lblRelatedWords.TabIndex = 6;
            this.lblRelatedWords.Text = "관련 단어";
            // 
            // lblWordInfo
            // 
            this.lblWordInfo.Font = new System.Drawing.Font("바탕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblWordInfo.Location = new System.Drawing.Point(590, 9);
            this.lblWordInfo.Name = "lblWordInfo";
            this.lblWordInfo.Size = new System.Drawing.Size(196, 327);
            this.lblWordInfo.TabIndex = 7;
            this.lblWordInfo.Text = "디버그 정보";
            this.lblWordInfo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkMemorized
            // 
            this.chkMemorized.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkMemorized.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkMemorized.Location = new System.Drawing.Point(173, 552);
            this.chkMemorized.Name = "chkMemorized";
            this.chkMemorized.Size = new System.Drawing.Size(150, 52);
            this.chkMemorized.TabIndex = 8;
            this.chkMemorized.Text = "암기(&C)";
            this.chkMemorized.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkMemorized.UseVisualStyleBackColor = true;
            this.chkMemorized.CheckedChanged += new System.EventHandler(this.chkMemorized_CheckedChanged);
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNext.Location = new System.Drawing.Point(636, 552);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 52);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "다음(&A)";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // chkForgot
            // 
            this.chkForgot.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkForgot.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkForgot.Location = new System.Drawing.Point(17, 552);
            this.chkForgot.Name = "chkForgot";
            this.chkForgot.Size = new System.Drawing.Size(150, 52);
            this.chkForgot.TabIndex = 11;
            this.chkForgot.Text = "미암기(&S)";
            this.chkForgot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkForgot.UseVisualStyleBackColor = true;
            this.chkForgot.CheckedChanged += new System.EventHandler(this.chkForgot_CheckedChanged);
            // 
            // barTime
            // 
            this.barTime.Location = new System.Drawing.Point(17, 533);
            this.barTime.Name = "barTime";
            this.barTime.Size = new System.Drawing.Size(769, 13);
            this.barTime.TabIndex = 12;
            // 
            // LearningForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 616);
            this.Controls.Add(this.barTime);
            this.Controls.Add(this.chkForgot);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.chkMemorized);
            this.Controls.Add(this.lblWordInfo);
            this.Controls.Add(this.lblRelatedWords);
            this.Controls.Add(this.lblMeaning);
            this.Controls.Add(this.lblSubNotation);
            this.Controls.Add(this.lblMainNotation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LearningForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LearningForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMainNotation;
        private System.Windows.Forms.Label lblSubNotation;
        private System.Windows.Forms.Label lblMeaning;
        private System.Windows.Forms.Label lblRelatedWords;
        private System.Windows.Forms.Label lblWordInfo;
        private System.Windows.Forms.CheckBox chkMemorized;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.CheckBox chkForgot;
        private System.Windows.Forms.ProgressBar barTime;
    }
}