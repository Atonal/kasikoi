﻿namespace Kasikoi.Forms
{
    partial class AddKanjiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanji = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnRelatedAdd = new System.Windows.Forms.Button();
            this.txtRelated = new System.Windows.Forms.TextBox();
            this.lblRelated = new System.Windows.Forms.Label();
            this.lblRelatedPreview = new System.Windows.Forms.Label();
            this.lblRelatedList = new System.Windows.Forms.Label();
            this.btnAccept = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblKanji
            // 
            this.lblKanji.AutoSize = true;
            this.lblKanji.Font = new System.Drawing.Font("바탕", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblKanji.Location = new System.Drawing.Point(12, 9);
            this.lblKanji.Name = "lblKanji";
            this.lblKanji.Size = new System.Drawing.Size(145, 50);
            this.lblKanji.TabIndex = 0;
            this.lblKanji.Text = "テスト";
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtDescription.Location = new System.Drawing.Point(12, 62);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(370, 26);
            this.txtDescription.TabIndex = 2;
            this.txtDescription.Text = "テスト";
            // 
            // btnRelatedAdd
            // 
            this.btnRelatedAdd.Location = new System.Drawing.Point(231, 99);
            this.btnRelatedAdd.Name = "btnRelatedAdd";
            this.btnRelatedAdd.Size = new System.Drawing.Size(60, 26);
            this.btnRelatedAdd.TabIndex = 18;
            this.btnRelatedAdd.Text = "추가(&R)";
            this.btnRelatedAdd.UseVisualStyleBackColor = true;
            this.btnRelatedAdd.Click += new System.EventHandler(this.btnRelatedAdd_Click);
            // 
            // txtRelated
            // 
            this.txtRelated.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRelated.Location = new System.Drawing.Point(95, 99);
            this.txtRelated.MaxLength = 1;
            this.txtRelated.Name = "txtRelated";
            this.txtRelated.Size = new System.Drawing.Size(130, 26);
            this.txtRelated.TabIndex = 16;
            this.txtRelated.Text = "テスト";
            this.txtRelated.TextChanged += new System.EventHandler(this.txtRelated_TextChanged);
            // 
            // lblRelated
            // 
            this.lblRelated.AutoSize = true;
            this.lblRelated.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelated.Location = new System.Drawing.Point(9, 102);
            this.lblRelated.Name = "lblRelated";
            this.lblRelated.Size = new System.Drawing.Size(80, 18);
            this.lblRelated.TabIndex = 17;
            this.lblRelated.Text = "연관 한자:";
            // 
            // lblRelatedPreview
            // 
            this.lblRelatedPreview.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelatedPreview.Location = new System.Drawing.Point(95, 128);
            this.lblRelatedPreview.Name = "lblRelatedPreview";
            this.lblRelatedPreview.Size = new System.Drawing.Size(130, 18);
            this.lblRelatedPreview.TabIndex = 19;
            this.lblRelatedPreview.Text = "연관 한자:";
            this.lblRelatedPreview.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRelatedList
            // 
            this.lblRelatedList.AutoSize = true;
            this.lblRelatedList.Font = new System.Drawing.Font("바탕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRelatedList.Location = new System.Drawing.Point(92, 160);
            this.lblRelatedList.Name = "lblRelatedList";
            this.lblRelatedList.Size = new System.Drawing.Size(80, 18);
            this.lblRelatedList.TabIndex = 20;
            this.lblRelatedList.Text = "연관 한자:";
            // 
            // btnAccept
            // 
            this.btnAccept.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAccept.Location = new System.Drawing.Point(279, 260);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(103, 37);
            this.btnAccept.TabIndex = 21;
            this.btnAccept.Text = "확인(&N)";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // AddKanjiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 309);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.lblRelatedList);
            this.Controls.Add(this.lblRelatedPreview);
            this.Controls.Add(this.btnRelatedAdd);
            this.Controls.Add(this.txtRelated);
            this.Controls.Add(this.lblRelated);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblKanji);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddKanjiForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "한자 추가";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanji;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnRelatedAdd;
        private System.Windows.Forms.TextBox txtRelated;
        private System.Windows.Forms.Label lblRelated;
        private System.Windows.Forms.Label lblRelatedPreview;
        private System.Windows.Forms.Label lblRelatedList;
        private System.Windows.Forms.Button btnAccept;
    }
}