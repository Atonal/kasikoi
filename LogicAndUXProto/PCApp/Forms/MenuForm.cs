﻿using Kasikoi.Data;
using System;
using System.Text;
using System.Windows.Forms;

namespace Kasikoi.Forms
{
    public partial class MenuForm : Form
    {
        public MenuForm()
        {
            InitializeComponent();
            KeyUp += MenuForm_KeyUp;

            var data = LearningSystem.Instance;
            data.Save();
        }

        void MenuForm_Load(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        void MenuForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                btnAddWord.PerformClick();
            }
            else if (e.KeyCode == Keys.L)
            {
                btnLearn.PerformClick();
            }
        }

        private void btnAddWord_Click(object sender, EventArgs e)
        {
            var addWordForm = new AddWordForm();
            addWordForm.Confirmed += () =>
                {
                    addWordForm.AddWord();
                    addWordForm.NewWord();
                };
            addWordForm.Cancelled += () =>
                {
                    addWordForm.NewWord();
                };
            addWordForm.FormClosed += delegate {
                LearningSystem.Instance.Save();
                UpdateStatus(); 
            };
            addWordForm.ShowDialog();
        }

        private void UpdateStatus()
        {
            var data = LearningSystem.Instance;

            var statusTextBuilder = new StringBuilder();
            statusTextBuilder.Append("(암기/학습 중/전체) 단어 수: ");
            statusTextBuilder.Append(data.GetMemorizedWord());
            statusTextBuilder.Append("/");
            statusTextBuilder.Append(data.GetEncounteredWord());
            statusTextBuilder.Append("/");
            statusTextBuilder.Append(data.WordCount);
            statusTextBuilder.AppendLine();

            statusTextBuilder.Append("총 한자 수: ");
            statusTextBuilder.Append(data.KanjiCount);
            statusTextBuilder.AppendLine();

            if (data.RecentlyAddedWord != null)
            {
                statusTextBuilder.Append("최근 추가 된 단어: ");
                data.RecentlyAddedWord.ToStringBuilder(statusTextBuilder, 3);
                statusTextBuilder.AppendLine();
            }

            if (data.RecentlyAddedKanji != null)
            {
                statusTextBuilder.Append("최근 추가 된 한자: ");
                statusTextBuilder.Append(data.RecentlyAddedKanji.ToString());
                statusTextBuilder.AppendLine();
            }

            lblStatus.Text = statusTextBuilder.ToString();

            btnLearn.Enabled = data.WordCount > 0;
        }

        private void btnLearn_Click(object sender, EventArgs e)
        {
            var learning = new LearningForm();
            learning.FormClosed += delegate { UpdateStatus(); };
            learning.ShowDialog();
        }
    }
}
