﻿using Kasikoi.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kasikoi.Forms
{
    public partial class AddKanjiForm : Form
    {
        public AddKanjiForm(char kanji)
        {
            InitializeComponent();

            lblKanji.Text = kanji.ToString();
            txtDescription.Text = "";
            txtRelated.Text = "";
            lblRelatedList.Text = "";
            lblRelatedPreview.Text = "";

            KeyUp += AddKanjiForm_KeyUp;
        }

        void AddKanjiForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Handled) return;

            if (e.Control)
            {
                if (e.KeyCode == Keys.N)
                {
                    btnAccept.PerformClick();
                    e.Handled = true;
                }
            }
        }

        public event Action Accepted = delegate { };

        public Kanji KanjiAdded { get; private set; }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            var descs = StringHelper.SplitWithPunctuation(txtDescription.Text);
            if (descs.Length == 0)
            {
                MessageBox.Show("한자 설명을 달아야 합니다.");
                return;
            }

            var data = LearningSystem.Instance;
            var kanji = data.AddKanji(new Kanji(0, lblKanji.Text, descs));
            KanjiAdded = kanji;

            foreach (var related in relatedKanjiSet)
                data.AddKanjiRelation(kanji, related);

            Close();
            Accepted();
        }

        HashSet<Kanji> relatedKanjiSet = new HashSet<Kanji>();
        private void txtRelated_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRelated.Text))
            {
                lblRelatedPreview.Text = "";
                return;
            }

            var data = LearningSystem.Instance;
            if (data.ContainsKanji(txtRelated.Text))
            {
                var kanji = data.GetKanji(txtRelated.Text);
                lblRelatedPreview.Text = kanji.ToString();
            }
            else
            {
                lblRelatedPreview.Text = "신규 한자";
            }
        }

        private void btnRelatedAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtRelated.Text))
            {
                return;
            }

            var data = LearningSystem.Instance;
            if (data.ContainsKanji(txtRelated.Text))
            {
                var kanji = data.GetKanji(txtRelated.Text);
                relatedKanjiSet.Add(kanji);
                UpdateRelatedList();

                txtRelated.Text = "";
                txtRelated.Focus();
            }
            else
            {
                var addKanjiForm = new AddKanjiForm(txtRelated.Text[0]);
                addKanjiForm.Accepted += () =>
                {
                    relatedKanjiSet.Add(addKanjiForm.KanjiAdded);
                    UpdateRelatedList();

                    txtRelated.Text = "";
                    txtRelated.Focus();
                };
                addKanjiForm.ShowDialog();
            }
        }

        void UpdateRelatedList()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var kanji in relatedKanjiSet)
            {
                sb.Append(kanji.Character);
                sb.Append(" - ");
                StringHelper.AppendWithSemicolonTo(sb, kanji.Descriptions);
                sb.AppendLine();
            }
            lblRelatedList.Text = sb.ToString();
        }
    }
}
