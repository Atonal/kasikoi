﻿using Kasikoi.Data;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Kasikoi.Forms
{
    public partial class LearningForm : Form
    {
        public LearningForm()
        {
            InitializeComponent();

            FormClosed += LearningForm_FormClosed;

            var system = LearningSystem.Instance;
            NextProblem(system.NextProblem());
        }

        void LearningForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            var system = LearningSystem.Instance;
            system.Save();
        }

        bool isProblemState = false;
        Problem currentProblem;
        void NextProblem(Problem problem)
        {
            currentProblem = problem;
            SetWord(problem.Word);
            if (problem.Type == ProblemType.HideKanji)
            {
                lblMainNotation.Text = "■";
            }
            else if (problem.Type == ProblemType.HideKana)
            {
                if (problem.Word.KanjiNotations.Count == 0)
                    lblMainNotation.Text = "■";
                else
                    lblSubNotation.Text = "■";
            }
            else if (problem.Type == ProblemType.HideMeaning)
            {
                lblMeaning.Text = "■";
            }
            lblRelatedWords.Visible = false;

            lblWordInfo.Text = problem.DebugInfo;

            isProblemState = true;
            chkForgot.Checked = false;
            chkMemorized.Checked = false;
            btnNext.Enabled = false;
        }

        void SetWord(Word word)
        {
            var mainNotation = word.KanjiNotations;
            var subNotation = word.KanaNotations;
            if (mainNotation.Count == 0)
            {
                mainNotation = subNotation;
                subNotation = new string[0];
            }
            var currentWord = currentProblem.Word;
            var meaning = currentWord.Meanings;
            lblMainNotation.Text = StringHelper.ConcatenateWithSemicolon(mainNotation);
            lblSubNotation.Text = StringHelper.ConcatenateWithSemicolon(subNotation);
            lblMeaning.Text = StringHelper.ConcatenateWithSemicolon(meaning);
        }

        void ShowAll()
        {
            var currentWord = currentProblem.Word;
            SetWord(currentWord);

            var data = LearningSystem.Instance;
            lblRelatedWords.Text = data.GetDescription(currentWord);
            lblRelatedWords.Visible = true;

            btnNext.Enabled = true;

            isProblemState = false;
        }

        private void chkForgot_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkForgot.Checked)
            {
                chkMemorized.Checked = false;
                if (isProblemState)
                    ShowAll();
            }
            else if (!chkMemorized.Checked && !isProblemState)
            {
                chkForgot.Checked = true;
            }
        }

        private void chkMemorized_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkMemorized.Checked)
            {
                chkForgot.Checked = false;
                if (isProblemState)
                    ShowAll();
            }
            else if (!chkForgot.Checked && !isProblemState)
            {
                chkMemorized.Checked = true;
            }
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            var memorized = chkMemorized.Checked;
            var system = LearningSystem.Instance;
            if (memorized)
                system.MarkCorrect(currentProblem);
            else
                system.MarkWrong(currentProblem);
            NextProblem(system.NextProblem());
        }
    }
}
