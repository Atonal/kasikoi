﻿using System;
using System.Windows.Forms;

namespace Kasikoi
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Forms.MenuForm());

            var data = Data.LearningSystem.Instance;
            if (data.WordCount > 0)
                data.Save();
        }
    }
}
