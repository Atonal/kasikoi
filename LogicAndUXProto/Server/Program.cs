﻿using Kasikoi.Data;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Kasikoi.Server
{
    class Program
    {
        static Socket listener;

        static void Main(string[] args)
        {
            Console.WriteLine("KASIKOI Temp Server");

            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 25250);
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(ipEndPoint);
            listener.Listen(100);
            listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
            Console.WriteLine("Wating for user data");

            while (true)
                Thread.Sleep(1000);
        }

        static byte[] buffer = new byte[5 * 1024 * 1024];
        static void AcceptCallback(IAsyncResult ar)
        {
            lock (buffer)
            {
                var listener = (Socket)ar.AsyncState;
                var handler = listener.EndAccept(ar);

                var recvStr = ReadRequest(handler);
                Console.WriteLine("----------");
                Console.WriteLine("User request received. len=" + recvStr.Length);
                try
                {
                    var remoteData = new LearningSystem(recvStr);
                    var remoteDate = remoteData.LastLogRecorded();
                    var hostDate = LearningSystem.Instance.LastLogRecorded();
                    Console.WriteLine("remote date: " + remoteDate.ToString());
                    Console.WriteLine("host date: " + hostDate.ToString());
                    if (remoteDate < hostDate)
                    {
                        SendHostData(handler);
                    }
                    else if (remoteDate == hostDate)
                    {
                        Console.WriteLine("data already synced.");
                        SendResponse(handler, "ok");
                    }
                    else
                    {
                        Console.WriteLine("take remote data to overwrite host data.");
                        LearningSystem.ChangeInstance(remoteData);
                        SendResponse(handler, "ok");
                    }
                }
                catch
                {
                    Console.WriteLine("error raised.");
                    SendHostData(handler);
                }
                finally
                {
                    handler.Close();
                    Console.WriteLine("----------");
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                    Console.WriteLine("Wating for user data");
                }
            }
        }

        static void SendHostData(Socket socket)
        {
            var hostData = LearningSystem.Serialize(LearningSystem.Instance);
            Console.WriteLine("sending host data. len=" + hostData.Length);
            SendResponse(socket, hostData);
        }

        static string ReadRequest(Socket requestedSocket)
        {
            var length = 0;
            var contentLength = 0;
            while (length < buffer.Length)
            {
                var read = requestedSocket.Receive(buffer, length, 1, SocketFlags.None);
                length += read;

                if (length >= 2)
                {
                    if (buffer[length - 2] == '\r' && buffer[length - 1] == '\n')
                    {
                        var headerLine = Encoding.UTF8.GetString(buffer, 0, length);
                        length = 0;
                        headerLine = headerLine.ToLower();
                        if (headerLine.StartsWith("content-length: "))
                        {
                            var colonPos = headerLine.IndexOf(':');
                            var lengthString = headerLine.Substring(colonPos + 1);
                            contentLength = int.Parse(lengthString);
                        }
                        if (headerLine == "\r\n")
                            break;
                    }
                }
            }

            if (contentLength > buffer.Length)
                return "";

            length = 0;
            while (length < contentLength)
            {
                var remains = buffer.Length - length;
                var read = requestedSocket.Receive(buffer, length, 1, SocketFlags.None);
                length += read;
            }

            return Encoding.UTF8.GetString(buffer, 0, length);
        }

        static void SendResponse(Socket socket, string response)
        {
            var content = Encoding.UTF8.GetBytes(response);
            var header = "HTTP/1.0 200 ok\r\n"
                + "Data: " + DateTime.Now.ToString() + "\r\n"
                + "Server: Kasikoi Temp Server\r\n"
                + "Content-Length: " + content.Length + "\r\n"
                + "content-type:text/html\r\n"
                + "\r\n";
            socket.Send(Encoding.UTF8.GetBytes(header));
            socket.Send(content);
        }
    }
}
