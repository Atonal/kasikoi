﻿using System.Text;

namespace Kasikoi.Data.Parsers
{
    static class WordParser
    {
        public static Word Parse(string[] textSplittedByLine, ref int currentLine)
        {
            var id = int.Parse(textSplittedByLine[currentLine++]);
            var kanjiNotations = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
            var kanaNotations = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
            var meanings = StringHelper.SplitWithPunctuation(textSplittedByLine[currentLine++]);
            return new Word(id, kanjiNotations, kanaNotations, meanings);
        }

        public static void AppendTo(StringBuilder sb, Word word)
        {
            sb.AppendLine(word.Id.ToString());
            StringHelper.AppendWithSemicolonTo(sb, word.KanjiNotations);
            sb.AppendLine();
            StringHelper.AppendWithSemicolonTo(sb, word.KanaNotations);
            sb.AppendLine();
            StringHelper.AppendWithSemicolonTo(sb, word.Meanings);
            sb.AppendLine();
        }
    }
}
