﻿using System.Collections.Generic;
using System.Text;

namespace Kasikoi.Data
{
    /// <summary>
    /// 학습 대상이 되는 전체 단어 사전
    /// </summary>
    public class WordBook
    {
        public WordBook()
        {
        }

        public WordBook(IEnumerable<Word> words)
        {
            foreach (var w in words)
                AddWord(w);
        }

        public Word Add(Word word)
        {
            while (idToWordMap.ContainsKey(idNextEmpty))
                idNextEmpty++;

            var wordWithId = new Word(idNextEmpty, word);
            AddWord(wordWithId);
            idNextEmpty++;

            return wordWithId;
        }

        void AddWord(Word word)
        {
            allWords.Add(word);
            idToWordMap.Add(word.Id, word);
            foreach (var kana in word.KanaNotations)
            {
                kanaToWordMap.Add(kana, word);
            }
            foreach (var kanji in word.KanjiNotations)
            {
                kanjiToWordMap.Add(kanji, word);
            }
        }

        public Word this[int id]
        {
            get
            {
                return idToWordMap[id];
            }
        }

        public int WordCount
        {
            get
            {
                return allWords.Count;
            }
        }

        public IReadOnlyList<Word> GetWordWithKanaNotation(string kanaNotation)
        {
            return kanaToWordMap[kanaNotation];
        }

        public IReadOnlyList<Word> GetWordWithKanjiNotation(string kanjiNotation)
        {
            return kanjiToWordMap[kanjiNotation];
        }

        public IReadOnlyList<Word> GetAllWords()
        {
            return allWords;
        }

        WordMap kanaToWordMap = new WordMap();
        WordMap kanjiToWordMap = new WordMap();
        Dictionary<int, Word> idToWordMap = new Dictionary<int, Word>();
        List<Word> allWords = new List<Word>();
        int idNextEmpty = 0;
    }
}
