﻿using Kasikoi.Data;

namespace Kasikoi.Data
{
    public class Problem
    {
        public Problem(Word word, ProblemType type, string debugInfo)
        {
            Word = word;
            Type = type;
            DebugInfo = debugInfo;
        }

        public readonly Word Word;
        public readonly ProblemType Type;
        public readonly string DebugInfo;
    }

    public enum ProblemType
    {
        HideKanji,
        HideKana,
        HideMeaning
    }
}
