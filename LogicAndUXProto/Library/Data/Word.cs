﻿using System.Collections.Generic;
using System.Text;

namespace Kasikoi.Data
{
    /// <summary>
    /// 하나의 단어를 표현하는 객체
    /// </summary>
    public class Word
    {
        public Word(int id, string[] kanjiNotations, string[] kanaNotations, string[] meanings)
        {
            Id = id;
            this.kanjiNotations = kanjiNotations ?? new string[0];
            this.kanaNotations = kanaNotations ?? new string[0];
            this.meanings = meanings ?? new string[0];
        }

        public Word(int id, Word word)
        {
            Id = id;
            kanjiNotations = word.kanjiNotations;
            kanaNotations = word.kanaNotations;
            meanings = word.meanings;
        }

        public readonly int Id;

        string[] kanjiNotations;
        public IReadOnlyList<string> KanjiNotations
        {
            get
            {
                return kanjiNotations;
            }
        }

        string[] kanaNotations;
        public IReadOnlyList<string> KanaNotations
        {
            get
            {
                return kanaNotations;
            }
        }
        
        string[] meanings;
        public IReadOnlyList<string> Meanings
        {
            get
            {
                return meanings;
            }
        }

        public IEnumerable<char> ComposedKanji
        {
            get
            {
                HashSet<char> kanjiSet = new HashSet<char>();
                foreach (var notations in kanjiNotations)
                {
                    foreach (var c in notations)
                    {
                        if (StringHelper.IsKanji(c))
                            kanjiSet.Add(c);
                    }
                }
                return kanjiSet;
            }
        }

        #region ToString

        public override string ToString()
        {
            return ToString(1);
        }

        public string ToString(int maxMeaning)
        {
            StringBuilder sb = new StringBuilder();
            ToStringBuilder(sb, maxMeaning);
            return sb.ToString();
        }

        public void ToStringBuilder(StringBuilder appendTarget, int maxMeaning = 1)
        {
            if (KanjiNotations.Count > 0)
                appendTarget.Append(KanjiNotations[0]);

            if (KanaNotations.Count > 0)
                appendTarget.AppendFormat("[{0}]", KanaNotations[0]);

            if (Meanings.Count > 0)
                appendTarget.AppendFormat(": {0}", Meanings[0]);

            for (int i = 1; i < maxMeaning && i < Meanings.Count; i++)
            {
                appendTarget.AppendFormat(", {0}", Meanings[i]);
            }
        }

        #endregion
    }
}
