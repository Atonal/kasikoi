﻿using Kasikoi.Data.Parsers;
using System.Collections.Generic;
using System.Text;

namespace Kasikoi.Data
{
    public partial class LearningSystem
    {
        class FileSection
        {
            public const string WordBook = "Word Book";
            public const string KanjiBook = "Kanji Book";
            public const string RelatedWords = "Related Words";
            public const string RelatedKanji = "Related Kanjis";
            public const string RecentWord = "Recently Added Word";
            public const string RecentKanji = "Recently Added Kanji";
            public const string WordRecord = "Word Record";
            public const string ProblemState = "Problem Selector State";
            public const string FCurveCorrect = "Forgetting Curve - Correct";
            public const string FCurveWrong = "Forgetting Curve - Wrong";
            public const string Log = "Log";
            public const string EndSection = "End Section";
        }

        static class ParseHelper
        {
            public static void MoveToSection(string section, string[] parseLines, ref int currentLine)
            {
                var marker = SectionMarker(section);
                while (currentLine < parseLines.Length)
                {
                    if (parseLines[currentLine] == marker)
                    {
                        currentLine++;
                        return;
                    }
                    currentLine++;
                }
            }

            public static int MoveToEndSection(string[] parseLines, ref int currentLine)
            {
                var marker = EndSectionMarker;
                while (currentLine < parseLines.Length)
                {
                    if (parseLines[currentLine] == marker)
                    {
                        currentLine++;
                        return currentLine - 1;
                    }
                    currentLine++;
                }
                return parseLines.Length;
            }

            static string SectionMarker(string section)
            {
                return "# " + section;
            }

            public static void AppendSection(StringBuilder sb, string section)
            {
                sb.AppendLine(SectionMarker(section));
            }

            static string EndSectionMarker = SectionMarker(FileSection.EndSection);
            public static bool IsEndSection(string[] parseLines, int currentLine)
            {
                return parseLines[currentLine] == EndSectionMarker;
            }
        }

        static void Parse(string[] parseLines, LearningSystem parseTarget)
        {
            int currentLine = 0;

            ParseHelper.MoveToSection(FileSection.WordBook, parseLines, ref currentLine);
            var words = new List<Word>();
            while (currentLine < parseLines.Length)
            {
                if (ParseHelper.IsEndSection(parseLines, currentLine))
                    break;
                var word = WordParser.Parse(parseLines, ref currentLine);
                words.Add(word);
            }
            parseTarget.wordBook = new WordBook(words);
            parseTarget.kanjiCharacterToWord.Clear();
            foreach (var w in words)
            {
                foreach (var c in w.ComposedKanji)
                {
                    parseTarget.kanjiCharacterToWord.Add(c.ToString(), w);
                }
            }

            ParseHelper.MoveToSection(FileSection.KanjiBook, parseLines, ref currentLine);
            var kanjis = new List<Kanji>();
            while (currentLine < parseLines.Length)
            {
                if (ParseHelper.IsEndSection(parseLines, currentLine))
                    break;

                var kanji = KanjiParser.Parse(parseLines, ref currentLine);
                kanjis.Add(kanji);
            }
            parseTarget.kanjiBook = new KanjiBook(kanjis);

            ParseHelper.MoveToSection(FileSection.RelatedWords, parseLines, ref currentLine);
            var relatedWordFrom = currentLine;
            var relatedWordTo = ParseHelper.MoveToEndSection(parseLines, ref currentLine);
            parseTarget.relatedWords = IdRelationParser.Parse(parseLines, relatedWordFrom, relatedWordTo);

            ParseHelper.MoveToSection(FileSection.RelatedKanji, parseLines, ref currentLine);
            var relatedKanjiFrom = currentLine;
            var relatedKanjiTo = ParseHelper.MoveToEndSection(parseLines, ref currentLine);
            parseTarget.relatedKanjis = IdRelationParser.Parse(parseLines, relatedKanjiFrom, relatedKanjiTo);

            ParseHelper.MoveToSection(FileSection.RecentWord, parseLines, ref currentLine);
            try
            {
                var id = int.Parse(parseLines[currentLine]);
                parseTarget.recentlyAddedWord = parseTarget.wordBook[id];
            }
            catch
            {
                if (words.Count > 0)
                    parseTarget.recentlyAddedWord = words[words.Count - 1];
                else
                    parseTarget.recentlyAddedWord = null;
            }

            ParseHelper.MoveToSection(FileSection.RecentKanji, parseLines, ref currentLine);
            try
            {
                var id = int.Parse(parseLines[currentLine]);
                parseTarget.recentlyAddedKanji = parseTarget.kanjiBook[id];
            }
            catch
            {
                if (kanjis.Count > 0)
                    parseTarget.recentlyAddedKanji = kanjis[kanjis.Count - 1];
                else
                    parseTarget.recentlyAddedKanji = null;
            }

            ParseHelper.MoveToSection(FileSection.WordRecord, parseLines, ref currentLine);
            parseTarget.wordRecords.Clear();
            while (currentLine < parseLines.Length)
            {
                if (ParseHelper.IsEndSection(parseLines, currentLine))
                    break;

                var record = WordRecord.Parse(parseLines, ref currentLine);
                parseTarget.wordRecords.Add(record.Id, record);
            }

            ParseHelper.MoveToSection(FileSection.FCurveCorrect, parseLines, ref currentLine);
            if (currentLine < parseLines.Length)
                parseTarget.forgettingCurve.ParseCorrectTable(parseLines, ref currentLine);
            ParseHelper.MoveToSection(FileSection.FCurveWrong, parseLines, ref currentLine);
            if (currentLine < parseLines.Length)
                parseTarget.forgettingCurve.ParseWrongTable(parseLines, ref currentLine);

            ParseHelper.MoveToSection(FileSection.Log, parseLines, ref currentLine);
            var logFrom = currentLine;
            var logTo = ParseHelper.MoveToEndSection(parseLines, ref currentLine);
            parseTarget.logList.Clear();
            for (int line = logFrom; line < logTo; line++)
                parseTarget.logList.Add(parseLines[line]);

            parseTarget.problemSelector = new ProblemSelector(
                parseTarget.wordBook,
                parseTarget.wordRecords);
        }

        public static string Serialize(LearningSystem data)
        {
            StringBuilder sb = new StringBuilder();

            ParseHelper.AppendSection(sb, FileSection.WordBook);
            var words = data.GetAllWords();
            foreach (var w in words)
                WordParser.AppendTo(sb, w);
            ParseHelper.AppendSection(sb, FileSection.EndSection);
            sb.AppendLine();
            
            ParseHelper.AppendSection(sb, FileSection.KanjiBook);
            var kanjis = data.GetAllKanjis();
            foreach (var k in kanjis)
                KanjiParser.AppendTo(sb, k);
            ParseHelper.AppendSection(sb, FileSection.EndSection);
            sb.AppendLine();
            
            ParseHelper.AppendSection(sb, FileSection.RelatedWords);
            IdRelationParser.AppendTo(sb, data.relatedWords);
            ParseHelper.AppendSection(sb, FileSection.EndSection);
            sb.AppendLine();
            
            ParseHelper.AppendSection(sb, FileSection.RelatedKanji);
            IdRelationParser.AppendTo(sb, data.relatedKanjis);
            ParseHelper.AppendSection(sb, FileSection.EndSection);

            if (data.recentlyAddedWord != null)
            {
                ParseHelper.AppendSection(sb, FileSection.RecentWord);
                sb.Append(data.recentlyAddedWord.Id);
                sb.AppendLine();
            }

            if (data.recentlyAddedKanji != null)
            {
                ParseHelper.AppendSection(sb, FileSection.RecentKanji);
                sb.Append(data.recentlyAddedKanji.Id);
                sb.AppendLine();
            }

            ParseHelper.AppendSection(sb, FileSection.WordRecord);
            foreach (var r in data.wordRecords.Values)
                r.AppendTo(sb);
            ParseHelper.AppendSection(sb, FileSection.EndSection);

            ParseHelper.AppendSection(sb, FileSection.FCurveCorrect);
            data.forgettingCurve.AppendCorrectTable(sb);
            ParseHelper.AppendSection(sb, FileSection.FCurveWrong);
            data.forgettingCurve.AppendWrongTable(sb);

            ParseHelper.AppendSection(sb, FileSection.Log);
            foreach (var log in data.logList)
                sb.AppendLine(log);
            ParseHelper.AppendSection(sb, FileSection.EndSection);


            return sb.ToString();
        }
    }
}
