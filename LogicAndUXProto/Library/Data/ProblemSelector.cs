﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Kasikoi.Data
{
    public class ProblemSelector
    {
        public ProblemSelector(WordBook wordBook, IReadOnlyDictionary<int, WordRecord> records)
        {
            this.wordBook = wordBook;
            this.records = records;

            var allWords = wordBook.GetAllWords();
            foreach (var word in allWords)
                newWordSet.Add(word.Id);

            foreach (var record in records.Values)
            {
                newWordSet.Remove(record.Id);
                waitingSet.Add(record.Id);

#if PC
                var weight = Heuristics.WordWeight(record);
                var word = wordBook[record.Id];
                Debug.WriteLine("lv:" + record.Level + " weight:" + weight.ToString("F2") + " - " + word.ToString(1));
#endif
            }
        }

        public Problem NextProblem()
        {
            if (nextIndex >= workingList.Count)
            {
                EndLearning();
                FillWorkingList();
                ShuffleWorkingList();
                nextIndex = 0;
            }
            if (workingList.Count == 0)
                return null;

            var wordId = workingList[nextIndex++];
            var word = wordBook[wordId];
            var type = DecideProblemType(word);
            var debugInfo = BuildDebugInfo(wordId);
            return new Problem(word, type, debugInfo);
        }

        ProblemType DecideProblemType(Word word)
        {
            WordRecord record = null;
            if (records.ContainsKey(word.Id))
                record = records[word.Id];
            else
                record = new WordRecord(word.Id); // null object

            var availableType = new List<ProblemType>();
            var correctCounts = new List<int>();
            if (word.KanjiNotations.Count > 0)
            {
                availableType.Add(ProblemType.HideKanji);
                correctCounts.Add(record.CorrectKanjiCount);
            }
            if (word.KanaNotations.Count > 0)
            {
                availableType.Add(ProblemType.HideKana);
                correctCounts.Add(record.CorrectKanaCount);
            }
            if (word.Meanings.Count > 0)
            {
                availableType.Add(ProblemType.HideMeaning);
                correctCounts.Add(record.CorrectMeaningCount);
            }
            var weight = Heuristics.ProblemTypeWeight(correctCounts);
            var index = RandomHelper.GetIndexWithWeight(weight);
            return availableType[index];
        }

        string BuildDebugInfo(int wordId)
        {
            if (!records.ContainsKey(wordId))
                return "신규 단어!";

            var record = records[wordId];
            var info = new StringBuilder();
            info.AppendLine("단어 id: " + wordId);
            info.AppendLine("암기 레벨: " + record.Level);
            info.AppendLine(string.Format(
                "정답/전체: {0}/{1}", record.CorrectCount, record.CorrectCount + record.WrongCount));
            info.AppendLine(string.Format("휴리스틱: {0:F2}", Heuristics.WordWeight(record)));
            info.AppendLine(string.Format(
                "문제형식: {0}/{1}/{2}",
                record.CorrectKanjiCount,
                record.CorrectKanaCount,
                record.CorrectMeaningCount));
            info.AppendLine("암기 시작: " + record.EncounterTime.ToString("yyyy-MM-dd HH:mm"));
            info.AppendLine("최근 학습: " + record.RecentTime.ToString("yyyy-MM-dd HH:mm"));
            
            double forgetStarts;
            double forgetEnds;
            Heuristics.GetForgetCurve(record, out forgetStarts, out forgetEnds);
            info.AppendLine("최근 오답 수: " + record.GetRecentWrongCount());
            info.AppendLine("경과일: " + (DateTime.Now - record.RecentTime).TotalDays.ToString("F1"));
            info.AppendLine("망각 시작일: " + forgetStarts.ToString("F1"));

            return info.ToString();
        }

        void FillWeightList()
        {
            candidateWeightList.Clear();
            candidateIdList.Clear();
            var minWeight = double.MaxValue;
            var minWeightIndex = 0;
            foreach (var id in waitingSet)
            {
                var record = records[id];
                var weight = Heuristics.WordWeight(record);
                if (candidateWeightList.Count >= Heuristics.MaxWeightPool
                    && weight > minWeight)
                {
                    candidateWeightList.RemoveAt(minWeightIndex);
                    candidateIdList.RemoveAt(minWeightIndex);
                    minWeight = double.MaxValue;
                    for (int i = 0; i < candidateWeightList.Count; i++)
                    {
                        if (candidateWeightList[i] < minWeight)
                        {
                            minWeight = candidateWeightList[i];
                            minWeightIndex = i;
                        }
                    }
                }
                if (candidateWeightList.Count < Heuristics.MaxWeightPool)
                {
                    if (weight < minWeight)
                    {
                        minWeight = weight;
                        minWeightIndex = candidateWeightList.Count;
                    }
                    candidateWeightList.Add(weight);
                    candidateIdList.Add(id);
                }
            }
        }

        void FillMaxList()
        {
            maxWeights.Clear();
            foreach (var w in candidateWeightList)
            {
                if (maxWeights.Count < workingListTargetSize)
                {
                    maxWeights.Add(w);
                    if (maxWeights.Count == workingListTargetSize)
                    {
                        maxWeights.Sort();
                        maxWeights.Reverse();
                    }
                }
                else
                {
                    if (maxWeights[workingListTargetSize - 1] < w)
                    {
                        if (w > maxWeights[0])
                        {
                            maxWeights.Insert(0, w);
                        }
                        else
                        {
                            for (int i = 0; i < maxWeights.Count; i++)
                                if (maxWeights[i] < w)
                                {
                                    maxWeights.Insert(i, w);
                                    break;
                                }
                        }
                        while (maxWeights.Count > workingListTargetSize)
                            maxWeights.RemoveAt(workingListTargetSize);
                    }
                }
            }
        }

        void FillWorkingList()
        {
            var from = workingList.Count;
            var to = workingListTargetSize;

            FillWeightList();
            FillMaxList();

            int maxWeightIndex = maxWeights.Count - 1;
            for (int i = from; i < to; i++)
            {
                if (newWordSet.Count > 0)
                {
                    var newWordProb = 1.0;
                    if (maxWeightIndex >= 0)
                        newWordProb = Heuristics.NewWordProbability(maxWeights[maxWeightIndex--]);
                    //Debug.WriteLine("new word prob: " + (newWordProb * 100).ToString("F2") + "%");
                    if (RandomHelper.EventWithProbability(newWordProb))
                    {
                        var newWordIndex = RandomHelper.Rand(newWordSet.Count);
                        PickNewWord(newWordIndex);
                        continue;
                    }
                }
                if (candidateIdList.Count > 0)
                {
                    var waitingIndex = RandomHelper.GetIndexWithWeight(candidateWeightList);
                    PickWaiting(waitingIndex);
                }
            }
        }

        void ShuffleWorkingList()
        {
            for (int i = 0; i < workingList.Count / 2; i++)
            {
                var from = i;
                var to = RandomHelper.Rand(workingList.Count / 2) + workingList.Count / 2;
                SwapWorkingList(from, to);
            }
        }

        void SwapWorkingList(int from, int to)
        {
            var temp = workingList[from];
            workingList[from] = workingList[to];
            workingList[to] = temp;
        }

        void PickNewWord(int index)
        {
            var newWordId = newWordSet.ElementAt(index);
            newWordSet.Remove(newWordId);
            workingList.Add(newWordId);
        }

        void PickWaiting(int index)
        {
            var wordId = candidateIdList[index];
            candidateIdList.RemoveAt(index);
            candidateWeightList.RemoveAt(index);

            waitingSet.Remove(wordId);
            workingList.Add(wordId);
            Debug.WriteLine("pick: " + wordId);
        }

        void EndLearning()
        {
            for (int i = 0; i < workingList.Count; i++)
            {
                var wordId = workingList[i];
                var record = records[wordId];

                if (record.IsRecentCorrect())
                {
                    Debug.WriteLine("end: " + wordId + " = " + wordBook[wordId].KanaNotations[0]);
                    workingList.RemoveAt(i);
                    waitingSet.Add(wordId);
                    i--;
                }
            }
        }

        int nextIndex = 0;
        const int workingListTargetSize = 25;
        List<int> workingList = new List<int>();
        List<double> candidateWeightList = new List<double>();
        List<int> candidateIdList = new List<int>();
        List<double> maxWeights = new List<double>();
        SortedSet<int> waitingSet = new SortedSet<int>();
        SortedSet<int> newWordSet = new SortedSet<int>();
        WordBook wordBook;
        IReadOnlyDictionary<int, WordRecord> records;
    }
}
