﻿using System.Collections.Generic;

namespace Kasikoi.Data
{
    /// <summary>
    /// 한자를 표현하는 객체
    /// </summary>
    public class Kanji
    {
        public Kanji(int id, string character, string[] descriptions)
        {
            Id = id;
            Character = character;
            this.descriptions = descriptions ?? new string[0];
        }

        public Kanji(int id, Kanji kanji)
        {
            Id = id;
            Character = kanji.Character;
            this.descriptions = kanji.descriptions;
        }

        public readonly int Id;

        public readonly string Character;

        string[] descriptions;
        public IReadOnlyList<string> Descriptions
        {
            get
            {
                return descriptions;
            }
        }

        #region ToString

        public override string ToString()
        {
            return string.Format("{0}({1})", Character, descriptions[0]);
        }

        #endregion
    }
}
