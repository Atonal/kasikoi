﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Kasikoi.Data
{
    static class Heuristics
    {
        public static IReadOnlyList<double> ProblemTypeWeight(IReadOnlyList<int> correctCounts)
        {
            int sum = 0;
            int max = 0;
            var weights = new List<double>();
            bool hasZero = false;
            foreach (var cnt in correctCounts)
            {
                sum += cnt;
                max = Math.Max(cnt, max);
                weights.Add(1);
                if (cnt == 0)
                    hasZero = true;
            }

            if (sum > 0)
            {
                for (int i = 0; i < correctCounts.Count; i++)
                {
                    var cnt = correctCounts[i];
                    var diffToMax = max - cnt;
                    weights[i] = max + Math.Pow(diffToMax, 3) * 20;

                    if (hasZero && cnt != 0)
                        weights[i] = 0;
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("ProblemSelectorHeuristics.ProblemTypeWeight");
            sb.Append("    correctCounts = ");
            StringHelper.AppendWithSpaceTo(sb, correctCounts);
            sb.AppendLine();
            sb.Append("    weights = ");
            StringHelper.AppendWithSpaceTo(sb, weights);
            sb.AppendLine();
            //Debug.Write(sb.ToString());

            return weights;
        }

        public static double WordWeight(WordRecord record)
        {
            var prob = MemorizedProbability(record);
            if (prob > MemoryProbabilityAtForgetStarts)
            {
                var value = MapLinear(prob, 1, 0, MemoryProbabilityAtForgetStarts, 1);
                return value * value;
            }
            else
            {
                var weightByProb = MapLinear(prob, MemoryProbabilityAtForgetStarts, 0, 0, 10);
                weightByProb = weightByProb * weightByProb + 1;

                var weight = weightByProb;

                double forgetStarts;
                double forgetEnds;
                GetForgetCurve(record, out forgetStarts, out forgetEnds);

                var daysAfterRecent = (DateTime.Now - record.RecentTime).TotalDays;

                if (daysAfterRecent > forgetStarts)
                {
                    var weightByDayAfter = daysAfterRecent - forgetStarts;
                    weight += weightByDayAfter;
                }

                if (daysAfterRecent > forgetEnds)
                {
                    var dayAfterForgetEnd = daysAfterRecent - forgetEnds;
                    var clamped = Math.Min(dayAfterForgetEnd, 10);
                    var weightByForgetAfter = clamped * clamped;
                    weight += weightByForgetAfter;
                }

                return weight;
            }
        }

        public static void GetForgetCurve(WordRecord record, out double forgetStarts, out double forgetEnds)
        {
            GetForgetCurve(record.Level, record.GetRecentWrongCount(), out forgetStarts, out forgetEnds);
        }

        static void GetForgetCurve(int level, int wrongCount, out double forgetStarts, out double forgetEnds)
        {
            forgetStarts = ForgetStarts[level];
            forgetStarts = forgetStarts / (wrongCount + 1);
            forgetEnds = forgetStarts * 3;
        }

        public static double MemorizedProbability(WordRecord record)
        {
            if (!record.IsRecentCorrect())
                return 0;

            var daysAfterRecent = (DateTime.Now - record.RecentTime).TotalDays;

            return MemorizedProbability(
                record.Level,
                record.GetRecentWrongCount(),
                daysAfterRecent);
        }

        static double MemorizedProbability(int level, int wrongCount, double daysAfterRecent)
        {
            double forgetStarts;
            double forgetEnds;
            GetForgetCurve(level, wrongCount, out forgetStarts, out forgetEnds);

            return MemorizedProbability(level, wrongCount, daysAfterRecent, forgetStarts, forgetEnds);
        }

        static double MemorizedProbability(int level, int wrongCount, double daysAfterRecent, double forgetStarts, double forgetEnds)
        {
            if (daysAfterRecent < forgetStarts)
            {
                return MapCosine(daysAfterRecent, 0, forgetStarts, MemoryProbabilityAtForgetStarts, 1);
            }
            else if (daysAfterRecent < forgetEnds)
            {
                return MapCosine(daysAfterRecent, forgetStarts, forgetEnds, 0, MemoryProbabilityAtForgetStarts);
            }
            else
            {
                return 0;
            }
        }

        static double MapCosine(double domain, double minDomain, double maxDomain, double minValue, double maxValue)
        {
            var ratio = (domain - minDomain) / (maxDomain - minDomain);
            var theta = ratio * Math.PI / 2.0;
            var cos = Math.Cos(theta);
            var value = (maxValue - minValue) * cos + minValue;
            return value;
        }

        static double MapLinear(double domain, double minDomain, double valueAtMin, double maxDomain, double valueAtMax)
        {
            var ratio = (domain - minDomain) / (maxDomain - minDomain);
            var value = (valueAtMax - valueAtMin) * ratio + valueAtMin;
            return value;
        }

        public static double NewWordProbability(double highHeuristic)
        {
            if (highHeuristic < 1)
            {
                return Math.Pow(1 - highHeuristic, FastReview) * (1 - ReviewVsNewWord) + ReviewVsNewWord;
            }
            else
            {
                return 1.0 / highHeuristic * ReviewVsNewWord;
            }
        }

        static Dictionary<int, double> ForgetStarts = new Dictionary<int, double>();
        const double MemoryProbabilityAtForgetStarts = 0.8;

        static Heuristics()
        {
            ForgetStarts.Add(1, (1.0 / (24.0 * 60.0)) * 1);
            ForgetStarts.Add(2, (1.0 / (24.0 * 60.0)) * 3);
            ForgetStarts.Add(3, (1.0 / (24.0 * 60.0)) * 10);
            ForgetStarts.Add(4, 1);
            ForgetStarts.Add(5, 3);
            ForgetStarts.Add(6, 7);
            ForgetStarts.Add(7, 15);
            ForgetStarts.Add(8, 30);
            ForgetStarts.Add(9, 90);
            ForgetStarts.Add(10, 180);
        }

        public const double FastReview = 0.25;
        public const double ReviewVsNewWord = 0.05;
        public const int MaxWeightPool = 50;
    }
}
