﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kasikoi.Data
{
    public class WordRecord
    {
        public WordRecord(int id)
        {
            Id = id;
            EncounterTime = DateTime.Today;
            RecentTime = DateTime.Today;
            Level = MinLevel;
        }

        public static WordRecord Parse(string[] lines, ref int currentLine)
        {
            var id = int.Parse(lines[currentLine++]);
            var record = new WordRecord(id);
            record.EncounterTime = DateTime.Parse(lines[currentLine++]);
            record.RecentTime = DateTime.Parse(lines[currentLine++]);
            record.Level = int.Parse(lines[currentLine++]);

            var counts = StringHelper.SplitIntegersBySpace(lines[currentLine++]);
            var ci = 0;
            record.CorrectCount = counts[ci++];
            record.WrongCount = counts[ci++];
            record.CorrectKanjiCount = counts[ci++];
            record.WrongKanjiCount = counts[ci++];
            record.CorrectKanaCount = counts[ci++];
            record.WrongKanaCount = counts[ci++];
            record.CorrectMeaningCount = counts[ci++];
            record.WrongMeaningCount = counts[ci++];

            var recents = StringHelper.SplitBooleanBySpace(lines[currentLine++]);
            record.recentResult = new List<bool>(recents);
            return record;
        }

        public void AppendTo(StringBuilder sb)
        {
            sb.AppendLine(Id.ToString());
            sb.AppendLine(EncounterTime.ToString("yyyy-MM-dd HH:mm"));
            sb.AppendLine(RecentTime.ToString("yyyy-MM-dd HH:mm"));
            sb.AppendLine(Level.ToString());

            int[] counts = { 
                CorrectCount, WrongCount,
                CorrectKanjiCount, WrongKanjiCount,
                CorrectKanaCount, WrongKanaCount,
                CorrectMeaningCount, WrongMeaningCount };
            StringHelper.AppendWithSpaceTo(sb, counts);
            sb.AppendLine();

            StringHelper.AppendWithSpaceTo(sb, recentResult);
            sb.AppendLine();
        }

        public void MarkCorrect(ProblemType problemType)
        {
            RecentTime = DateTime.Now;
            CorrectCount++;
            if (problemType == ProblemType.HideKanji)
                CorrectKanjiCount++;
            else if (problemType == ProblemType.HideKana)
                CorrectKanaCount++;
            else if (problemType == ProblemType.HideMeaning)
                CorrectMeaningCount++;
            PushRecentResult(true);

            if (IsAllRecentCorrect(levelChangeThreshold))
                Level = Math.Min(Level + 1, MaxLevel);
        }

        public void MarkWrong(ProblemType problemType)
        {
            RecentTime = DateTime.Now;
            WrongCount++;
            if (problemType == ProblemType.HideKanji)
                WrongKanjiCount++;
            else if (problemType == ProblemType.HideKana)
                WrongKanaCount++;
            else if (problemType == ProblemType.HideMeaning)
                WrongMeaningCount++;
            PushRecentResult(false);

            if (IsAllRecentWrong(levelChangeThreshold))
                Level = Math.Max(Level - 1, MinLevel);
        }

        public readonly int Id;
        public DateTime EncounterTime { get; private set; }
        public DateTime RecentTime { get; private set; }
        public const int MinLevel = 1;
        public const int MaxLevel = 10;
        public int Level { get; private set; }
        public int CorrectCount { get; private set; }
        public int WrongCount { get; private set; }
        public int CorrectKanjiCount { get; private set; }
        public int WrongKanjiCount { get; private set; }
        public int CorrectKanaCount { get; private set; }
        public int WrongKanaCount { get; private set; }
        public int CorrectMeaningCount { get; private set; }
        public int WrongMeaningCount { get; private set; }
        public IReadOnlyList<bool> RecentResult
        {
            get
            {
                return recentResult;
            }
        }

        void PushRecentResult(bool correct)
        {
            while (recentResult.Count > maxRecentResult)
                recentResult.RemoveAt(0);
            recentResult.Add(correct);
        }

        bool IsAllRecentCorrect(int count)
        {
            if (recentResult.Count == 0) return false;
            int from = recentResult.Count - 1;
            int to = Math.Max(0, from - count);
            for (int i = from; i >= to; i--)
                if (recentResult[i] == false)
                    return false;
            return true;
        }

        bool IsAllRecentWrong(int count)
        {
            if (recentResult.Count == 0) return false;
            int from = recentResult.Count - 1;
            int to = Math.Max(0, from - count);
            for (int i = from; i >= to; i--)
                if (recentResult[i] == true)
                    return false;
            return true;
        }

        public bool IsRecentCorrect()
        {
            if (recentResult.Count == 0)
                return false;
            return recentResult[recentResult.Count - 1];
        }

        public int GetDaysAfterRecent()
        {
            return (int)(DateTime.Today - RecentTime).TotalDays;
        }

        public int GetRecentWrongCount()
        {
            int count = 0;
            foreach (var b in recentResult)
                if (b == false)
                    count++;
            return count;
        }

        const int levelChangeThreshold = 3;
        const int maxRecentResult = 5;
        List<bool> recentResult = new List<bool>();
    }
}
