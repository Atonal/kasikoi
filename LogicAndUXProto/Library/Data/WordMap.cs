﻿using System.Collections.Generic;

namespace Kasikoi.Data
{
    public class WordMap
    {
        public void Add(string key, Word word)
        {
            if (!map.ContainsKey(key))
                map.Add(key, new List<Word>());

            map[key].Add(word);
        }

        public void Clear()
        {
            map.Clear();
        }

        public IReadOnlyList<Word> this[string key]
        {
            get
            {
                if (map.ContainsKey(key))
                    return map[key];
                return emptyList;
            }
        }

        Dictionary<string, List<Word>> map = new Dictionary<string, List<Word>>();
        List<Word> emptyList = new List<Word>();
    }
}
