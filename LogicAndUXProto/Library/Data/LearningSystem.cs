﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Kasikoi.Data
{
    public partial class LearningSystem
    {
        #region singleton

        private LearningSystem()
        {
            problemSelector = new ProblemSelector(wordBook, wordRecords);
        }

        static LearningSystem instance;
        public static LearningSystem Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LearningSystem();
                    instance.Load();
                }
                return instance;
            }
        }

        public LearningSystem(string data)
        {
            var lines = data.Split(new string[]{ "\r\n", "\n" }, StringSplitOptions.None);
            LearningSystem.Parse(lines, this);
        }

        public static void ChangeInstance(LearningSystem system)
        {
            system.Save();
            Instance.Load();
        }

        #endregion

        #region save load

        private static string GetSavePath()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "kasikoi.txt");
            return filename;
        }

        public void Load()
        {
            var savePath = GetSavePath();
            if (!File.Exists(savePath))
            {
                problemSelector = new ProblemSelector(wordBook, wordRecords);
            }
            else
            {
                var lines = File.ReadAllLines(savePath);
                Parse(lines, this);
            }
        }

        public void Save()
        {
            RecordLog();
            var dataString = Serialize(this);
            File.WriteAllText(GetSavePath(), dataString);
        }

        #endregion

        #region word

        public Word AddWord(Word wordToBeAdded)
        {
            var wordAdded = wordBook.Add(wordToBeAdded);
            foreach (var kanjiNotation in wordToBeAdded.KanjiNotations)
                foreach (var kanji in kanjiNotation)
                    kanjiCharacterToWord.Add(kanji.ToString(), wordAdded);
            recentlyAddedWord = wordAdded;
            return wordAdded;
        }

        public IReadOnlyList<Word> GetWordWithKanjiNotation(string kanjiNotation)
        {
            return wordBook.GetWordWithKanjiNotation(kanjiNotation);
        }

        public IReadOnlyList<Word> GetWordWithKanaNotation(string kanaNotation)
        {
            return wordBook.GetWordWithKanaNotation(kanaNotation);
        }

        public IReadOnlyList<Word> GetWordWithKanjiCharacter(string kanjiCharacter)
        {
            return kanjiCharacterToWord[kanjiCharacter];
        }

        public IReadOnlyList<Word> GetWordWithKanjiCharacter(char kanjiCharacter)
        {
            return GetWordWithKanjiCharacter(kanjiCharacter.ToString());
        }

        public IReadOnlyList<Word> GetAllWords()
        {
            return wordBook.GetAllWords();
        }

        public int WordCount
        {
            get
            {
                return wordBook.WordCount;
            }
        }

        public void AddWordRelation(Word word, Word relatedWord)
        {
            relatedWords.AddRelation(word.Id, relatedWord.Id);
            relatedWords.AddRelation(relatedWord.Id, word.Id);
        }

        public Word RecentlyAddedWord
        {
            get
            {
                return recentlyAddedWord;
            }
        }

        #endregion

        #region kanji

        public Kanji AddKanji(Kanji kanji)
        {
            var addedKanji = kanjiBook.Add(kanji);
            recentlyAddedKanji = addedKanji;
            return addedKanji;
        }

        public bool ContainsKanji(char character)
        {
            return kanjiBook.Contains(character);
        }

        public bool ContainsKanji(string kanji)
        {
            if (kanji.Length != 1) return false;

            return ContainsKanji(kanji[0]);
        }

        public Kanji GetKanji(char character)
        {
            return kanjiBook[character];
        }

        public Kanji GetKanji(string kanji)
        {
            return GetKanji(kanji[0]);
        }

        public void AddKanjiRelation(Kanji kanji, Kanji relatedKanji)
        {
            relatedKanjis.AddRelation(kanji.Id, relatedKanji.Id);
            relatedKanjis.AddRelation(relatedKanji.Id, kanji.Id);
        }

        public IReadOnlyList<Kanji> GetAllKanjis()
        {
            return kanjiBook.GetAllKanjis();
        }

        public int KanjiCount
        {
            get
            {
                return kanjiBook.KanjiCount;
            }
        }

        public Kanji RecentlyAddedKanji
        {
            get
            {
                return recentlyAddedKanji;
            }
        }

        #endregion

        #region learning works

        public Problem NextProblem()
        {
            return problemSelector.NextProblem();
        }

        public void MarkCorrect(Problem problem)
        {
            var id = problem.Word.Id;
            if (!wordRecords.ContainsKey(id))
            {
                wordRecords.Add(id, new WordRecord(id));
            }
            else
            {
                var record = wordRecords[id];
                if (record.IsRecentCorrect())
                {
                    forgettingCurve.RecordCorrect(record.Level, record.GetDaysAfterRecent());
                }
            }
            wordRecords[id].MarkCorrect(problem.Type);
        }

        public void MarkWrong(Problem problem)
        {
            var id = problem.Word.Id;
            if (!wordRecords.ContainsKey(id))
            {
                wordRecords.Add(id, new WordRecord(id));
            }
            else
            {
                var record = wordRecords[id];
                if (record.IsRecentCorrect())
                {
                    forgettingCurve.RecordWrong(record.Level, record.GetDaysAfterRecent());
                }
            }
            wordRecords[id].MarkWrong(problem.Type);
        }

        public int GetEncounteredWord()
        {
            return wordRecords.Count;
        }

        public int GetMemorizedWord()
        {
            double count = 0;
            foreach (var record in wordRecords.Values)
            {
                count += Heuristics.MemorizedProbability(record);
            }
            return (int)count;
        }

        public string GetDescription(Word word)
        {
            var kanjis = word.ComposedKanji;
            var description = new StringBuilder();
            //TODO: 관련 단어 조회 후 추가
            foreach (var kanjiCharacter in kanjis)
            {
                if (ContainsKanji(kanjiCharacter))
                {
                    var kanji = GetKanji(kanjiCharacter);
                    description.Append(kanji.ToString());
                    description.AppendLine();
                    var sameKanjiWords = GetWordWithKanjiCharacter(kanjiCharacter);
                    var viewIndices = RandomHelper.GetRandomIndices(sameKanjiWords.Count, 3);
                    foreach (var i in viewIndices)
                    {
                        if (sameKanjiWords[i] == word)
                            continue;

                        description.Append("  ");
                        sameKanjiWords[i].ToStringBuilder(description, 3);
                        description.AppendLine();
                    }
                    description.AppendLine();
                }
            }
            return description.ToString();
        }

        public string GetKanjiDescription(Word word)
        {
            var kanjis = word.ComposedKanji;
            var description = new StringBuilder();
            foreach (var kanjiCharacter in kanjis)
            {
                if (ContainsKanji(kanjiCharacter))
                {
                    var kanji = GetKanji(kanjiCharacter);
                    description.Append(kanji.ToString());
                    description.AppendLine();
                }
            }
            return description.ToString();
        }

        #endregion

        #region log

        bool IsRecentLogDuplicated()
        {
            try
            {
                var lastLog = logList[logList.Count - 1];
                var lastTokens = lastLog.Split('\t');
                var lastDate = DateTime.Parse(lastTokens[0]);
                var lastMemorized = int.Parse(lastTokens[1]);
                var lastEncounter = int.Parse(lastTokens[2]);
                var lastTotal = int.Parse(lastTokens[3]);
                if (logList.Count < 2) return false;

                var prevLog = logList[logList.Count - 2];
                var prevTokens = prevLog.Split('\t');
                var prevDate = DateTime.Parse(prevTokens[0]);
                var prevMemorized = int.Parse(prevTokens[1]);
                var prevEncounter = int.Parse(prevTokens[2]);
                var prevTotal = int.Parse(prevTokens[3]);
                if (prevMemorized == lastMemorized
                    && prevEncounter == lastEncounter
                    && prevTotal == lastTotal)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public DateTime LastLogRecorded()
        {
            try
            {
                if (IsRecentLogDuplicated())
                {
                    var prevLog = logList[logList.Count - 2];
                    var prevTokens = prevLog.Split('\t');
                    var prevDate = DateTime.Parse(prevTokens[0]);
                    return prevDate;
                }

                var lastLog = logList[logList.Count - 1];
                var lastTokens = lastLog.Split('\t');
                var lastDate = DateTime.Parse(lastTokens[0]);
                return lastDate;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public void RecordLog()
        {
            var memorized = GetMemorizedWord();
            var encounter = GetEncounteredWord();
            var total = WordCount;
            try
            {
                var lastLog = logList[logList.Count - 1];
                var tokens = lastLog.Split('\t');
                var lastTime = DateTime.Parse(tokens[0]);
                var lastMemorized = int.Parse(tokens[1]);
                var lastEncounter = int.Parse(tokens[2]);
                var lastTotal = int.Parse(tokens[3]);
                var isUpdateLastLog =
                    ( lastMemorized == memorized )
                    && ( lastEncounter == encounter )
                    && ( lastTotal == total )
                    && ( IsRecentLogDuplicated() || (DateTime.Now - lastTime).TotalHours < 1.0 );
                if (isUpdateLastLog)
                {
                    logList.RemoveAt(logList.Count - 1);
                }
            }
            catch
            {

            }
            logList.Add(string.Format("{0}\t{1}\t{2}\t{3}",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm"),
                memorized,
                encounter,
                total));
        }

        #endregion

        WordBook wordBook = new WordBook();
        KanjiBook kanjiBook = new KanjiBook();
        IdRelations relatedWords = new IdRelations();
        IdRelations relatedKanjis = new IdRelations();
        WordMap kanjiCharacterToWord = new WordMap();
        Word recentlyAddedWord;
        Kanji recentlyAddedKanji;
        ProblemSelector problemSelector;
        Dictionary<int, WordRecord> wordRecords = new Dictionary<int, WordRecord>();
        ForgettingCurve forgettingCurve = new ForgettingCurve();
        List<string> logList = new List<string>();
    }
}
