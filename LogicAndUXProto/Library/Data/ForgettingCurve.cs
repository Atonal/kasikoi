﻿using System;
using System.Text;

namespace Kasikoi.Data
{
    public class ForgettingCurve
    {
        public ForgettingCurve()
        {
        }

        #region parse

        public void ParseCorrectTable(string[] lines, ref int currentLine)
        {
            ParseTable(correctCount, lines, ref currentLine);
        }

        public void ParseWrongTable(string[] lines, ref int currentLine)
        {
            ParseTable(wrongCount, lines, ref currentLine);
        }

        void ParseTable(int[,] table, string[] lines, ref int currentLine)
        {
            for (int level = WordRecord.MinLevel; level < table.GetLength(0); level++)
            {
                var counts = StringHelper.SplitIntegersBySpace(lines[currentLine++]);
                for (int days = 0; days < table.GetLength(1); days++)
                    table[level, days] = counts[days];
            }
        }

        public void AppendCorrectTable(StringBuilder sb)
        {
            AppendTable(sb, correctCount);
        }

        public void AppendWrongTable(StringBuilder sb)
        {
            AppendTable(sb, wrongCount);
        }

        void AppendTable(StringBuilder sb, int[,] table)
        {
            int[] counts = new int[table.GetLength(1)];
            for (int level = WordRecord.MinLevel; level < table.GetLength(0); level++)
            {
                for (int days = 0; days < table.GetLength(1); days++)
                    counts[days] = table[level, days];
                StringHelper.AppendWithSpaceTo(sb, counts);
                sb.AppendLine();
            }
        }

        #endregion

        public void RecordCorrect(int level, int daysAfterRecent)
        {
            if (daysAfterRecent < 0) return;
            if (level < WordRecord.MinLevel && level > WordRecord.MaxLevel) return;
            daysAfterRecent = Math.Min(maxDays, daysAfterRecent);

            correctCount[level, daysAfterRecent]++;
        }

        public void RecordWrong(int level, int daysAfterRecent)
        {
            if (daysAfterRecent < 0) return;
            if (level < WordRecord.MinLevel && level > WordRecord.MaxLevel) return;
            daysAfterRecent = Math.Min(maxDays, daysAfterRecent);

            wrongCount[level, daysAfterRecent]++;
        }

        const int maxDays = 45;
        int[,] correctCount = new int[WordRecord.MaxLevel + 1, maxDays + 1];
        int[,] wrongCount = new int[WordRecord.MaxLevel + 1, maxDays + 1];
    }
}
