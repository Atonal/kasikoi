﻿using Kasikoi.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kasikoi
{
    public static class StringHelper
    {
        public static string[] SplitWithPunctuation(string textToBeSplitted)
        {
            if (string.IsNullOrWhiteSpace(textToBeSplitted)) return new string[0];

            var list = textToBeSplitted.Split(new char[] { ';', ',', '.' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < list.Length; i++)
                list[i] = list[i].Trim(' ');

            return list;
        }

        static StringBuilder sb = new StringBuilder();
        public static string ConcatenateWithSemicolon(IReadOnlyList<string> textToBeConcatenated)
        {
            if (textToBeConcatenated == null || textToBeConcatenated.Count == 0) return "";
            sb.Clear();
            AppendWithSemicolonTo(sb, textToBeConcatenated);
            return sb.ToString();
        }

        public static void AppendWithSemicolonTo(StringBuilder appendTarget, IReadOnlyList<string> textToBeAppended)
        {
            if (textToBeAppended == null || textToBeAppended.Count == 0)
                return;
            foreach (var token in textToBeAppended)
            {
                appendTarget.Append(token);
                appendTarget.Append(';');
            }
            appendTarget.Remove(appendTarget.Length - 1, 1);
        }

        public static void AppendWithSpaceTo<T>(StringBuilder appendTarget, IEnumerable<T> list)
        {
            foreach (var e in list)
            {
                appendTarget.Append(e);
                appendTarget.Append(' ');
            }
        }

        public static int[] SplitIntegersBySpace(string textToBeSplitted)
        {
            if (string.IsNullOrWhiteSpace(textToBeSplitted)) return new int[0];

            var tokens = textToBeSplitted.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            List<int> list = new List<int>();
            foreach (var token in tokens)
            {
                int number;
                if (int.TryParse(token, out number))
                    list.Add(number);
            }
            return list.ToArray();
        }

        public static bool[] SplitBooleanBySpace(string textToBeSplitted)
        {
            if (string.IsNullOrWhiteSpace(textToBeSplitted)) return new bool[0];

            var tokens = textToBeSplitted.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            List<bool> list = new List<bool>();
            foreach (var token in tokens)
            {
                bool value;
                if (bool.TryParse(token, out value))
                    list.Add(value);
            }
            return list.ToArray();
        }

        public static bool IsKana(char character)
        {
            return (0x3040 <= character && character <= 0x309F)
                || (0x30A0 <= character && character <= 0x30FF);
        }

        public static bool IsKanji(char character)
        {
            return (0x4E00 <= character && character <= 0x9FBF);
        }
    }
}
