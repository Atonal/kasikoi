﻿using System;
using Android.Widget;
using Android.Views;
using System.Threading;
using Kasikoi.Data;

namespace Kasikoi
{
    public class TestView
    {
        MainActivity activity;

        TextView textKanji;
        TextView textKana;
        TextView textMeaning;
        TextView textDescription;
        CheckBox checkMemorized;
        CheckBox checkForgot;
        CheckBox[] checks;
        Button buttonNext;

        bool isProblemState = false;
        Problem currentProblem;

        public TestView(MainActivity activity)
        {
            this.activity = activity;

            activity.SetContentView(Resource.Layout.Test);
            textKanji = activity.FindViewById<TextView>(Resource.Id.kanji);
            textKana = activity.FindViewById<TextView>(Resource.Id.kana);
            textMeaning = activity.FindViewById<TextView>(Resource.Id.meaning);
            textDescription = activity.FindViewById<TextView>(Resource.Id.description);
            checkMemorized = activity.FindViewById<CheckBox>(Resource.Id.memroized);
            checkForgot = activity.FindViewById<CheckBox>(Resource.Id.forgot);
            buttonNext = activity.FindViewById<Button>(Resource.Id.next);
            buttonNext.Click += (object sender, EventArgs e) => { 
                NextWord();
            };

            checks = new CheckBox[]
            {
                checkMemorized,
                checkForgot,
            };
            foreach (var c in checks)
            {
                c.Click += CheckClicked;
            }

            currentProblem = null;
            NextWord();
        }
     
        void CheckClicked(object sender, EventArgs args)
        {
            foreach (var c in checks)
                c.Checked = false;
            (sender as CheckBox).Checked = true;

            if (isProblemState)
                ShowAnswer();
        }

        void NextWord()
        {
            var system = LearningSystem.Instance;
            if (currentProblem != null)
            {
                if (checkMemorized.Checked)
                    system.MarkCorrect(currentProblem);
                else
                    system.MarkWrong(currentProblem);
            }

            var problem = system.NextProblem();

            var word = problem.Word;
            var firstNotation = StringHelper.ConcatenateWithSemicolon(word.KanjiNotations);
            var secondNotation = StringHelper.ConcatenateWithSemicolon(word.KanaNotations);
            var meaning = StringHelper.ConcatenateWithSemicolon(word.Meanings);
            if (problem.Type == ProblemType.HideKana)
            {
                secondNotation = "■";
            }
            else if (problem.Type == ProblemType.HideKanji)
            {
                firstNotation = "■";
            }
            else if (problem.Type == ProblemType.HideMeaning)
            {
                meaning = "■";
            }
            if (string.IsNullOrWhiteSpace(firstNotation))
            {
                firstNotation = secondNotation;
                secondNotation = "";
            }
            textKanji.Text = firstNotation;
            textKana.Text = secondNotation;
            textMeaning.Text = meaning;
            textDescription.Visibility = ViewStates.Gone;

            foreach (var c in checks)
                c.Checked = false;

            isProblemState = true;
            buttonNext.Enabled = false;

            currentProblem = problem;
        }

        void ShowAnswer()
        {
            isProblemState = false;
            buttonNext.Enabled = true;

            var word = currentProblem.Word;
            var firstNotation = StringHelper.ConcatenateWithSemicolon(word.KanjiNotations);
            var secondNotation = StringHelper.ConcatenateWithSemicolon(word.KanaNotations);
            var meaning = StringHelper.ConcatenateWithSemicolon(word.Meanings);
            if (string.IsNullOrWhiteSpace(firstNotation))
            {
                firstNotation = secondNotation;
                secondNotation = "";
            }
            textKanji.Text = firstNotation;
            textKana.Text = secondNotation;
            textMeaning.Text = meaning;

            var system = LearningSystem.Instance;
            textDescription.Text = system.GetKanjiDescription(word);
            textDescription.Visibility = ViewStates.Visible;
        }
    }
}

