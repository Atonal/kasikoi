﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using System.Text;
using System.IO;
using Kasikoi.Data;

namespace Kasikoi
{
    [Activity(Label = "KASIKOI Prototype", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            Button btnClear = FindViewById<Button>(Resource.Id.btnClear);
            btnClear.Click += delegate
            {
                    ShowAlertPopup("미구현");
            };

            Button btnSynch = FindViewById<Button>(Resource.Id.btnSynch);
            btnSynch.Click += delegate
            {
                    var system = LearningSystem.Instance;
                    var data = LearningSystem.Serialize(system);
                    var response = GetServerResponse(data);
                    if (response.ToLower().StartsWith("ok"))
                    {
                        ShowAlertPopup("동기화 완료. 현재 파일이 최신입니다.");
                        return;
                    }

                    var hostSystem = new LearningSystem(response);
                    LearningSystem.ChangeInstance(hostSystem);
                    ShowAlertPopup("동기화 완료. 새 파일을 받아 왔습니다.");
            };

            Button btnStart = FindViewById<Button>(Resource.Id.btnStart);
            btnStart.Click += delegate
            {
                    LearningSystem.Instance.Load();
                    new TestView(this);
            };
        }

        protected override void OnStop()
        {
            LearningSystem.Instance.Save();
            base.OnStop();
        }

        public string GetServerResponse(string requestText)
        {
            try
            {
                var data = Encoding.UTF8.GetBytes(requestText);
                var request = WebRequest.Create("http://110.76.78.21:25250");
                request.Method = "POST";
                request.ContentLength = data.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                var sendStream = request.GetRequestStream();
                sendStream.Write(data, 0, data.Length);
                sendStream.Close();

                var response = request.GetResponse();
                if (response == null) return null;

                var recvStream = response.GetResponseStream();
                var reader = new StreamReader(recvStream);
                return reader.ReadToEnd().Trim();
            }
            catch
            {
                return null;
            }
        }

        public void ShowAlertPopup(string content)
        {
            var dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("알림");
            dialog.SetMessage(content);
            dialog.SetPositiveButton("확인", (sender, e) =>
                {
                });
            dialog.Show();
        }
    }
}


